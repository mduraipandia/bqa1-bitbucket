<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Discontinued_Date</fullName>
        <description>Populate the Discontinued Date when the M33 Proof Site Branding check box is set to true, use the current date.</description>
        <field>Discontinued_Date__c</field>
        <formula>Today()</formula>
        <name>Populate Discontinued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Discontinued Date</fullName>
        <actions>
            <name>Populate_Discontinued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Directory__c.CANCELLED_DIRECTORIES_INDICATOR__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Populate the Discontinued Date when the M33 Proof Site Branding is set to True.  The date will be set to the current date when the check box is set to True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
