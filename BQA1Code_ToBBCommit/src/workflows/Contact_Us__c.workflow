<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Customer_Contact_Us_Advertising_Questions_or_Other</fullName>
        <description>Customer Contact Us - Advertising Questions or Other</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric.pohlabel@berrysfdc2.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Berry_Customer_Portal_Templates/Customer_Contact_Us_Advertising_Question_or_Other</template>
    </alerts>
    <alerts>
        <fullName>Customer_Contact_Us_Billing_and_Customer_Service</fullName>
        <description>Customer Contact Us - Billing and Customer Service</description>
        <protected>false</protected>
        <recipients>
            <recipient>eric.pohlabel@berrysfdc2.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>joann.woodcock@berrysfdc2.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Berry_Customer_Portal_Templates/Customer_Contact_Us_Billing_and_Customer_Service</template>
    </alerts>
    <rules>
        <fullName>Contact Us Advert or Other</fullName>
        <actions>
            <name>Customer_Contact_Us_Advertising_Questions_or_Other</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Us__c.How_Can_We_Help__c</field>
            <operation>equals</operation>
            <value>Advertising Question,Other</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contact Us Billing or CS</fullName>
        <actions>
            <name>Customer_Contact_Us_Billing_and_Customer_Service</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact_Us__c.How_Can_We_Help__c</field>
            <operation>equals</operation>
            <value>Billing Question/Issue,Customer Service</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
