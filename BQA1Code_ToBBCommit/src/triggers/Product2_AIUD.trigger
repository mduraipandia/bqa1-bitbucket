trigger Product2_AIUD on Product2 (after insert) {
    if(trigger.isInsert){    	
    	map<Id, Product2> mapProductList = Product2SOQLMethods.getProductDetailsWithPBEByProductID(trigger.newMap.keySet());
        List<PricebookEntry> lstPriceBookEntry = new List<PricebookEntry>();
        List<Pricebook2> priceBook = [Select Id from Pricebook2 where IsStandard = true limit 1];
        if(priceBook != null && priceBook.size() > 0){
            for(Product2 iterator : trigger.new){
            	Product2 objProduct2 = mapProductList.get(iterator.ID);
            	if(objProduct2.PricebookEntries.size() <= 0){
            		PricebookEntry PBE = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = priceBook[0].ID, IsActive = iterator.IsActive);
                	lstPriceBookEntry.add(PBE);
            	}                
            }
        }        
        if(lstPriceBookEntry.size() > 0){
        	//if(!Test.isRunningTest()) {
            	insert lstPriceBookEntry;
        	//}
        }
    }
}