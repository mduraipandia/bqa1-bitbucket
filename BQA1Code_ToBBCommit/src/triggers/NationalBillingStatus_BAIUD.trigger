trigger NationalBillingStatus_BAIUD on National_Billing_Status__c (before insert, before update) {
	if(trigger.isInsert) {
		NationalBillingStatusHandler.onBeforeInsert(trigger.new);
	} else if(trigger.isUpdate) {
		NationalBillingStatusHandler.onBeforeInsert(trigger.new, trigger.OldMap);
	}
}