trigger bSure_InsertPIAamountupdate on BsureCreditApplication__c (after update) {
    String oppid ='';
    Integer piaamount;
    for(BsureCreditApplication__c caobj: Trigger.new)
    {
        if(caobj.Status__c == 'Completed')
        {
            oppid = caobj.Opportunity__c;
            piaamount = Integer.valueOf(caobj.PIA__c);
        }
    }
    if(oppid != null && oppid != '')
    {
        if(piaamount != null)
        {
            String strQry = 'Select Id,PIA__c from Opportunity where Id = \''+oppid+'\'';
            list<Opportunity> lstopp = new list<Opportunity>();
            lstopp = Database.Query(strQry);
            if(lstopp != null && lstopp.size() > 0)
            {
                for(Opportunity o : lstopp)
                {
                    o.PIA__c = piaamount;
                }
                update lstopp;
            }
        }    
    }
}