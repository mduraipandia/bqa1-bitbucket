global class DirectoryEditionLetterRenewal {
    
   webservice static void DELetterrenewl(string objDEId){
       list<order_line_items__c> OliLstRenewal = new list<order_line_items__c>();
       List<User> objUserLst = [SELECT Id,Name FROM User where Name = 'Letter Renewal' Limit 1];
       String UserId = objUserLst[0].Id;
       OliLstRenewal=OrderLineItemSOQLMethods.getOLINotHandledinModification(objDEId,UserId);
       
       if(OliLstRenewal.size()>0){
       	LetterRenewalController_v1.letterRenewal(OliLstRenewal);
       }
   }
}