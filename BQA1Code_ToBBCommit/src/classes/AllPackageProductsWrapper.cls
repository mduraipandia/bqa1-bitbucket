/******************************************************************************
Wrapper class to form serialized JSON data for creating package product records
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/08/2014
$Id$
*******************************************************************************/
public class AllPackageProductsWrapper {

    public class Products {
        public String code;
        public String cycle;
        public Integer default_quantity;
        public Integer id;
        public Integer max_quantity;
        public Integer min_quantity;
        public Product product;
    }

    public class Product {
        public String billing_cycle;
        public String code;
        public String description;
        public Integer id;
        public String name;
        public List < Properties > properties;
        public String sku;
        public String unit_of_measure;
    }

    public String description;
    public String end_date;
    public Integer id;
    public String name;
    public List < Products > products;
    public String resource_uri;
    public String sku;
    public String start_date;
    public String status;

    public class Properties {
        public String name;
        //public String optional;
        public Boolean optional;
        public String type;
        //public Object value;
    }
}