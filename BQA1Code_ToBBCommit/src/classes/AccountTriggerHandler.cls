public with sharing class AccountTriggerHandler 
{
  private List<Account> newAccounts = new List<Account>();

  public void handleBefore(List<SObject> newAccounts, Map<Id, SObject> oldObjectsMap)
  {
    //AccountList al = new AccountList(newObjects);
    this.newAccounts = newAccounts;
    this.validate(oldObjectsMap);
  }
  /**
   *  method: validate
   *  Takes the map Trigger.oldMap from the Account Trigger
   *   Recursively (?) goes down the parental hierarchy to set delinquency indicator
   */
  public void validate(Map<Id, SObject> oldAccountsMap)
  {
    Set<Id> parentIds = new Set<Id>();
    Set<Id> childIds = new Set<Id>();
    Map<Id,Boolean> delinquentMap = new Map<Id,Boolean>();

    // loop the NEW newAccounts
    for(Account acc : newAccounts)
    {
      Account oldAccount = (Account)oldAccountsMap.get(acc.Id);
      if(Trigger.isBefore && oldAccount != null)
      {
        if(acc.get('Total_Past_Due__c') != oldAccount.get('Total_Past_Due__c'))
        {
          // total past due has gone from > 0 to <= 0
          if((Decimal)oldAccount.get('Total_Past_Due__c') > 0 && (Decimal)acc.get('Total_Past_Due__c') <= 0)
          {
            acc.put('OCA_Vendor__c', null);
            acc.put('Delinquency_Indicator__c', false);
          }
        }
        if((Decimal)acc.get('Net_External_WO__c') > 0 || (Decimal)acc.get('Net_Internal_WO__c') > 0 )
        {
          acc.put('Delinquency_Indicator__c', true);
        }
      }

      if(Trigger.isAfter)
      {
        if(oldAccount.get('Delinquency_Indicator__c') != acc.get('Delinquency_Indicator__c'))
        {
          if(acc.ParentId == null)
          {
            //the account is the parent, update all the children. 
            parentIds.add(acc.Id);
            delinquentMap.put(acc.Id, (boolean)acc.get('Delinquency_Indicator__c'));
          } 
          else 
          {
            //the account is a child, update the parent (which in turn updates the children.)
            childIds.add(acc.parentId);
            delinquentMap.put(acc.parentId, (boolean)acc.get('Delinquency_Indicator__c'));
          }
        }        
      }
    }

     // After the update, perform another update which in turn will fire the trigger again. Only when at the bottom of a hierarchy will the
     // recursion stop (could be dangerous, but the hierarchy is apparently not very deep)
    if(Trigger.isAfter)
    {
      List<Account> accountsToUpdate = new List<Account>();

      if(childIds.size() > 0)
      {
        List<Account> accounts = Database.query('Select Id, ' + 'Delinquency_Indicator__c' + ' from Account where Id in :childIds');
        for(Account acc : accounts)
        {
          Boolean delinquent = delinquentMap.get(acc.Id);

          if(acc.get('Delinquency_Indicator__c') != delinquent)
          {
            acc.put('Delinquency_Indicator__c', delinquent);
            accountsToUpdate.add(acc);
          }
        }
      }

      if(parentIds.size() > 0)
      {
        List<Account> childAccounts = Database.query('Select ParentId, ' + 'Delinquency_Indicator__c' + ' from Account where ParentId in :parentIds');
        for(Account acc : childAccounts)
        {
          Boolean delinquent = delinquentMap.get(acc.ParentId);
          if(acc.get('Delinquency_Indicator__c') != delinquent)
          {
            acc.put('Delinquency_Indicator__c', delinquent);
            accountsToUpdate.add(acc);
          }
        }    
      }
      update accountsToUpdate;      
    }
  }
}