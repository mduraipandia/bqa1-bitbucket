global class GalleyReportDeleteScheduleHndlr implements GalleyReportDeleteSchedule.GalleyReportDeleteScheduleInterface {
   global void execute(SchedulableContext SC) {
        GalleyReportStageDeleteBatch obj = new GalleyReportStageDeleteBatch();
        Database.executeBatch(obj, 2000);
    }  
}