@isTest
private class TestJIRAConnectorWebserviceCalloutSync {
     @isTest static void testCallout() {
        // Set mock callout class 
        Case obj_case=new case();
        obj_case.Status='New';
        insert obj_case;
        case obj_case_up=[select Status from Case where id=:obj_case.id];
        obj_case_up.Status='New';
        update obj_case_up;
        
        CaseComment obje=new CaseComment ();
        obje.CommentBody='testtest';
        obje.ParentId=obj_case.id;
        insert obje;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
         
          JIRAConnectorWebserviceCalloutSync.synchronizeWithJIRAIssue(obj_case.id);
        
        // Verify response received contains fake values
      
    }
}