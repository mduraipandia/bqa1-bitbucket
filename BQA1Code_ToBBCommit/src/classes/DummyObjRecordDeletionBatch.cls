global class DummyObjRecordDeletionBatch implements Database.Batchable<SObject>
{
    //ATP-4381
    string SOQLQuery;
    list<id> listRecID = new list <ID> ();
    global DummyObjRecordDeletionBatch ()
    {
        integer recordCount= Database.countQuery('select count() from Dummy_Object__c  where CreatedDate >= TODAY');      
        if(recordCount > 9)
        {
            //get all the records less than today and delete
            SOQLQuery='select id from Dummy_Object__c  where CreatedDate < TODAY';
           
        }
        else
        {
            //delete all the records other than the last 10 records
            //SOQLQuery ='select id from Dummy_Object__c  order by CreatedDate desc offset 10';
             
             list<Dummy_Object__c> recordsNottoDelete = [select id,name from Dummy_Object__c order by createdDate desc limit 10];
             for(Dummy_Object__c obj:recordsNottoDelete)
             {
                listRecID.add(obj.id);
             }
             SOQLQuery ='select id,name from Dummy_Object__c  where id NOT IN :listRecID';
        }
    }
    
    global Database.queryLocator start(Database.BatchableContext bc)
    {
         return Database.getQueryLocator(SOQLQuery);
    }   
    
     global void execute(Database.BatchableContext bc, List<Dummy_Object__c> listDummyObj) {  
       
        
          
        delete listDummyObj;
        DataBase.emptyRecycleBin(listDummyObj);
    }
    
    global void finish(Database.BatchableContext BC)
    {
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, CreatedById from AsyncApexJob where Id =:BC.getJobId()];
    
      // Send an email to the Apex job's submitter notifying of job completion.  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      /*String[] toAddresses = new String[] {a.CreatedBy.Email};
      mail.setToAddresses(toAddresses);*/
      CommonEmailUtils.sendTextEmailForTargetObject(a.CreatedById,
      'Dummy Object Record deletion Job ' + a.Status,
      'The Dummy Object Record deletion Job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
    }
    
    
    
    
}