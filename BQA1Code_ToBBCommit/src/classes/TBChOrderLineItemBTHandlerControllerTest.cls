@isTest
private class TBChOrderLineItemBTHandlerControllerTest {

    static testMethod void test_updateOLIWithBillingTransferInformation() {
    	 list<Account> lstAccount = new list<Account>();
    	 Account acc=TestMethodsUtility.generateAccount('telco');
    	 acc.Billing_Anniversary_Date__c=System.Today()-60;
		lstAccount.add(acc);
		 Account acc1=TestMethodsUtility.generateAccount('customer');
    	 acc1.Billing_Anniversary_Date__c=System.Today()-60;
		lstAccount.add(acc1);
		Account acc2=TestMethodsUtility.generateAccount('publication');
    	 acc2.Billing_Anniversary_Date__c=System.Today()+60;
		lstAccount.add(acc2);
		insert lstAccount;  
		Account newAccount = new Account();
		Account newPubAccount = new Account();
		Account newTelcoAccount = new Account();
		for(Account iterator : lstAccount) {
		if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
		newAccount = iterator;
		}
		else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
		newPubAccount = iterator;
		}
		else {
		newTelcoAccount = iterator;  
		}
		}
		system.assertNotEquals(newAccount.ID, null);
		system.assertNotEquals(newPubAccount.ID, null);
		system.assertNotEquals(newAccount.Primary_Canvass__c, null);
		system.assertNotEquals(newTelcoAccount.ID, null);
		Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
		objTelco.Telco_Code__c = 'Test';
		update objTelco;
		system.assertNotEquals(newTelcoAccount.ID, null);
		Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
		Contact newContact1 = TestMethodsUtility.createContact(newTelcoAccount.Id);
		Contact newContact2 = TestMethodsUtility.createContact(newPubAccount.Id);
		Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
		Division__c objDiv = TestMethodsUtility.createDivision();
		pymt__Payment_Method__c ParentPaymentMethod= new pymt__Payment_Method__c(pymt__Payment_Type__c='Credit Card',Name='Credit Card',pymt__Contact__c=newContact.Id);
		/*Directory__c objDir = TestMethodsUtility.generateDirectory();
		objDir.Telco_Provider__c = objTelco.Id;
		objDir.Canvass__c = newAccount.Primary_Canvass__c;        
		objDir.Publication_Company__c = newPubAccount.Id;
		objDir.Division__c = objDiv.Id;
		insert objDir;*/
		Directory__c objDir =TestMethodsUtility.createDirectory();
		Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
		Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
		Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
		insert objSHM;
		
		Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
		objDirEd.Book_Status__c='BOTS';
		objDirEd.Ship_Date__c=System.Today();
		objDirEd.Pub_Date__c=system.today();
		insert objDirEd;
		system.debug('*****Directory Edition******'+objDirEd);
		/*Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
		objDirEd1.Book_Status__c='BOTS-1';
		objDirEd.Ship_Date__c=System.Today();
		objDirEd1.BOC__c=System.Today()+2;
		objDirEd1.Pub_Date__c =Date.parse('05/05/2015');
		insert objDirEd1;*/
		 Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd1.New_Print_Bill_Date__c=date.today();
            objDirEd1.Bill_Prep__c=date.parse('01/01/2013');
            objDirEd1.XML_Output_Total_Amount__c=100;
            objDirEd1.Pub_Date__c =Date.today().addMonths(1);
            objDirEd1.Ship_Date__c=Date.today().addMonths(4);
            insert objDirEd1;
		//Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
		//insert objDirEd1;
		
		Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
		objDM.Telco__c = objDir.Telco_Provider__c;
		objDM.Canvass__c = objDir.Canvass__c;
		objDM.Directory__c = objDir.Id;
		insert objDM;
		
		system.assertNotEquals(objDir.ID, null);
		list<Product2> lstProduct = new list<Product2>();      
		for(Integer x=0; x<3;x++){
		Product2 newProduct = TestMethodsUtility.generateproduct();
		newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
		newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
		lstProduct.add(newProduct);
		}
		insert lstProduct;
		
		list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>(); 
		list<PricebookEntry> lstPBE = new list<PricebookEntry>();
		for(Product2 iterator : lstProduct) {
			PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
		    lstPBE.add(pbe);
		}
		insert lstPBE;       
		//list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
		for(Product2 iterator : lstProduct) {
			Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
			objDPM.Product2__c = iterator.Id;
		}
		insert lstDPM;
		
		Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
		newOpportunity.AccountId = newAccount.Id;
		newOpportunity.Pricebook2Id = newPriceBook.Id;
		newOpportunity.Billing_Contact__c = newContact.Id;
		newOpportunity.Signing_Contact__c = newContact.Id;
		newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
		newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
		newOpportunity.Account_Primary_Canvass__c=newAccount.Primary_Canvass__c;  
		newOpportunity.StageName ='Closed Won';
		newOpportunity.CloseDate=System.Today();
		insert newOpportunity;
		List<Opportunity> newOpportunityList=new List<Opportunity>();
		newOpportunityList.add(newOpportunity);
		
		list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
		map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
		for(PricebookEntry iterator : lstPBE) {
			OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
			objOLI.PricebookEntryId = iterator.Id;
			objOLI.Billing_Duration__c = 12;
			objOLI.Directory__c = objDir.Id;
			objOLI.Directory_Edition__c = objDirEd.Id;
			objOLI.Full_Rate__c = 30.00;
			objOLI.UnitPrice = 30.00;
			objOLI.Package_ID__c = '123456';
			objOLI.Billing_Partner__c = objOLI.Id;
			objOLI.OpportunityId = newOpportunity.Id;
			objOLI.Directory_Heading__c = objDH.Id;
			objOLI.Directory_Section__c = objDS.Id;
			lstOLI.add(objOLI);
			mapPBE.put(iterator.Id, iterator);
		}
		insert lstOLI;
		system.debug('****OpportunityProducts****'+lstOLI);
		
		Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);  
		List<Order__c> newOrderList=new List<Order__c>();
		newOrderList.add(newOrder);
		Order__c newOrder1 = TestMethodsUtility.createOrder(newPubAccount.Id);  
		List<Order__c> newOrderList1=new List<Order__c>();
		newOrderList1.add(newOrder1);
		Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
		system.debug('*****Order set****'+newOrderSet);
		List<Order_Group__c> newOrderSetList=new List<Order_Group__c>();
		newOrderSetList.add(newOrderSet);
		list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
		Integer i=0;
		for(OpportunityLineItem iterator : lstOLI) {
		    i++;
			Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
			objOLI.Package_ID__c = iterator.Package_ID__c;
			objOLI.Package_Item_Quantity__c = 2;
			objOLI.Directory__c = iterator.Directory__c;
			objOLI.Directory_Edition__c = iterator.Directory_Edition__c;
			objOLI.UnitPrice__c = iterator.UnitPrice;
			objOLI.Opportunity_line_Item_id__c = iterator.Id;
			objOLI.Media_Type__c = CommonMessages.oliPrintProductType;   
			objOLI.Payment_Method__c = CommonMessages.telcoPaymentMethod;
			objOLI.Successful_Payments__c=1;
			objOLI.Payments_Remaining__c=1;
			objOLI.Billing_Frequency__c='Monthly';
			if(i==1){
			    objOLI.Media_Type__c = 'Digital';  
			    objOLI.isCanceled__c = false;
			    objOLI.Parent_ID__c='1233-3434';
			    objOLI.Parent_ID_of_Addon__c='8785-3454';
			    objOLI.Package_ID_External__c='34346';  
			    objOLI.Billing_Frequency__c='Single Payment';
			    objOLI.Billing_Partner__c='THE BERRY COMPANY';
			}else if(i==2){
			    objOLI.Parent_ID_of_Addon__c='4567-3454';
			    objOLI.Directory_Edition__c = objDirEd1.Id;
			}
			
			objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
			objOLI.Directory_Section__c = iterator.Directory_Section__c;
			objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;  
			objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';  
			objOLI.Talus_Go_Live_Date__c=System.Today();
			objOLI.Order_Anniversary_Start_Date__c=System.Today();
			objOLI.Service_Start_Date__c=System.Today();
			objOLI.Payment_Duration__c=11;
			lstOrderLI.add(objOLI);
		}
		system.debug('***OLI List****'+lstOrderLI);  
		
		insert lstOrderLI;
		Line_Item_History__c lineItemHis=TestMethodsUtility.createLineItemHistory(lstOrderLI[0].Id,objDirEd1.Id);
		List<Line_Item_History__c> lineItemHisList=new List<Line_Item_History__c>();  
		//lineItemHisList.add(lineItemHis);
		map<Id, Integer> mapOLINoOfMonthTransfer=new map<Id,Integer>();  
		mapOLINoOfMonthTransfer.put(lstOrderLI[0].Id,2);
		Set<ID> setOLIForExtraSIN=new Set<Id>();
		setOLIForExtraSIN.add(lstOrderLI[0].Id);
		Account selectedAcc=[select id,Name,Billing_Anniversary_Date__c,Account_Number__c,(select id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from Contacts) from Account where id =:newTelcoAccount.Id];
		Account selectedPubAcc=[select id,Name,Billing_Anniversary_Date__c,Account_Number__c,(select id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from Contacts),(select id,Billing_Anniversary_Date__c from Orders__r) from Account where id =:newPubAccount.Id];
		List<Order_Line_Items__c> selectedOliList=[select id,Billing_Partner__c,Name,Package_ID__c,Package_Item_Quantity__c,Service_Start_Date__c,Service_End_Date__c,Next_Billing_Date__c,Last_Billing_Date__c,Successful_Payments__c,Payments_Remaining__c,Directory_Edition__r.Book_Status__c,
													Date_of_Transfer__c,Billing_Transfer_Type__c,Order_Anniversary_Start_Date__c,Reason_for_Transfer__c,Account__c,Billing_Frequency__c,Number_of_Months_Transferred__c,
													Payment_Method__c,Payment_Duration__c,Talus_Go_Live_Date__c,Billing_Contact__c,Order_Group__c,Opportunity__r.StageName,Opportunity__r.CloseDate from Order_Line_Items__c where id IN :lstOrderLI];
		
		TBChOrderLineItemBTHandlerController.updateOLIWithBillingTransferInformation(selectedOliList,selectedAcc,newPubAccount.Id,newContact1.Id,'Frequency Change',
																						'Go Forward',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
        List<Line_Item_History__c> lineItemHisList1=new List<Line_Item_History__c>();
        TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformation(selectedOliList,selectedAcc,'Frequency Change',objTelco.Id,'Telco','Single',ParentPaymentMethod.Id,'Go Forward',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList1,mapOLINoOfMonthTransfer);
    	 List<Line_Item_History__c> lineItemHisList2=new List<Line_Item_History__c>();
    	TBChOrderLineItemBTHandlerController.updateOLIWithAllBillingTransferInformation(selectedOliList,selectedAcc,newPubAccount.Id,newContact1.Id,'Frequency Change',objTelco.Id,'Telco','Single',ParentPaymentMethod.Id,'Go Forward',
    																					newOrderList,newOrderSetList,newOpportunityList,lineItemHisList2,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
    	 Map<id,Line_Item_History__c>  mapOfLineItemHistory=new Map<id,Line_Item_History__c>();
    	List<Line_Item_History__c> lineItemHisList3=new List<Line_Item_History__c>();
    	TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformationForFC(selectedOliList,selectedAcc,'Frequency Change',mapOfLineItemHistory,'Single',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList3);  
   		List<Line_Item_History__c> lineItemHisList4=new List<Line_Item_History__c>();
   		TBChOrderLineItemBTHandlerController.updateOLIWithBillingTransferInformation(selectedOliList,selectedPubAcc,newTelcoAccount.Id,newContact1.Id,'Frequency Change',
																						'Go Forward',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList4,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
    	List<Line_Item_History__c> lineItemHisList5=new List<Line_Item_History__c>();
    	TBChOrderLineItemBTHandlerController.updateOLIWithAllBillingTransferInformation(selectedOliList,selectedPubAcc,newTelcoAccount.Id,newContact1.Id,'Frequency Change',objTelco.Id,'Telco','Single',ParentPaymentMethod.Id,'Go Forward',
    																					newOrderList,newOrderSetList,newOpportunityList,lineItemHisList5,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
    }  
    
     static testMethod void test_updateOLIWithBillingTransferInformationForRetro() {  
    	 list<Account> lstAccount = new list<Account>();
		 Account acc=TestMethodsUtility.generateAccount('telco');
    	 acc.Billing_Anniversary_Date__c=System.Today()-60;
		lstAccount.add(acc);
		 Account acc1=TestMethodsUtility.generateAccount('customer');
    	 acc1.Billing_Anniversary_Date__c=System.Today()-60;
		lstAccount.add(acc1);
		Account acc2=TestMethodsUtility.generateAccount('publication');
    	 acc2.Billing_Anniversary_Date__c=System.Today()-60;
		lstAccount.add(acc2);   
		insert lstAccount;  
		Account newAccount = new Account();
		Account newPubAccount = new Account();
		Account newTelcoAccount = new Account();
		for(Account iterator : lstAccount) {
		if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
		newAccount = iterator;
		}
		else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
		newPubAccount = iterator;
		}
		else {
		newTelcoAccount = iterator;  
		}
		}
		system.assertNotEquals(newAccount.ID, null);  
		system.assertNotEquals(newPubAccount.ID, null);
		system.assertNotEquals(newAccount.Primary_Canvass__c, null);
		system.assertNotEquals(newTelcoAccount.ID, null);
		Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
		objTelco.Telco_Code__c = 'Test';
		update objTelco;
		system.assertNotEquals(newTelcoAccount.ID, null);
		Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
		Contact newContact1 = TestMethodsUtility.createContact(newTelcoAccount.Id);
		Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
		Division__c objDiv = TestMethodsUtility.createDivision();
		pymt__Payment_Method__c ParentPaymentMethod= new pymt__Payment_Method__c(pymt__Payment_Type__c='Credit Card',Name='Credit Card',pymt__Contact__c=newContact.Id);
		/*Directory__c objDir = TestMethodsUtility.generateDirectory();
		objDir.Telco_Provider__c = objTelco.Id;
		objDir.Canvass__c = newAccount.Primary_Canvass__c;        
		objDir.Publication_Company__c = newPubAccount.Id;
		objDir.Division__c = objDiv.Id;
		insert objDir;*/
		Directory__c objDir =TestMethodsUtility.createDirectory();
		Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
		Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
		Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
		insert objSHM;
		
		Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
		objDirEd.Book_Status__c='BOTS';
		objDirEd.Ship_Date__c=System.Today();
		objDirEd.Pub_Date__c=system.today();
		insert objDirEd;
		system.debug('*****Directory Edition******'+objDirEd);
		/*Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
		objDirEd1.Book_Status__c='BOTS-1';
		objDirEd.Ship_Date__c=System.Today();
		objDirEd1.BOC__c=System.Today()+2;
		objDirEd1.Pub_Date__c =Date.parse('05/05/2015');
		insert objDirEd1;*/
		 Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
            objDirEd1.New_Print_Bill_Date__c=date.today();
            objDirEd1.Bill_Prep__c=date.parse('01/01/2013');
            objDirEd1.XML_Output_Total_Amount__c=100;
            objDirEd1.Pub_Date__c =Date.today().addMonths(1);
            objDirEd1.Ship_Date__c=Date.today().addMonths(4);
            insert objDirEd1;
		//Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
		//insert objDirEd1;
		
		Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
		objDM.Telco__c = objDir.Telco_Provider__c;
		objDM.Canvass__c = objDir.Canvass__c;
		objDM.Directory__c = objDir.Id;
		insert objDM;
		
		system.assertNotEquals(objDir.ID, null);
		list<Product2> lstProduct = new list<Product2>();      
		for(Integer x=0; x<3;x++){
		Product2 newProduct = TestMethodsUtility.generateproduct();
		newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
		newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
		lstProduct.add(newProduct);
		}
		insert lstProduct;
		
		list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>(); 
		list<PricebookEntry> lstPBE = new list<PricebookEntry>();
		for(Product2 iterator : lstProduct) {
			PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
		    lstPBE.add(pbe);
		}
		insert lstPBE;       
		//list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
		for(Product2 iterator : lstProduct) {
			Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
			objDPM.Product2__c = iterator.Id;
		}
		insert lstDPM;
		
		Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
		newOpportunity.AccountId = newAccount.Id;
		newOpportunity.Pricebook2Id = newPriceBook.Id;
		newOpportunity.Billing_Contact__c = newContact.Id;
		newOpportunity.Signing_Contact__c = newContact.Id;
		newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
		newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;
		newOpportunity.Account_Primary_Canvass__c=newAccount.Primary_Canvass__c;  
		newOpportunity.StageName ='Closed Won';
		newOpportunity.CloseDate=System.Today();
		insert newOpportunity;
		List<Opportunity> newOpportunityList=new List<Opportunity>();
		newOpportunityList.add(newOpportunity);
		
		list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
		map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
		for(PricebookEntry iterator : lstPBE) {
			OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
			objOLI.PricebookEntryId = iterator.Id;
			objOLI.Billing_Duration__c = 12;
			objOLI.Directory__c = objDir.Id;
			objOLI.Directory_Edition__c = objDirEd.Id;
			objOLI.Full_Rate__c = 30.00;
			objOLI.UnitPrice = 30.00;
			objOLI.Package_ID__c = '123456';
			objOLI.Billing_Partner__c = objOLI.Id;
			objOLI.OpportunityId = newOpportunity.Id;
			objOLI.Directory_Heading__c = objDH.Id;
			objOLI.Directory_Section__c = objDS.Id;
			lstOLI.add(objOLI);
			mapPBE.put(iterator.Id, iterator);
		}
		insert lstOLI;
		system.debug('****OpportunityProducts****'+lstOLI);
		
		Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
		List<Order__c> newOrderList=new List<Order__c>();
		newOrderList.add(newOrder);
		Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
		system.debug('*****Order set****'+newOrderSet);
		List<Order_Group__c> newOrderSetList=new List<Order_Group__c>();
		newOrderSetList.add(newOrderSet);
		list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
		Integer i=0;
		for(OpportunityLineItem iterator : lstOLI) {
		    i++;
			Order_Line_Items__c objOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet, iterator);
			objOLI.Package_ID__c = iterator.Package_ID__c;
			objOLI.Package_Item_Quantity__c = 2;
			objOLI.Directory__c = iterator.Directory__c;
			objOLI.Directory_Edition__c = iterator.Directory_Edition__c;  
			objOLI.UnitPrice__c = iterator.UnitPrice;
			objOLI.Opportunity_line_Item_id__c = iterator.Id;
			objOLI.Media_Type__c = CommonMessages.oliPrintProductType;   
			objOLI.Payment_Method__c = CommonMessages.telcoPaymentMethod;
			objOLI.Successful_Payments__c=1;
			objOLI.Payments_Remaining__c=1;
			objOLI.Billing_Frequency__c='Monthly'; 
			if(i==1){
			    objOLI.Media_Type__c = 'Digital';
			    objOLI.isCanceled__c = false;
			    objOLI.Parent_ID__c='1233-3434';
			    objOLI.Parent_ID_of_Addon__c='8785-3454';
			    objOLI.Package_ID_External__c='34346';  
			    objOLI.Billing_Frequency__c='Single Payment';
			    objOLI.Billing_Partner__c='THE BERRY COMPANY';
			}else if(i==2){
			    objOLI.Parent_ID_of_Addon__c='4567-3454';
			    objOLI.Directory_Edition__c = objDirEd1.Id;
			}
			
			objOLI.Directory_Heading__c = iterator.Directory_Heading__c;
			objOLI.Directory_Section__c = iterator.Directory_Section__c;
			objOLI.Product2__c = mapPBE.get(iterator.PricebookEntryId).Product2Id;    
			objOLI.Product_Inventory_Tracking_Group__c = 'YP Leader Ad';  
			objOLI.Talus_Go_Live_Date__c=System.Today(); 
			objOLI.Order_Anniversary_Start_Date__c=System.Today();  
			objOLI.Service_Start_Date__c=System.Today();  
			objOLI.Payment_Duration__c=11;
			lstOrderLI.add(objOLI);
		}
		system.debug('***OLI List****'+lstOrderLI);  
		
		insert lstOrderLI;
		Line_Item_History__c lineItemHis=TestMethodsUtility.createLineItemHistory(lstOrderLI[0].Id,objDirEd1.Id);
		List<Line_Item_History__c> lineItemHisList=new List<Line_Item_History__c>();
		//lineItemHisList.add(lineItemHis);
		map<Id, Integer> mapOLINoOfMonthTransfer=new map<Id,Integer>();  
		mapOLINoOfMonthTransfer.put(lstOrderLI[0].Id,2);  
		Set<ID> setOLIForExtraSIN=new Set<Id>();
		setOLIForExtraSIN.add(lstOrderLI[0].Id);
		Account selectedAcc=[select id,Name,Billing_Anniversary_Date__c,Account_Number__c,(select id,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry from Contacts),(select id,Billing_Anniversary_Date__c from Orders__r) from Account where id =:newTelcoAccount.Id];  
		List<Order_Line_Items__c> selectedOliList=[select id,Billing_Partner__c,Name,Package_ID__c,Package_Item_Quantity__c,Service_Start_Date__c,Service_End_Date__c,Next_Billing_Date__c,Last_Billing_Date__c,Successful_Payments__c,Payments_Remaining__c,Directory_Edition__r.Book_Status__c,
													Date_of_Transfer__c,Billing_Transfer_Type__c,Order_Anniversary_Start_Date__c,Reason_for_Transfer__c,Account__c,Billing_Frequency__c,Number_of_Months_Transferred__c,
													Payment_Method__c,Payment_Duration__c,Talus_Go_Live_Date__c,Billing_Contact__c,Order_Group__c,Opportunity__r.StageName,Opportunity__r.CloseDate from Order_Line_Items__c where id IN :lstOrderLI];
		
        TBChOrderLineItemBTHandlerController.updateOLIWithBillingTransferInformation(selectedOliList,selectedAcc,newPubAccount.Id,newContact1.Id,'Frequency Change',
																						'retro',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
    	 List<Line_Item_History__c> lineItemHisList1=new List<Line_Item_History__c>();
    	TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformation(selectedOliList,selectedAcc,'Frequency Change',objTelco.Id,'Telco','Single',ParentPaymentMethod.Id,'retro',newOrderList,newOrderSetList,newOpportunityList,lineItemHisList1,mapOLINoOfMonthTransfer);
    	List<Line_Item_History__c> lineItemHisList2=new List<Line_Item_History__c>();
    	TBChOrderLineItemBTHandlerController.updateOLIWithAllBillingTransferInformation(selectedOliList,selectedAcc,newPubAccount.Id,newContact1.Id,'Frequency Change',objTelco.Id,'Telco','Single',ParentPaymentMethod.Id,'retro',
    																					newOrderList,newOrderSetList,newOpportunityList,lineItemHisList2,mapOLINoOfMonthTransfer,setOLIForExtraSIN);
    }    
}