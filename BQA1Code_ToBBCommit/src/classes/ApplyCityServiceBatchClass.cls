global class ApplyCityServiceBatchClass implements database.batchable<sObject>{
	set<string> setstrDS = new set<string>();
	set<string> setstrCity = new set<string>();
	map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();  
	Galley_Staging_Job__c objGSJ;
	List<Pagination_Job__c> PJs = new List<Pagination_Job__c>();
	
    global ApplyCityServiceBatchClass(set<string> setDS,set<string> setCity,map<string,Community_Section_Abbreviation__c> mapCSA) {
    	if(setDS != null && setDS.size()> 0) {
    		setstrDS.addAll(setDS);
    	}
    	if(setCity != null && setCity.size()> 0) {
    		setstrCity.addAll(setCity);
    	}
    	if(mapCSA.size()> 0) {
    		mapstrCSA = mapCSA;
    	}
    }
    
    global ApplyCityServiceBatchClass(set<string> setDS, set<string> setCity, map<string,Community_Section_Abbreviation__c> mapCSA, Galley_Staging_Job__c GSJ,
    								 List<Pagination_Job__c> PJs) {
    	if(setDS != null && setDS.size()> 0) {
    		setstrDS.addAll(setDS);
    	}
    	if(setCity != null && setCity.size()> 0) {
    		setstrCity.addAll(setCity);
    	}
    	if(mapCSA.size()> 0) {
    		mapstrCSA = mapCSA;
    	}
    	this.objGSJ = GSJ;
    	this.PJs = PJs;
    }
    
    global database.QueryLocator start(database.batchablecontext bc) {
        string Soql = 'Select Id,Name,City_Name_Section_Abbreviation__c,City_Name_Section_Suppressed__c,City_Name_Unedited__c,Directory_Section__c,Listing_City__c,Listing_City_Unformatted__c from Directory_Pagination__c where Directory_Section__c IN :setstrDS AND Listing_City_Unformatted__c IN : setstrCity';
        return database.getQueryLocator(Soql);
    }
    
    global void execute(database.batchablecontext bc,List<Directory_Pagination__c> DPlst) {
        if(DPlst.size()>0){
           DirectoryPagination_AIUDTrigger.ApplyCityServiceDPUpate(DPlst,setstrCity,mapstrCSA,false);
        } 
       
    }
	
    global void finish(Database.BatchableContext bc) {
		if(objGSJ != null) {
            insert objGSJ;
        }
        
        if(PJs != null && PJs.size() > 0) {
        	for(Pagination_Job__c PJ : PJs) {
        		PJ.PJ_Ready_to_Run_Informatica_Job__c = true;
        	}
        	update PJs;
        }
    }
}