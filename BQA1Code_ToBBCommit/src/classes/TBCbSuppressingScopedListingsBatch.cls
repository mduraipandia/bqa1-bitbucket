global class TBCbSuppressingScopedListingsBatch implements Database.Batchable<sObject>,Database.Stateful {
	private set<Id> setId = new set<Id>();
	global TBCbSuppressingScopedListingsBatch(set<Id> setDirEditionId){
		if(setDirEditionId.size()>0 && setDirEditionId != null) {
        	setId.addAll(setDirEditionId);
		}
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
      return database.getQuerylocator('select id,CreatedDate, Listing__c,strSuppressionCheck__c  from Order_Line_Items__c where Directory_Edition__c IN : setId AND Requires_Scoped_Suppression_Processing__c = TRUE AND Scoped_Suppression_Processing_Complete__c = FALSE AND Media_Type__c=\'Print\' order by createdDate ASC');
    }
    global void execute(Database.BatchableContext bc, List<Order_Line_Items__c > oliList) {
    	map<string,list<order_line_Items__c>> mapstrOli = new  map<string,list<order_line_Items__c>>();
        map<String,list<Directory_Listing__c>> mapstrDL = new map<String,list<Directory_Listing__c>>();
        set<Id> setLstId =new Set<Id>();
        set<Id> setscopedListingId = new set<Id>();
        list<Directory_Listing__c> lstDLUpdate=new list<Directory_Listing__c>();
        map<Id,Order_Line_Items__c> mapOliforUpdate = new map<Id,Order_Line_Items__c>();
        for(Order_Line_Items__c OLI:oliList) { 
        	if(OLI.Listing__c != null) {
            	setLstId.add(OLI.Listing__c);
            }
            if(OLI.strSuppressionCheck__c != null) {
                if(!mapstrOli.ContainsKey(OLI.strSuppressionCheck__c)) {
                    mapstrOli.put(OLI.strSuppressionCheck__c,new List<Order_Line_Items__c>());
                }
                mapstrOli.get(OLI.strSuppressionCheck__c).add(OLI);
            }   
        }
        if(setLstId.size()> 0) {
            for(Directory_Listing__c iteratorSL:[select id ,strSuppressionCheck__c,Listing__c from Directory_Listing__c where Caption_Header__c=FALSE AND Do_Not_Suppress__c = FALSE AND Listing__c IN :setLstId AND strSuppressionCheck__c IN:mapstrOli.keySet()]){
                if(iteratorSL.strSuppressionCheck__c != null) {
                    if(!mapstrDL.containsKey(iteratorSL.strSuppressionCheck__c)) {
                        mapstrDL.put(iteratorSL.strSuppressionCheck__c,new list<Directory_Listing__c>());
                    }
                    mapstrDL.get(iteratorSL.strSuppressionCheck__c).add(iteratorSL);
                }
            }
        }
        
        if(mapstrOli.size()> 0) {
            for(String str: mapstrOli.keySet()){
                for(Order_Line_Items__c iterator:mapstrOli.get(str)) {
                    System.debug('#############iterator.strSuppressionCheck__c '+iterator.strSuppressionCheck__c);
                    if(mapstrDL.get(iterator.strSuppressionCheck__c) != null){
                        for(Directory_Listing__c tempDL: mapstrDL.get(iterator.strSuppressionCheck__c)) {
                            if(tempDL.Listing__c==iterator.Listing__c && !setscopedListingId.contains(tempDL.Id)) {
                                tempDL.Suppressed__c = true;
                                tempDL.Suppressed_By_OLI__c = iterator.id;
                                lstDLUpdate.add(tempDL);
                                setscopedListingId.add(tempDL.Id);
                                iterator.Scoped_Suppression_Processing_Complete__c = true;
                                if(!mapOliforUpdate.containskey(iterator.Id)){
                                	mapOliforUpdate.put(iterator.Id,iterator);
                                }
                            }
                        }
                    }           
                }   
            }
        }
        if(lstDLUpdate.size()> 0) {
            update lstDLUpdate;
        }
        if(mapOliforUpdate.size()> 0) {
            update mapOliforUpdate.values();
        }
    }
    global void finish(Database.BatchableContext bc){}
}