@IsTest(SeeAllData=true)
public with sharing class CustomContactLookupControllerTest {

    
    static testMethod void cutomContactLookup()
    {
        UserRole usrRl;
        try{
            usrRl = [Select Id,name from UserRole where Name ='Account Manager'];
        }
        catch(exception e){}
        
        String RoleId;
        if(usrRl != null){
         RoleId = usrRl.Id;
        }
        Profile p = [select Id from profile where name='Standard User'];
      
        User u;
        if(RoleId != null)
            u = [Select Id from User where UserRoleId =: RoleId Limit 1];                               
        
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c objCanvass = TestMethodsUtility.createCanvass(telco);
        
        Account a= new Account(Primary_Canvass__c = objCanvass.Id, Name = 'Unit Testing', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999');
        insert a;        
       /* Contact c1 = new Contact(Phone = '(999) 999-9999', LastName = 'Contact1', IsActive__c = true, FirstName = 'Test Unit', Fax = '9999999999', Email = 'test1@test.com', Description = 'Test', Contact_Type__c = 'Billing', AccountId = a.Id, Contact_Role__c = '1;2', PrimaryBilling__c = true, 
                                        MailingStreet = 'Test', MailingState = 'AL', MailingPostalCode = '245678', MailingCountry = 'US', MailingCity = 'Test');
        insert c1;
         Contact c2 = new Contact(Phone = '(999) 999-9999', LastName = 'Contact2', IsActive__c = true, FirstName = 'Test Unit', Fax = '9999999999', Email = 'test2@test.com', Description = 'Test', Contact_Type__c = 'Billing', AccountId = a.Id, Contact_Role__c = '1;2', 
                                        MailingStreet = 'Test', MailingState = 'AL', MailingPostalCode = '245678', MailingCountry = 'US', MailingCity = 'Test');
        insert c2;
         Contact c3 = new Contact(Phone = '(999) 999-9999', LastName = 'Contact3', IsActive__c = true, FirstName = 'Test Unit', Fax = '9999999998', Email = 'test3@test.com', Description = 'Test', Contact_Type__c = 'Billing', AccountId = a.Id, Contact_Role__c = '1;2', 
                                        MailingStreet = 'Test', MailingState = 'AL', MailingPostalCode = '245678', MailingCountry = 'US', MailingCity = 'Test');
        insert c3; */
        
        Contact objContact1 = TestMethodsUtility.generateContact(a.Id);
        objContact1.Primary_Contact__c=true;
        insert objContact1;
        
        Contact objContact2 = TestMethodsUtility.generateContact(a.Id);
        objContact2.Primary_Contact__c=false;
        objContact2.PrimaryBilling__c=false;
        insert objContact2;
        
        Contact objContact3 = TestMethodsUtility.generateContact(a.Id);
        objContact3.Primary_Contact__c=false;
        objContact3.PrimaryBilling__c=false;
        insert objContact3;
        
        
        
        Opportunity oppty = new Opportunity(Account_Primary_Canvass__c = objCanvass.Id, StageName ='Perception Analysis', 
                                        Name = 'unit test', CloseDate = System.today(), AccountId = a.ID );
        insert oppty;
 
    
        ApexPages.currentPage().getParameters().put('sId', objContact1.FirstName);
        ApexPages.currentPage().getParameters().put('acid',a.id);
        ApexPages.currentPage().getParameters().put('opID',oppty.id);
        
        
        CustomContactLookupController contactLookUPTest = new CustomContactLookupController();
        contactLookUPTest.searchString = objContact1.FirstName;
        contactLookUPTest.search();
        contactLookUPTest.choice=objContact1.Id;
        system.debug('contactLookUPTest.choice======='+contactLookUPTest.choice);
        contactLookUPTest.saveAccount();
        
        
        //contactLookUPTest.runSearch();
                
    }

}