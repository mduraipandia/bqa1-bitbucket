@isTest(SeeAllData=true)
public class EditionCostControllerTest
{
    public static testMethod void TestEditionCostController() 
    {
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Division__c objDiv = TestMethodsUtility.createDivision();
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();*/
        Directory__c objDir = TestMethodsUtility.createDirectory();  
        /*objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.Pub_Date__c=System.today().addMOnths(1);
        insert objDirEd;
        
        Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd1.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd1;
       
        /*
        Edition_Cost__c ec=new Edition_Cost__c();
        ec.Amount__c=1000.00;
        ec.Cost_Type__c='Berry Cost1';
        ec.Directory_Edition__c=objDirEd.Id;
        ec.Telco__c=false;
        insert ec;
        */
        Billing_Settlement__c bs=new Billing_Settlement__c ();
        bs.Berry_Cost_Share__c=60;
        bs.Berry_Settlement_percent__c=10.0;
        bs.Canvass__c=newAccount.Primary_Canvass__c;
        bs.Directory_Edition__c=objDirEd.Id;
        bs.Directory__c=objDir.id;
        bs.Local_Commission__c=10.0;
        bs.Name='Test Settlement';
        bs.Revenue_Loss_percentage__c=10;
        bs.Settlement_frequency_TBC__c='MONTHLY';
        bs.Settlement_frequency_Telco__c='MONTHLY';
        bs.Telco_Cost_Share__c=50;
        bs.Telco__c=objTelco.id;
        insert bs;
      
       PageReference pageRef = Page.EditionCost;
       Test.setCurrentPage(pageRef);
       EditionCostController ecc=new EditionCostController();
       ApexPages.currentPage().getParameters().put('deId',objDirEd.Id);
       ecc.addrow();
       List<EditionCostController.wrapData> WrapList=new List<EditionCostController.wrapData>();
       
      // ecc.editionCostInsert();
       ecc.dosave();
       ecc.newCtype();
       ecc.getEditionCostTypes();
       ecc.doAdd(); 
       ecc.lstWD.clear();
       ecc.Id=String.valueof(objDirEd.Id);
       EditionCostController.wrapData Erap=new EditionCostController.wrapData();
       Erap.PicklistName='Berry Cost1';
       Erap.Amount=100;
       Erap.telcosharepercent=true;
       WrapList.add(Erap);
       ecc.lstWD =WrapList;
       ecc.wrapDataInsert();    
    }
}