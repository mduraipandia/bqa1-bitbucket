global class CommonEmailUtils {	
    
    global static void sendEmailWithEmailTemplate(list<String> CCrecipients,String emailTemplateID,String targetObjectId,String whatId) { 
        sendEmail(null, CCrecipients, null, null, null, emailTemplateID, targetObjectId, whatId, null);
    }
    
    global static void sendEmailWithEmailTemplateAttachement(list<String> CCrecipients,String emailTemplateID,String targetObjectId,String whatId,list<Messaging.EmailFileAttachment> fileAttachments) { 
        sendEmail(null, CCrecipients, null, null, null, emailTemplateID, targetObjectId, whatId, fileAttachments);
    }
    
    global static void sendHTMLEmail(List<String> recipients,String emailSubject,String htmlBody) { 
        sendEmail(recipients, null, emailSubject, htmlBody, true, null, null, null, null);
    }
    
    global static void sendHTMLEmailForTargetObject(String targetObjectId, String emailSubject,String htmlBody) { 
        sendEmail(null, null, emailSubject, htmlBody, true, null, targetObjectId, null, null);
    }
    
    global static void sendTextEmailForTargetObject(String targetObjectId, String emailSubject,String textBody) { 
        sendEmail(null, null, emailSubject, textBody, false, null, targetObjectId, null, null);
    }
    
    global static void sendHTMLEmailWithAttachment(List<String> recipients,String emailSubject,String htmlBody,list<Messaging.EmailFileAttachment> fileAttachments) { 
        sendEmail(recipients, null, emailSubject, htmlBody, true, null, null, null, fileAttachments);
    }
    
    global static void sendEmail(list<String> recipients,list<String> CCrecipients,String emailSubject,String body,Boolean useHTML,String emailTemplateID,String targetObjectId,String whatId,list<Messaging.EmailFileAttachment> fileAttachments) {        
        // Create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();        
        //the email is not saved as an activity.
        mail.setSaveAsActivity(false);
        // Assign the addresses for the To lists to the mail object.
        if(recipients != null && recipients.size() > 0) {
        	mail.setToAddresses(recipients);
        }
        //Assihn the CC addresses for the CC lists to the mail object
        if(CCrecipients != null && CCrecipients.size() > 0) {
        	mail.setCcAddresses(CCrecipients);
        }
        //Assign the templete Id to the mail object
        if(String.isNotBlank(emailTemplateID)) {
        	mail.setTemplateId(emailTemplateID);
        }
        //Assign Target Object to the mail object
        if(String.isNotBlank(targetObjectId)) {
        	mail.setTargetObjectId(targetObjectId);
        }
        //Assign the WhatId to the mail object
        if(String.isNotBlank(whatId)) {
        	mail.setWhatId(whatId);
        }
        // Specify the subject line for your email address.
        if(String.isNotBlank(emailSubject)) {
        	mail.setSubject(emailSubject);
        }
        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);
        // The email address of the user executing the Apex Code will be used.        
        mail.setUseSignature(false);
        if(String.isNotBlank(body)) {
	        if (useHTML) {
	            // Specify the html content of the email.
	            
	            	mail.setHtmlBody(body);
	                        
	        } else {
	            // Specify the text content of the email.
	            mail.setPlainTextBody(body);
	        }
        }
        // Specify FileAttachments
        if(fileAttachments != null && fileAttachments.size() > 0) {
            mail.setFileAttachments(fileAttachments);
        }
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    global static Messaging.EmailFileAttachment generateEmailFileAttachment(String attachmentName,Blob attachmentBody) {
    	Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
        fileAttachment.setFileName(attachmentName);
        fileAttachment.setBody(attachmentBody);        
        return fileAttachment;
    }

}