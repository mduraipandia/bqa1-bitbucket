global class CCExpireEmailBatch implements Database.Batchable<sobject> {	
	global Database.QueryLocator start(Database.BatchableContext bc) {
		String SOQL = 'SELECT Id, Billing_Contact__c, Talus_Go_Live_Date__c FROM Order_Line_Items__c WHERE IsCanceled__c = False AND Talus_Go_Live_Date__c != Null';
		return Database.getQueryLocator(SOQL);
	}
	
	global void execute(Database.BatchableContext bc, List <Order_Line_Items__c> OLIs) {
		ccexpireEmails.ccExpireNotification(OLIs);
	}
	
	global void finish(Database.BatchableContext bc) {
	}
}