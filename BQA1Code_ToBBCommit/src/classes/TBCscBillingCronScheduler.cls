global class TBCscBillingCronScheduler Implements Schedulable {
    public interface TBCscBillingCronSchedulerInterface {
        void execute(SchedulableContext sc);
    }
    global void execute(SchedulableContext sc) {
        Type targetType = Type.forName('TBChBillingCronSchedulerHandler');
        if(targetType != null) {
            TBCscBillingCronSchedulerInterface obj = (TBCscBillingCronSchedulerInterface )targetType.newInstance();
            obj.execute(sc);
        }
    }
}