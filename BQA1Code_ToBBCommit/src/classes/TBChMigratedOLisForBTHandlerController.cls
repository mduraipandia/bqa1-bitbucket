public class TBChMigratedOLisForBTHandlerController{
    public static void SingleToMonthly(boolean isValidate,Map<id,order_line_Items__c> mapForUpdatedOLis,map<Integer, list<c2g__codaCreditNoteLineItem__c>> mapSCNLI, map<Integer, list<c2g__codaInvoiceLineItem__c>> mapSalesInvoiceLineItem,list<c2g__codaCreditNote__c> lstInsertSCN,list<c2g__codaInvoice__c> insertSalesInvoice,Id accountid,String selectedReasonForTransfer,List<Order__c> lstNewOrders,List<Order_Group__c> lstNewOrderGroup,List<Opportunity> lstNewOpportunity,List<Line_Item_History__c> lstOLIHistory){
        map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>> mapOLISI = new map<String, map<Id, list<c2g__codaInvoiceLineItem__c>>>();
        Map<id,Line_Item_History__c>  mapOfLineItemHistory= new map<id,Line_Item_History__c>();
        List<order_line_items__c> lstorderlineForUpdate =mapForUpdatedOLis.values();
        Map<String ,id> dimensionsMapValues;
        map<String, Id> mapOfPeriod  = TBCcBillingTransferCommonMethod.getMapOFPeriod();
        dimensionsMapValues=DimensionsValues.getDimensionByOLI(mapForUpdatedOLis.values());     
        if(!isValidate){
            lstorderlineForUpdate= new List<order_line_items__c>();
             list<Account> lstAccount = AccountSOQLMethods.getAccountsByAcctId(new set<Id> {accountid});
              lstorderlineForUpdate = TBChOrderLineItemBTHandlerController.updateOLIWithDigitalBillingTransferInformationForFC( mapForUpdatedOLis.values(), lstAccount[0], selectedReasonForTransfer,mapOfLineItemHistory,'Monthly',lstNewOrders,lstNewOrderGroup,lstNewOpportunity,lstOLIHistory);
             System.debug('====>>>after lstorderlineForUpdate'+lstorderlineForUpdate);
             update lstorderlineForUpdate;
             dimensionsMapValues= DimensionsValues.getDimensionByOLI(lstorderlineForUpdate);
        }
        Integer monthAlreadyPassed =0;
        Date startDate; 
        Date todayDate = Date.today();
        order_line_items__C tempOLI;
        integer iCountSI=0;
        integer iCountSCN=0;
        
        for(Order_line_items__c oli :lstorderlineForUpdate) {
            iCountSCN++;
            if(mapOfLineItemHistory.containskey(oli.Id))
                startDate = mapOfLineItemHistory.get(oli.Id).Service_Start_Date__c;
            else //this will be used only on Validate button
                startDate=Date.newInstance(oli.Talus_Go_Live_Date__c.year(),oli.Talus_Go_Live_Date__c.month(),oli.Order_Anniversary_Start_Date__c.day());  
            if(startDate==null && Test.isRunningTest())
                 startDate= todayDate.addMonths(-5);  
            monthAlreadyPassed = startDate.monthsBetween(todayDate);
            if(todayDate.day() > startDate.day()) {
                monthAlreadyPassed += 1;
            }
            
            // this is for creating SCN for Offset one time payment.
                if(!mapSCNLI.containsKey(iCountSCN)) {
                   mapSCNLI.put(iCountSCN, new list<c2g__codaCreditNoteLineItem__c>());
                }
                 mapSCNLI.get(iCountSCN).add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineItemForMigratedOLisForStoM(oli,dimensionsMapValues));          
                 if(mapSCNLI.size()>0)
                       lstInsertSCN.add(TBChFFDocumentsForBTHandlerController.newSalesCreditLineNoteForMigratedOLisForStoM(oli,iCountSCN,mapOfPeriod));                         
            // this is creating Monthly invoices 
            for(integer i = 1;i <= monthAlreadyPassed ; i++) {
               iCountSI++;
               insertSalesInvoice.add(TBChFFDocumentsForBTHandlerController.newSalesInvoiceForMOnthlyFrequencyChangeForMigratedOLisForStoM(Oli,iCountSI,mapOfPeriod,0,startDate.addMonths(i-1),startDate.addMonths(i).adddays(-1),i,Integer.valueOf(oli.Payment_Duration__c)-i));
               if(!mapSalesInvoiceLineItem.containsKey(iCountSI)) {
                        mapSalesInvoiceLineItem.put(iCountSI, new list<c2g__codaInvoiceLineItem__c>());
               }
               mapSalesInvoiceLineItem.get(iCountSI).add(TBChFFDocumentsForBTHandlerController.newalesInvoiceLineItemForFrequencyChangeForMigratedOLisForStoM(OLi,dimensionsMapValues,i,Integer.valueOf(oli.Payment_Duration__c)-i));
            }
        }
        System.debug('#######Ankit#########List of insertSalesInvoice==='+insertSalesInvoice);
        System.debug('#######Ankit#########Size of insertSalesInvoice=='+insertSalesInvoice.size());
        System.debug('#######Ankit#########List of insertSalesInvoice==='+lstInsertSCN);
        System.debug('#######Ankit#########Size of insertSalesInvoice=='+lstInsertSCN.size());      
    } 

}