/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class FFA_CleanupTelcoCashMatchingApiBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private List<String> errorMessages;
    private List<String> successMessages;
    private Set<id> transLineIds;

    public FFA_CleanupTelcoCashMatchingApiBatch() 
    {
        initialise();
    }
    
    private void initialise()
    {
        errorMessages = new List<String>();
        successMessages = new List<String>();
        transLineIds = new Set<id>();     
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date matchingDate = system.today().adddays(-3);
        Id userId = UserInfo.getUserId();
        
        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__Matching_Reference__c, '+
            '   ffps_bmatching__Completed__c '+
            'FROM '+
            '   ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '   ffps_bmatching__Completed__c = false '+
            'AND '+
            '   CreatedDate >= :matchingDate '+
            'AND '+
            '   CreatedById = :userId ';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> unTypedScope) 
    {
        delete [Select Id From ffps_bmatching__Custom_Cash_Matching_History_Group__c];
           
        Set<id> headerIds = new set<id>();
        List<ffps_bmatching__Custom_Cash_Matching_History__c> scope = (List<ffps_bmatching__Custom_Cash_Matching_History__c>)unTypedScope;
        for(ffps_bmatching__Custom_Cash_Matching_History__c so :scope)
        {
            headerIds.add(so.id);
        }

        List<ffps_bmatching__Custom_Cash_Matching_History__c> matchingHistories = 
            [SELECT
                id,
                name,
                ffps_bmatching__Matching_Reference__c,
                ffps_bmatching__Period__c,
                ffps_bmatching__Completed__c,
                ffps_bmatching__Account__c,             
                ffps_bmatching__Matching_Date__c,
                (SELECT
                    id,
                    ffps_bmatching__Transaction_Line_Id__c,
                    ffps_bmatching__Transaction_Line_Item__c,
                    ffps_bmatching__Amount_Matched__c
                FROM
                    ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
            FROM 
                ffps_bmatching__Custom_Cash_Matching_History__c
            WHERE
                id IN :headerIds];

        for(ffps_bmatching__Custom_Cash_Matching_History__c so :matchingHistories)
        {
            System.Savepoint sp = Database.setSavepoint();
            try
            {
                c2g.CODAAPICommon_7_0.Context context = null;
                c2g.CODAAPICashMatchingTypes_7_0.Configuration configuration = new c2g.CODAAPICashMatchingTypes_7_0.Configuration();
                configuration.MatchingCurrencyMode = c2g.CODAAPICashMatchingTypes_7_0.enumMatchingCurrencyMode.Document; 
                configuration.MatchingDate = so.ffps_bmatching__Matching_Date__c;
                List<c2g.CODAAPICashMatchingTypes_7_0.Item> items = new List<c2g.CODAAPICashMatchingTypes_7_0.Item>();

                configuration.Account = c2g.CODAAPICommon.getRef(so.ffps_bmatching__Account__c, null);
                configuration.MatchingPeriod = c2g.CODAAPICommon.getRef(so.ffps_bmatching__Period__c, null);

                for(ffps_bmatching__Custom_Cash_Matching_History_Line__c line :so.ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
                {
                    transLineIds.add(line.ffps_bmatching__Transaction_Line_Item__c);
                    c2g.CODAAPICashMatchingTypes_7_0.Item item = new c2g.CODAAPICashMatchingTypes_7_0.Item();
                    item.TransactionLineItem = c2g.CODAAPICommon.getRef( line.ffps_bmatching__Transaction_Line_Id__c, null );
                    item.Paid = line.ffps_bmatching__Amount_Matched__c;
                    item.Discount = 0; 
                    item.WriteOff = 0; 
                    items.add( item );
                }

                if (items.size() > 0)
                {
                    c2g.CODAAPICashMatchingTypes_7_0.Analysis analisysInfo = new c2g.CODAAPICashMatchingTypes_7_0.Analysis();
                    c2g.CODAAPICommon.Reference matchReference = c2g.CODAAPICashMatching_7_0.Match(context, configuration, items, analisysInfo);
                    so.ffps_bmatching__Matching_Reference__c = matchReference.id;
                    so.ffps_bmatching__Completed__c = true;
                    so.ffps_bmatching__Error__c = true;
                }
            }
            catch (Exception e)
            {
                Database.rollback(sp);
                errorMessages.add( 'Custom Cash Matching Reference : ' + so.Name + ' was not matched because : ' + e.getMessage() + '(' + e.getLineNumber() + ')' + '\n\n');
                so.ffps_bmatching__Error_Message__c = 'Custom Cash Matching Reference : ' + so.Name + ' was not matched because : ' + e.getMessage() + '(' + e.getLineNumber() + ')';
                so.ffps_bmatching__Error__c = true;
            }
        }
        update matchingHistories;
         
        if(transLineIds.size() > 50) 
        {
            update [SELECT id FROM c2g__codaTransactionLineItem__c WHERE id IN :transLineIds];
            transLineIds = new Set<id>();
        }
    }
    public void finish( Database.BatchableContext BC )
    {
        if(!transLineIds.isEmpty()) update [SELECT id FROM c2g__codaTransactionLineItem__c WHERE id IN :transLineIds]; 
        AsyncApexJob batchJob = [
            Select
                Id, 
                Status, 
                NumberOfErrors, 
                ExtendedStatus, 
                JobItemsProcessed, 
                TotalJobItems, 
                CreatedBy.Email 
            From 
                AsyncApexJob 
            Where 
                Id = :bc.getJobId()
        ];
        
        if(!Test.isRunningTest()) sendEmail( batchJob.CreatedBy.Email, successMessages, errorMessages );
    }

    private void sendEmail( String address, List<String>successMessages, List<String>errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of matching:\n\n';
 
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Cash Matching API Batch' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}