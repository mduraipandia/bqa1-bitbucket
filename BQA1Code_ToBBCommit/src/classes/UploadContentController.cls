public class UploadContentController {
    public ContentVersion contentToUpload {get;set;}
    public Blob fileContent {get;set;}
    Directory_Edition__c DirEdi = new Directory_Edition__c();
    Id DEId;

    public UploadContentController(ApexPages.StandardController controller) {
        DirEdi = (Directory_Edition__c)controller.getRecord();
        contentToUpload = new ContentVersion();
        DEId = DirEdi.Id;
    }    
    
    public PageReference uploadContents() {
        List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name = 'Directory Edition Files'];
        contentToUpload.VersionData = fileContent;
        try {
            insert contentToUpload;
        } catch(Exception e) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        if(contentToUpload.Id != null) {
            contentToUpload = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :contentToUpload.Id];
        }
        
        ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
        cwd.ContentDocumentId = contentToUpload.ContentDocumentId;
        if(CWList.size()>0){
            cwd.ContentWorkspaceId = CWList.get(0).Id;
        }
        try {
            insert cwd;
        } catch(Exception e) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
        /*ContentDocumentLink cdl = new ContentDocumentLink();
        cdl.LinkedEntityId = DEId;
        cdl.ContentDocumentId = contentToUpload.ContentDocumentId;
        cdl.ShareType = 'c';
        insert cdl;*/
        
        PageReference pg = new PageReference('/' + DEId);
        pg.setRedirect(true);
        return pg;
    }
}