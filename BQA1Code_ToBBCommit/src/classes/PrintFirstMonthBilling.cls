global class PrintFirstMonthBilling implements Database.Batchable<sObject>, Database.Stateful {
    String mediaType;
    global Date dtBD;
    global Set<Id> setOrderSetIds = new Set<Id>();
    Boolean dailyCronBool = false;
    
    global PrintFirstMonthBilling(Date billPrepDate, String mediaType) {
       dtBD = billPrepDate;
       this.mediaType = mediaType;
    }
    
    global PrintFirstMonthBilling(Date billPrepDate, String mediaType, Boolean dailyCronBool) {
       dtBD = billPrepDate;
       this.mediaType = mediaType;
       this.dailyCronBool = dailyCronBool;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, MC_Order_Set_Id__c FROM Monthly_Cron__c WHERE MC_OLI_Media_Type__c =: mediaType AND MC_OLI_Next_Billing_Date__c =: dtBD';
        System.debug('Testingg query '+query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Monthly_Cron__c> listMonthlyCron) {
    	List<Order_Line_Items__c> listOLI = new List<Order_Line_Items__c>();
        String strRT = System.Label.TestOLIRTLocal;
        Set<Id> orderSetIds = new Set<Id>();
        
        for(Monthly_Cron__c MC : listMonthlyCron) {
			orderSetIds.add(MC.MC_Order_Set_Id__c);
		}
        
        listOLI = [SELECT Id,Name, Order__c, Order__r.Account__c, Canvass__c, Description__c , Directory_Code__c, 
                   Directory__c, Directory_Edition__c,Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, 
                   Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.Family, Product2__r.RGU__c, Media_Type__c, 
                   ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, UnitPrice__c, Billing_Contact__c,  
                   Edition_Code__c, Order_Anniversary_Start_Date__c,Billing_End_Date__c, Billing_Frequency__c,
                   Billing_Partner__c, Billing_Start_Date__c,Telco__c,Telco_Invoice_Date__c, Print_First_Bill_Date__c,
                   Digital_Product_Requirement__c,Payment_Method__c, Payments_Remaining__c, 
                   Successful_Payments__c, Payment_Duration__c,Service_Start_Date__c,
                   Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c,
                   Talus_Fulfillment_Date__c,Current_Daily_Prorate__c,Quote_Signed_Date__c,
                   Quote_signing_method__c, Total_Prorated_Days_for_contract__c,
                   Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, Order_Group__c,
                   Billing_Close_Date__c, Last_Billing_Date__c, Next_Billing_Date__c,
                   Contract_End_Date__c, Line_Status__c,Continious_Billing__c,
                   Prorate_Stored_Value__c, Cancellation__c, Total_Prorate__c,
                   Product2__r.Name, Product2__r.Product_Type__c,Prorate_Credit__c,
                   Prorate_Credit_Days__c, Current_Billing_Period_Days__c,Order_Line_Total__c,
                   BillingChangeNoofDays__c, CMR_Name__c,
                   Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c,
                   BillingChangesPayment__c, Order_Billing_Date_Changed__c, OriginalBillingDate__c,
                   P4P_Price_Per_Click_Lead__c, P4P_Current_Billing_Clicks_Leads__c, 
                   P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c,Billing_Partner_Account__c,
                   Order_Line_Items__c.Account__c 
                   FROM Order_Line_Items__c  
                   WHERE IsCanceled__c = False 
                   AND Telco_Invoice_Date__c =: dtBD
                   AND Media_Type__c = 'Print' 
                   AND Talus_Go_Live_Date__c = Null AND RecordTypeID =:System.Label.TestOLIRTLocal AND Order_Group__c IN: orderSetIds];
        
        if(listOLI.size() > 0) {
            BillingFirstMonthPrintHandlerCtl.processTelcoInvoiceAndFirstPrintBillDateBilling(listOLI);
            String str = '';
            for(Order_Line_Items__c OLI : listOLI) {
                str += OLI.Order_Group__c;
                setOrderSetIds.add(OLI.Order_Group__c);
            }
        }
    }
    
    global void finish(Database.BatchableContext bc) {
         AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
         Batch_Size__c FMPBatch = Batch_Size__c.getInstance('FMP');
         Set<String> recipientIds = new Set<String>();
         recipientIds.addAll(User_Ids_For_Email__c.getInstance('LocalBilling').User_Ids__c.split(';'));
         recipientIds.add(UserInfo.getUserId());
         if(a.NumberOfErrors > 0) {
            futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'Print First Month Billing batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and No Of Order Set Processed ' + setOrderSetIds.size() +
            ' batches with '+ a.NumberOfErrors + ' failures.', 'Print First Month Billing batch process');
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Print First Month Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + setOrderSetIds.size() +
            	' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            }
         }else if(a.JobItemsProcessed != a.TotalJobItems) {
            futureCreateErrorLog.createErrorRecordBatch('Either the batch job was aborted or Job failed to process due to internal error', 'Print First Month Billing batch process status : ' + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and No Of Order Set Processed ' + setOrderSetIds.size() +
                ' batches with '+ a.NumberOfErrors + ' failures.', 'Print First Month Billing batch process');
            
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Print First Month Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and No Of Order Set Processed ' + setOrderSetIds.size() +
            	' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            }
            
        }
        else {
            for(String str : recipientIds) {
	            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Print First Month Billing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and No Of Order Set Processed ' + setOrderSetIds.size() +
            	' batches with '+ a.NumberOfErrors + ' failures.');
            }
        }
         
        if(dailyCronBool) {
	        if(a.NumberOfErrors > 0 && FMPBatch.Size__c != 1) {
	            PrintFirstMonthBilling objFMPBatch = new PrintFirstMonthBilling(dtBD, mediaType, true);
	            Database.executeBatch(objFMPBatch, 1);
	            FMPBatch.Size__c = 1;
	            update FMPBatch;
	        } else {
	            Database.executeBatch(new DigitalMonthlyBilling(dtBD, CommonMessages.digitalMediaType, true), 20);
	            FMPBatch.Size__c = 20;
	            update FMPBatch;
	        }
        }
    }
}