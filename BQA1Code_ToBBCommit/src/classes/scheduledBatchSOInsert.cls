global class scheduledBatchSOInsert implements Schedulable {
   public Interface scheduledBatchSOInsertInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('scheduledBatchSOInsertHndlr');
        if(targetType != null) {
            scheduledBatchSOInsertInterface obj = (scheduledBatchSOInsertInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}