global class TBChMonthlyCronSchedulerHandler implements TBCscMonthlyCronScheduler.TBCscMonthlyCronSchedulerInterface {
    global void execute(SchedulableContext sc) {
        TBCbCleanMonthlyCronRecords obj = new TBCbCleanMonthlyCronRecords(null, true);
        Database.executeBatch(obj, 2000);
    }
}