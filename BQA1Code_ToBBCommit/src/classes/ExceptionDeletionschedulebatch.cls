global class ExceptionDeletionschedulebatch implements Schedulable {
   public Interface ExceptionDeletionschedulebatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ExceptionDeletionschedulebatchHndlr');
        if(targetType != null) {
            ExceptionDeletionschedulebatchInterface obj = (ExceptionDeletionschedulebatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}