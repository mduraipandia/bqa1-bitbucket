@IsTest
public class FulfillmentProfileSOQLMethodsTest{
    static testmethod void FulfillmentProfileSOQLMethodsTest(){
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            insert lstAccount;
            set<Id> setAccId = new set<Id>();
            for(Account iterator : lstAccount) {
                setAccId.add(iterator.Id);
            }
            Test.startTest();
                list<Fulfillment_Profile__c> lstFF = [Select ID from Fulfillment_Profile__c where Account__c IN:setAccId];
                set<Id> setFF = new set<Id>();
                system.assertNotEquals(lstFF, null);
                system.assertNotEquals(lstFF[0].Id, null);
                for(Fulfillment_Profile__c iterator : lstFF) {
                    setFF.add(iterator.Id);
                }
                FulfillmentProfileSOQLMethods.getFulFillmentProfileByAccountId(setAccId);
                FulfillmentProfileSOQLMethods.getFulfillmentProfilebyId(setFF);
                FulfillmentProfileSOQLMethods.getFulfillmentProfilebyAccountIdForUpdatePF(setAccId);
                FulfillmentProfileSOQLMethods.getFulFillmentProfileListByAccountId(setAccId);
            Test.stopTest();
        }
    }
}