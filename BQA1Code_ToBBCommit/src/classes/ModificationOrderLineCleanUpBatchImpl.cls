global class ModificationOrderLineCleanUpBatchImpl implements Database.Batchable<SObject>
{
    public String query;

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
      System.debug('Query for ModificationOrderLineCleanUpBatchImpl' + query);
      return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
    
      delete scope;
      DataBase.emptyRecycleBin(scope);
    }

    global void finish(Database.BatchableContext BC)
    {
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email, CreatedById from AsyncApexJob where Id =:BC.getJobId()];
    
      // Send an email to the Apex job's submitter notifying of job completion.  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      /*String[] toAddresses = new String[] {a.CreatedBy.Email};
      mail.setToAddresses(toAddresses);*/
      CommonEmailUtils.sendTextEmailForTargetObject(a.CreatedById, 'Modification Order line Item Deletion Job ' + a.Status,  
      'The Modification Order line Item job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.');
    }

}