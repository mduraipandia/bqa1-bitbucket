/*********************************************************
Batch class to create properties' records for each Product
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 10/07/2014
$Id$
**********************************************************/
global class CreateUDACProperties implements Database.Batchable < sObject > , Database.AllowsCallouts {

    global String query;

    global CreateUDACProperties(String query) {
        this.query = query;
    }

    global Database.Querylocator start(Database.BatchableContext BC) {
        //System.debug('************The Query************' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Package_ID__c > scope) {

        List < Product_Properties__c > lstproperties = new List < Product_Properties__c > ();
        List < Product_Properties__c > packageproperties = new List < Product_Properties__c > ();

        for (Package_ID__c pkgs: scope) {
            //Invoke pkg prdts prpts creation class
            packageproperties = CreateUDACPackageProperties_2.createProperties(pkgs.id);
            if (packageproperties.size() > 0) {
                for (Product_Properties__c prts: packageproperties) {
                    lstproperties.add(prts);
                }
            }
        }

        if (lstproperties.size() > 0) {

            insert lstproperties;

        }

    }

    global void finish(Database.BatchableContext BC) {

        List < Messaging.SingleEmailMessage > lstAdmEmails = new List < Messaging.SingleEmailMessage > ();
        List < String > admEmails = Label.Fulfillment_Batch_Notification_Emails.split(',');

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        //Notify Admin on Job completion
        for (String trgtObjId: admEmails) {
            lstAdmEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Fulfillment Packages/Products/Properties creation was ' + a.Status, 'The batch Apex job is processed for ' + a.TotalJobItems +' batches with ' + a.NumberOfErrors + ' failures'));
        }
        Messaging.sendEmail(lstAdmEmails);

    }

}