global class PushLeadstoQueueScheduler implements Schedulable {
   public Interface PushLeadstoQueueSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('PushLeadstoQueueSchedulerHndlr');
        if(targetType != null) {
           PushLeadstoQueueSchedulerInterface obj = (PushLeadstoQueueSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}