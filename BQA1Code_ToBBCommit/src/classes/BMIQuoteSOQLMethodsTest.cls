@isTest
public with sharing class BMIQuoteSOQLMethodsTest {
	@isTest static void BMIQuoteSOQLMethodsCoverage() {
        User u = TestMethodsUtility.generateStandardUser();
		list<Account> lstAccount = new list<Account>();
		lstAccount.add(TestMethodsUtility.generateAccount('telco'));
		lstAccount.add(TestMethodsUtility.generateAccount('customer'));
		lstAccount.add(TestMethodsUtility.generateAccount('publication'));
		insert lstAccount;	
		Account newAccount = new Account();
		Account newPubAccount = new Account();
		Account newTelcoAccount = new Account();
		for(Account iterator : lstAccount) {
			if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
				newAccount = iterator;
			}
			else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
				newPubAccount = iterator;
			}
			else {
				newTelcoAccount = iterator;
			}
		}
		system.assertNotEquals(newAccount.ID, null);
		system.assertNotEquals(newPubAccount.ID, null);
		system.assertNotEquals(newAccount.Primary_Canvass__c, null);
		system.assertNotEquals(newTelcoAccount.ID, null);
		Telco__c objTelco = new Telco__c();
		list<Telco__c> lstTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id];
		if(lstTelco.size() > 0) {
			for(Telco__c iterator : lstTelco) {
				iterator.Telco_Code__c = 'Test';
			}		
			update lstTelco;
			objTelco = lstTelco[0];
		}
		system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        System.runAs(u) {
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
			newOpportunity.AccountId = newAccount.Id;
			newOpportunity.Pricebook2Id = newPriceBook.Id;
			newOpportunity.Billing_Contact__c = newContact.Id;
			newOpportunity.Signing_Contact__c = newContact.Id;
			newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
			newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
			insert newOpportunity;
			system.assertNotEquals(newOpportunity, null);
			BMIQuoteSOQLMethods.getPrimaryBMIQuoteByOpportunityID(new set<Id>{newOpportunity.Id});
			BMIQuoteSOQLMethods.getBMIQuoteByOpportunityID(new set<Id>{newOpportunity.Id});
        }
    }
}