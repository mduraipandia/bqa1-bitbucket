/*public with sharing class TBCbCleanMonthlyCronRecords {

	private final sObject mysObject;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public TBCbCleanMonthlyCronRecords(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
    }

    public String getRecordName() {
        return 'Hello ' + (String)mysObject.get('name') + ' (' + (Id)mysObject.get('Id') + ')';
    }
}*/
global class TBCbCleanMonthlyCronRecords Implements Database.Batchable <sObject> {
	global Date dtBD;
	global Boolean dailyCronBool = false;
	
	global TBCbCleanMonthlyCronRecords(Date dtBillingDate) {
       dtBD = dtBillingDate;
    }
    
    global TBCbCleanMonthlyCronRecords(Date dtBillingDate, Boolean dailyCronBool) {
       dtBD = dtBillingDate;
       this.dailyCronBool = dailyCronBool;
    }    
    
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id FROM Monthly_Cron__c ';
        if(dtBD != null) {
        	SOQL += 'WHERE MC_OLI_Next_Billing_Date__c =: dtBD';
        }
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<Monthly_Cron__c> listMonthlyCron) {
    	delete listMonthlyCron;
    	Database.emptyRecycleBin(listMonthlyCron);
    }

    global void finish(Database.BatchableContext bc) {
    	if(dailyCronBool) {
    		MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(system.today() + 7, 'FMP', true);
        	Database.executeBatch(obj, 2000);
        	TBC_Cron__c CleanUpBatch = TBC_Cron__c.getInstance('CleanUpCron');    	
    		CleanUpBatch.Batch_Ran_Check__c = true;
    		update CleanUpBatch;
    	} else {
	    	TBC_Cron__c FMPBatch = TBC_Cron__c.getInstance('FMPCron');
	    	FMPBatch.Batch_Ran_Check__c = true;
	    	update FMPBatch;
	    	MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(system.today(), 'FMP', true, true); 
	    	Database.executeBatch(obj, 2000);
    	}
    }
}