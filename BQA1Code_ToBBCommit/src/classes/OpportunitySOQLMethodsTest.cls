@isTest(SeeAllData=True)
public class OpportunitySOQLMethodsTest{
@IsTest
    public static void OpportunitySOQLMethodsTest() {
        Account objAccount = TestMethodsUtility.createAccount('customer');
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount, objContact);
        Set<ID> setOppId = new Set<ID>();
        setOppId.add(objOpportunity.id);
        Test.startTest();
            System.assertNotEquals(null, OpportunitySOQLMethods.getOpportunityForPendingItem(objOpportunity.Id));
            System.assertNotEquals(null, OpportunitySOQLMethods.getOpportunityCanvassById(setOppId));
            System.assertNotEquals(null, OpportunitySOQLMethods.getOpportunityForNational(setOppId));
            System.assertNotEquals(null, OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(setOppId));
            System.assertNotEquals(null, OpportunitySOQLMethods.setOpportunityByIdForLockCloneCreateOrder(setOppId));
            //OpportunitySOQLMethods.getOpportunityBasedOnTransactionID('T1234');
            //OpportunitySOQLMethods.getOpportunityNationalByTransaction(new Set<String>{'T1234'});
            System.assertNotEquals(null, OpportunitySOQLMethods.getOpportunityByAcctIds(new set<Id>{objAccount.Id}));
            System.assertNotEquals(null, OpportunitySOQLMethods.getOpportunityForLockCloneCreateOrder(new List<Opportunity>{objOpportunity}));
        Test.stopTest();
    }
    }