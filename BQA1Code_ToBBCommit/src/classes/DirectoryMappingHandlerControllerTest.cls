@isTest
private class DirectoryMappingHandlerControllerTest {
    static testMethod void DMCanvassTest() {
    	Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Directory__c objDir = TestMethodsUtility.createDirectory();
        Directory__c objDir1 = TestMethodsUtility.createDirectory();
        Directory_Mapping__c objDirMapping = TestMethodsUtility.generateDirectoryMapping(telco);
        objDirMapping.Directory__c = objDir.Id;
        insert objDirMapping;
        objDirMapping.Directory__c = objDir1.Id;
        update objDirMapping;
    }
}