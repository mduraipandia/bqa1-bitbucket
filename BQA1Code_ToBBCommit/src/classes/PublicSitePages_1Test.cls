/************
This Test class will cover both PublicSitePages_1 and ExcelPage
************/
@isTest(seeAllData=true)
public class PublicSitePages_1Test {
	
	 static testMethod void test_method_one() {
		Test.startTest();

		//Create Directory Heading records
		Directory_Heading__c dirHdng1 = new Directory_Heading__c(Name='Test Directory Heading', code__c='2255');
		insert dirHdng1;

		Directory_Heading__c dirHdng2 = new Directory_Heading__c(Name='Test Directory Heading', code__c='5522');
		insert dirHdng2;


		//Create Cross Reference Heading records
		Cross_Reference_Headings__c crsRefHdng1 = new Cross_Reference_Headings__c(Cross_Reference_Heading__c=dirHdng1.Id, Source_Heading__c=dirHdng2.Id);
		insert crsRefHdng1;

		Cross_Reference_Headings__c crsRefHdng2 = new Cross_Reference_Headings__c(Cross_Reference_Heading__c=dirHdng2.Id, Source_Heading__c=dirHdng1.Id);
		insert crsRefHdng2;

		//Create Telco record
		Telco__c Tel = new Telco__c(Name='TestTelco');
		insert Tel;

		//Create Directory record
		Directory__c dirty = new Directory__c(Name='Test Directory', Telco_Provider__c=Tel.Id, Directory_Code__c='2525Test');
		insert dirty;

		//Create Directory Section record
		Directory_Section__c dirSec = new Directory_Section__c(Name='Test Directory Section', Section_Page_Type__c='YP', Directory__c=dirty.Id, Section_Code__c='5252Test');
		insert dirSec;

		//Create Section Heading Mapping record
		Section_Heading_Mapping__c secHdngMpng = new Section_Heading_Mapping__c(Name='Test Secction Heading Mapping', Directory_Section__c=dirSec.Id, Directory_Heading__c=dirHdng1.Id);
		insert secHdngMpng;

		//Pagereference to test Publicsitepages_1
		PageReference pageRef = Page.publicsitepages_1;
		Test.setCurrentPage(pageRef);
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(dirHdng1);
		ApexPages.currentPage().getParameters().put('Id', dirHdng1.Id);
		ApexPages.currentPage().getParameters().put('dirCode', dirty.Directory_Code__c);

		publicsitepages_1 pgStPg = new publicsitepages_1(sc);
        pgStPg.default();

        //Pagereference to test ExcelPage
        PageReference pageRef1 = Page.ExcelPage;
        Test.setCurrentPage(pageRef1);
        ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(dirty);
        ApexPages.currentPage().getParameters().put('Id', dirty.Id);
        ApexPages.currentPage().getParameters().put('dirCode', dirty.Directory_Code__c);

        ExcelPageController exclPgCntrl = new ExcelPageController();

        Test.stopTest();
	}
	
}