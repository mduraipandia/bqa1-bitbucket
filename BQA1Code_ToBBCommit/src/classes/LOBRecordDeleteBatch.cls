global class LOBRecordDeleteBatch Implements Database.Batchable <sObject> {
	Id selectedDirId;
    Id selectedDirEdId; 
    List<String> listSelBillingPartners = new List<String>();
    Map<Id, String> mapAcctIdPrimConPhone = new Map<Id, String>();
    String reportType = '';
    String invFromDate;
    String invToDate;  
    String selectedEntity;
    String editionCode;
    
	global LOBRecordDeleteBatch() {
		
	}
    
    global LOBRecordDeleteBatch(Id selectedDirId, Id selectedDirEdId, List<String> listSelBillingPartners, String reportType, String invFromDate, 
    							String invToDate, String selectedEntity, String editionCode) {
        this.selectedDirId = selectedDirId;
        this.selectedDirEdId = selectedDirEdId;
        this.listSelBillingPartners = listSelBillingPartners;
        system.debug('Testss : '+ listSelBillingPartners);
        this.reportType = reportType;
        this.invFromDate = invFromDate;
        this.invToDate = invToDate;
        this.selectedEntity = selectedEntity;
    }
	
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id FROM List_Of_Business__c';
        if(reportType == 'digital') {
	    	SOQL += ' WHERE LOB_Billing_Entity__c =: selectedEntity';
    	} else if(reportType == 'print') {
    		SOQL += ' WHERE LOB_Edition_Code__c =: editionCode';
    	}
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<List_Of_Business__c> listLOB) {
    	delete listLOB;
		Database.emptyRecycleBin(listLOB);
    }
 
    global void finish(Database.BatchableContext bc) {
    	if(reportType == 'digital') {
	    	LOBReportGenerationBatch lob = new LOBReportGenerationBatch(selectedDirId, selectedDirEdId, listSelBillingPartners, reportType, invFromDate, 
	    									invToDate, selectedEntity);
	    	Database.executeBatch(lob);
    	} else if(reportType == 'print') {
    		LOBJobRecordDeleteBatch LOBJobDel = new LOBJobRecordDeleteBatch(selectedDirId, selectedDirEdId, listSelBillingPartners);
    		Database.executeBatch(LOBJobDel);
    	}
    }
}