@isTest
private class  DummyObjRecordDeletionBatchTest
{
     public static testMethod void createDummyObjectRecords()
     {
        list<Dummy_Object__c> listDummyrec = new list<Dummy_Object__c> ();
        for(integer i=0;i<12;i++)
        {           
        
            Dummy_Object__c rec = TestMethodsUtility.createDummyObhjectrecords(2015,i,i);
            listDummyrec.add(rec);
        }
        insert listDummyrec;
        
        DummyObjRecordDeletionBatch  testcls = new DummyObjRecordDeletionBatch();
        ID batchprocessid = Database.executeBatch(testcls);
     }
    
}