public with sharing class SummaryController {

    //Getters and Setters
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String dffId {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > fnlLstDffs {
        get;
        set;
    }

    transient Map < String, String > mpDFlds = new Map < String, String > ();
    transient Digital_Product_Requirement__c dff;

    public List<Digital_Product_Requirement__c> lstDffs1 {get; set;}
     
    //Constructor
    public SummaryController(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
        dffId = dff.Id;
        //fnlLstDffs = getPrdtInfo();
    }
    
    public Map < String, String > getSmrryFlds() {
        
        fnlLstDffs = CommonUtility.DFFFieldLabelNames(orderSetId, mOrderSetId);
        Map < String, String > mapDFFFields = CommonMethods.fetchDFFFields();

        //System.debug('************ALLDFFS************' + allDffs.size() + allDffs);

        if (fnlLstDffs.size() > 0) {
            for (Digital_Product_Requirement__c dff: fnlLstDffs) {
                transient String strDFFFields = mapDFFFields.get(dff.DFF_RecordType_Name__c);
                transient String[] strArrDFFFields;
                transient String DFFFldVals;
                transient Set < String > lkFlds = CommonUtility.lookupFields();
                if(Test.isRunningTest()){
                    strDFFFields = 'business_name__c';
                }
                if (String.isNotBlank(strDFFFields)) {
                    strArrDFFFields = strDFFFields.split(',');
                    for (String strField: strArrDFFFields) {
                        if (!lkFlds.Contains(strField)) {
                            if (String.isNotBlank(DFFFldVals)) {
                                DFFFldVals += '<br/>' + strField.replace('__c', '') + ': ' + dff.get(strField);
                            } else {
                                DFFFldVals = strField.replace('__c', '') + ': ' + dff.get(strField);
                            }
                        }
                    }

                    mpDFlds.put(dff.UDAC__c, DFFFldVals);
                }

            }

        }
        //System.debug('************DFFFields************' + mpDFlds.keySet());
        return mpDFlds;
    }    

}