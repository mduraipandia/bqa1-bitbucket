global class TBCbScopedListingsSuppressionDataCleanup implements Database.Batchable<sObject>,Database.Stateful {
	global Database.QueryLocator start(Database.BatchableContext bc) {
		Date TodayDate = Date.today();
		set<string> setBookStatus = new set<string>{'BOTS-1','BOTS-2','BOTS-3'};
    	return database.getQuerylocator('SELECT Id,Suppressed_By_OLI__c,Suppressed__c,Directory__c FROM Directory_Listing__c where Suppressed_By_OLI__r.Directory_Edition_Book_Status__c IN : setBookStatus AND Directory__r.Dir_BOTS_Suppress_UnSuppress_OLI__c >=:TodayDate');
    }
    global void execute(Database.BatchableContext bc, List<Directory_Listing__c > dlList) {
    	list<Directory_Listing__c> lstDL = new list<Directory_Listing__c>();
    	if(dlList.size() > 0) {
            for(Directory_Listing__c iterator : dlList) {
                iterator.Suppressed__c = False;
                iterator.Suppressed_By_OLI__c = null;
                lstDL.add(iterator);
            }
            if(lstDL.size() > 0) {
                update lstDL;
            }
        }
    }
    global void finish(Database.BatchableContext bc){}
}