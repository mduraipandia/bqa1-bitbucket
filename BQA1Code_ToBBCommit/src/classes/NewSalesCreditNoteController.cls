public with sharing class NewSalesCreditNoteController {
	public c2g__codaCreditNote__c newSCR {get;set;}
	
	public NewSalesCreditNoteController(ApexPages.StandardController controller) {
		newSCR = new c2g__codaCreditNote__c();
    }
    
    public PageReference insertSCR() {
    	try {
    		newSCR.Is_Manually_Created__c = true;
    		insert newSCR;
    		PageReference pg = new PageReference('/' + newSCR.Id);
    		pg.setRedirect(true);
    		return pg;
    	} catch(Exception e) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
    		return null;
    	}
    }
}