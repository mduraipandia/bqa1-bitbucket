global class BillingSubsequentDigitalScheduler Implements Schedulable {
   public Interface BillingSubsequentDigitalSchedulerInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('BillingSubsequentDigitalSchedulerHndlr');
        if(targetType != null) {
           BillingSubsequentDigitalSchedulerInterface obj = (BillingSubsequentDigitalSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}