public class PaymentReceipt {
public string invoiceID {get;set;}
    public String initialPayment {get;set;}
    public boolean bDateRangeFlag {get;set;}
    public Date servEnd{get;set;}
    public string errortext{get;set;}
    public boolean isCreditCard{get;set;}
    public boolean isECheck{get;set;}
    public Canvass__c objCanvass {get;set;}
    public c2g__codaInvoice__c objInvoice {get;set;}    
    public Opportunity objOpportunity {get;set;}
    public Account objAccount {get;set;}
    public Contact objContact {get;set;}
    public pymt__PaymentX__c objPayment{get;set;}
    public list<c2g__codaInvoiceLineItem__c> lstILI {get;set;}
    
    public PaymentReceipt(ApexPages.StandardController controller) {
       
        objPayment = new pymt__PaymentX__c();
        objAccount = new Account();
        objContact = new Contact();
        objCanvass = new Canvass__c();
        objOpportunity = new Opportunity();
        Map<Id,pymt__PaymentX__c> mapPayment=new Map<Id,pymt__PaymentX__c>(); 
        initialPayment = ApexPages.currentPage().getParameters().get('ipymt');
        if(String.isNotEmpty(initialPayment)) 
        {
            objPayment=[Select pymt__Opportunity__c,pymt__Opportunity__r.AccountID,pymt__Amount__c,pymt__Check_Number__c,pymt__Last_4_Digits__c,pymt__Authorization_Id__c,pymt__Status__c,pymt__Card_Type__c,pymt__Contact__r.name,pymt__Payment_Type__c from pymt__PaymentX__c where id=:initialPayment];
            objOpportunity=new Opportunity(id=objPayment.pymt__Opportunity__c);
            map<Id, Account> mapAccount = AccountSOQLMethods.getContactOpportunityByAccountId(new set<Id>{objPayment.pymt__Opportunity__r.AccountID});
            objAccount =mapAccount.get(objPayment.pymt__Opportunity__r.AccountID);
             makePDF();
        }
      
       
    }
    
    
    
    public void makePDF() {
       
            list<pymt__PaymentX__c> lstPayment = new list<pymt__PaymentX__c>();            
            set<Id> accountID = new set<Id>();
            //For ACH AND Credit Card Billing
               
                 pymt__PaymentX__c paymentX=[Select pymt__Payment_Type__c from pymt__PaymentX__c where id=:objPayment.id];
                 
                 if(paymentX.pymt__Payment_Type__c =='Credit Card')
                 {
                    isCreditCard=true;
                    isECheck=false;
                 }
                 else if(paymentX.pymt__Payment_Type__c =='ECheck')
                 {
                    isCreditCard=false;
                    isECheck=true;
                 }
                 else
                 {
                 
                 }
                               
                //Updation for ACH & Credit Card End Here
       
    }

}