@isTest(SeeAllData=true)
public class UpdateDFFTest 
{
    public static testMethod void updtTest () 
    {

       Product2 prdt = CommonUtility.createPrdtTest();
       prdt.Product_Type__c = 'Video';
       insert prdt;
             
       Canvass__c c = CommonUtility.createCanvasTest();
       insert c;
        
       Account a = CommonUtility.createAccountTest(c);
       insert a;

       Opportunity oppty = CommonUtility.createOpptyTest(a);
       insert oppty;
        
       Contact cnt = CommonUtility.createContactTest(a);
       insert cnt;           

       Order__c ord = CommonUtility.createOrderTest(a);
       insert ord;
        
        
       Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
       insert og;
        
       Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
       insert oln;

       Fulfillment_Profile__c fp = CommonUtility.createFpTest(a);
       insert fp;
              
       Digital_Product_Requirement__c DFF = CommonUtility.createDffTest();
       DFF.Fulfillment_Profile__c = fp.id;
       DFF.Account__c = a.id;
       DFF.DFF_Product__c = prdt.Id;
       insert DFF;
       
       Test.startTest();

       ApexPages.currentPage().getParameters().put('Id', DFF.id);
       ApexPages.StandardController sc1 = new ApexPages.StandardController(DFF);
       UpdateDFF updtDff = new UpdateDFF(sc1);
       updtDff.fpLoad(); 
       updtDff.updateDffFp();  
        
       Test.stopTest();
       
    }
/*    
    public static testMethod void TestUpdateDFF1 () 
    {
     
       Account Acc2 = TestMethodsUtility.generateAccount('customer');
       insert Acc2;
       Fulfillment_Profile__c fp=TestMethodsUtility.createFulfillmentProfile(Acc2);
       insert fp;
       Digital_Product_Requirement__c dpr=TestMethodsUtility.generateDataFulfillmentForm();
       dpr.Fulfillment_Profile__c=fp.id;
       insert dpr;
       
       Test.startTest();
       PageReference pageRef = new PageReference('/apex/UpdateDFF?id='+dpr.id);
       Test.setCurrentPage(pageRef);    
       ApexPages.StandardController dffctrl = new ApexPages.standardController(new Digital_Product_Requirement__c());
       UpdateDFF uDFF=new UpdateDFF(dffctrl);
      // uDFF.onLoad();
      // uDFF.updateDFFNew();
      // uDFF.updateCurrentDFF();
     //  uDFF.updateFP();
      // uDFF.cancelDFF();
       //uDFF.updateFFfromDFF(dpr,fp);  
       
       PageReference pageRef2 = new PageReference('/apex/ReadyToUpdateDFF?id='+dpr.id+'&pid='+fp.id);
       Test.setCurrentPage(pageRef2);  
       ApexPages.StandardController dffctrl2 = new ApexPages.standardController(new Digital_Product_Requirement__c());
       ReadyToUpdateDFF updatedff=new ReadyToUpdateDFF(dffctrl2);
      // updatedff.onload();
       Test.stopTest();
       
    }
*/      
    
}