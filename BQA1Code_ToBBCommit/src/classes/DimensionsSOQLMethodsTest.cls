@IsTest
public class DimensionsSOQLMethodsTest {
	static testmethod void DimensionsSOQLMethodsCoverage() {
		c2g__codaDimension1__c dimension1 = TestMethodsUtility.createDimension1('test', 'test');
        c2g__codaDimension2__c dimension2 = TestMethodsUtility.createDimension2(new Product2(Name = 'test', RGU__c = 'Print'));
        c2g__codaDimension3__c dimension3 = TestMethodsUtility.createDimension3(new Order_Line_Items__c(Billing_Partner__c = CommonMessages.berryTelcoName));
        c2g__codaDimension4__c dimension4 = TestMethodsUtility.createDimension4();
        system.assertNotEquals(dimension4, null);
        Set<String> reportingCode = new Set<String>();
        reportingCode.add('test');
        DimensionsSOQLMethods.fetchDimensions1(reportingCode);
        DimensionsSOQLMethods.fetchDimensions2(reportingCode);
        DimensionsSOQLMethods.fetchDimensions3(reportingCode);
        DimensionsSOQLMethods.fetchDimensions4(reportingCode);
	}
}