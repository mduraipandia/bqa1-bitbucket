@isTest(seeallData=true)
Public class BillingSettlement_AIUTest{
    static testmethod void myunittest(){
         Test.StartTest();
           Account objA = new Account();
               objA.Name = 'Test Account';
               objA.Phone ='(923) 456-7890';
               objA.CMR_Number__c = '123456';
               objA.P4P_Spending_Cap_Type__c = 'test p4p';
               objA.P4P_Spending_Cap__c = 'Test';
               
               Insert objA;  
               
               /*Directory__c objD = new Directory__c();
               objD.Name = 'Test Directory';
               Insert objD;*/
               
                Directory__c objD = TestMethodsUtility.createDirectory();
               
               set<Id> setIds = new set<Id>();
               Directory_Edition__c objDE = new Directory_Edition__c();
               objDE.Directory__c = objD.Id;
               objDE.Book_Status__c = 'BOTS';
               objDE.setup_Cost__c = 123;
               objDE.Printing_Cost__c = 321;
               objDE.Other_Cost__c  = 456;
               
               
               Insert objDE;
               setIds.add(objDE.Id);
               
               billing_settlement__c objBS = new billing_settlement__c();
               objBS.Name = 'test';
               objBS.Directory__c = objD.Id;
               objBS.Directory_Edition__c = objDE.Id;
               objBS.Telco_Cost_Share__c=123;
               Insert objBS;
               
               BillbackInvoiceSetllementController objB = new BillbackInvoiceSetllementController();
               objB.billbackInsert(setids);
               
               
         Test.stopTest();
       }
 }