global without sharing class Miles33SpecAdXML {
/**************************************************************************************************
Program:            Used to formulate the XML needed from Spec Ads to transmit to Miles 33 Production Tracking
Programmer:         Joe Henry  - jhenry38@csc.com
Date:               August 14, 2013
Rev:                May 20, 2014 LSeverietti: Modified field passing to Miles as Title old: Section_Code__c  new: Directory_Code__c
                     June 2, 2014 LSeverietti: Added if clause, check if Heading is <> Null before check to see if contains &
***************************************************************************************************/ 
    webservice static String Miles33XML(id SpecId) {
        string xmlCode;
        
        list<Spec_Art_Request__c> lstYPCSAR = [Select Id, Account__r.name, Account__r.Berry_ID__c, Account__r.Website, CreatedDate, Name,Telephone_Prefix__c,Telephone_Areacode__c,Advertised_Business_Potalcode__c,DirectoryHeading__c,
                                                Contact_Email__c, Telephone_number__c, Advertised_Listed_City__c, SAR_User_NickName__c, SAR_Advertised_Listed_Country__c, Miles33_ImportAdvert__c, 
                                                Sequence__c, pgroup__c, Description_of_Artwork_to_be_completed__c, Advertised_Listed_Phone_Number__c, Advertised_Listed_State__c,
                                                Directory_Code__c, Colors__c,Advertised_Listed_Address__c, Directory_Section__r.DS_M33_Graphics_Geometry_Value__c, Miles_Status__c, 
                                                Account__c, Directory_Section__r.Section_Page_Type__c, Product__r.ProductCode, Edition__r.Edition_Code__c
                                                From Spec_Art_Request__c d where Id = :SpecId];
                    
        for(Spec_Art_Request__c SR : lstYPCSAR) {
            String strPTStatusCode = '';
            if(String.isNotBlank(SR.Miles33_ImportAdvert__c)) {
                SR.Miles_Status__c = 'Change';
                strPTStatusCode = 'CH';
            }
            else {
                SR.Miles_Status__c = 'New';
                strPTStatusCode = 'NE';
            }
            xmlCode = '<?xml version="1.0" encoding="utf-8"?>';
            xmlCode += '<FutureProofAdvert>';
            xmlCode += '<Order_Record>';
            xmlCode += '<Customer>';
            xmlCode += '<Name>'+ SR.Account__r.Name +'</Name>';
            xmlCode += '<URN_text>'+SR.Account__r.Berry_ID__c+'</URN_text>';
            xmlCode += '<Main_Address>';
            xmlCode += '<Address>'+SR.Advertised_Listed_Address__c+'</Address>';
            xmlCode += '<Town>'+SR.Advertised_Listed_City__c+'</Town>';
            xmlCode += '<County>'+SR.Advertised_Listed_State__c+'</County>';
            xmlCode += '<Country>'+SR.SAR_Advertised_Listed_Country__c+'</Country>';
            xmlCode += '<Postcode>'+SR.Advertised_Business_Potalcode__c+'</Postcode>';
            xmlCode += '</Main_Address>';
            xmlCode += '<Main_Email>';
            xmlCode += '<Email_address></Email_address>';
            xmlCode += '<Email_setting>EML</Email_setting>';
            xmlCode += '</Main_Email>';
            xmlCode += '<Main_Web>'; //<!-- OPTIONAL -->
            xmlCode += '<Email_address>'+ SR.Account__r.Website +'</Email_address>';
            xmlCode += '<Email_setting>WEB</Email_setting>';
            xmlCode += '<Comments></Comments>'; // <!-- OPTIONAL -->
            xmlCode += '</Main_Web>';
            xmlCode += '<Main_Person>';
            xmlCode += '<Surname></Surname>';
            xmlCode += '<Forenames></Forenames>';
            /*xmlCode += '<Main_Address>';
            xmlCode += '<Address>'+SR.Advertised_Listed_Address__c+'</Address>';
            xmlCode += '<Town>'+SR.Advertised_Listed_City__c+'</Town>';
            xmlCode += '<County>'+SR.Advertised_Listed_State__c+'</County>';
            xmlCode += '<Country>'+SR.SAR_Advertised_Listed_Country__c+'</Country>';
            xmlCode += '<Postcode>'+SR.Advertised_Business_Potalcode__c+'</Postcode>';
            xmlCode += '</Main_Address>';
            xmlCode += '<Main_Email />';
            xmlCode += '<Main_Phone>';
            xmlCode += '<Telephone_prefix>'+SR.telephone_prefix__c+'</Telephone_prefix>';
            xmlCode += '<Telephone_std>'+SR.Telephone_Areacode__c+'</Telephone_std>';
            xmlCode += '<Telephone_number>'+SR.Telephone_number__c+'</Telephone_number>';
            xmlCode += '</Main_Phone>';*/
            xmlCode += '</Main_Person>';
            xmlCode += '<Main_Phone>';
            xmlCode += '<Telephone_prefix>'+SR.Telephone_prefix__c+'</Telephone_prefix>';
            xmlCode += '<Telephone_std>'+SR.Telephone_Areacode__c+'</Telephone_std>';
            xmlCode += '<Telephone_number>'+SR.Telephone_number__c+'</Telephone_number>';
            xmlCode += '</Main_Phone>';
            xmlCode += '</Customer>';
            xmlCode += '<Negotiation_Currency>USD</Negotiation_Currency>';
            xmlCode += '<URN_text>'+SR.Name+'</URN_text>';
            xmlCode += '<Creator>';
            xmlCode += '<Log_on_name>'+ SR.SAR_User_NickName__c +'</Log_on_name>';
            xmlCode += '</Creator>';
            xmlCode += '<Create_time>'+SR.CreatedDate+'</Create_time>';                                
            xmlCode += '<Operator>';
            xmlCode += '<Log_on_name>'+ sr.SAR_User_NickName__c +'</Log_on_name>';
            xmlCode += '</Operator>';
            xmlCode += '<Order_Misc_Data>';
            //Commneted out by sathish - ATP-4464
            //xmlCode += '<Misc_Data Key="SPEC">';
            xmlCode += '<Misc_Data>';
            xmlCode += '<Name>Ad Type</Name>';
            xmlCode += '<Data_type>CBX</Data_type>';
            xmlCode += '</Misc_Data>';
            //Added by sathish - ATP-4464  -- Start
            xmlCode += '<Data_string>SPEC</Data_string>';
            //Added by sathish - ATP-4464  -- End
            xmlCode += '</Order_Misc_Data>';
            xmlCode += '<Order_Misc_Data>';
            xmlCode += '<Misc_Data Key="'+SR.Name+'">';
            xmlCode += '<Name>OrderNo</Name>';
            xmlCode += '<Data_type>STR</Data_type>';
            xmlCode += '</Misc_Data>';
            xmlCode += '<Data_string>'+SR.Id+'</Data_string>';
            xmlCode += '</Order_Misc_Data>';
            xmlCode += '<Order_Row><Sequence>'+SR.Sequence__c+'</Sequence>';
            xmlCode += '<Order_Row_Amend>true</Order_Row_Amend>';
            xmlCode += '<Order_Row_Ad_Status>'+ strPTStatusCode +'</Order_Row_Ad_Status>';           
            xmlCode += '<Title>';
            xmlCode += '<Name>'+SR.Directory_Code__c +'</Name>';
            xmlCode += '</Title>';
            xmlCode += '<Pgroup>';
            xmlCode += '<Name>'+ SR.Directory_Section__r.Section_Page_Type__c +'</Name>';
            xmlCode += '</Pgroup>';
            xmlCode += '<Class>';
            xmlCode += '<Name>'+ SR.DirectoryHeading__c +'</Name>';
            xmlCode += '</Class>';
            xmlCode += '<Style>';
            xmlCode += '<Name>'+SR.Product__r.ProductCode+'</Name>';
            xmlCode += '</Style>';
            xmlCode += '<Size>';
            xmlCode += '<Name>'+SR.Product__r.ProductCode+'</Name>';
            xmlCode += '</Size>';
            xmlCode += '<Colour>';
            xmlCode += '<Name>'+SR.Colors__c+'</Name>';
            xmlCode += '</Colour>';
            xmlCode += '<Position>';
            xmlCode += '<Name>Default</Name>';
            xmlCode += '</Position>';
            xmlCode += '<Geometry>';
            xmlCode += '<Name>'+ SR.Directory_Section__r.DS_M33_Graphics_Geometry_Value__c +'</Name>';
            xmlCode += '</Geometry>';
            xmlCode += '<Order_Text><RTF_Text />';
            xmlCode += '<Production_Notes>'+SR.Description_of_Artwork_to_be_completed__c+'</Production_Notes>';
            xmlCode += '</Order_Text>';
            xmlCode += '<Order_Insert>';
            xmlCode += '<Issue_number>'+SR.Edition__r.Edition_Code__c+'</Issue_number>';
            xmlCode += '<Insert_net_price>0</Insert_net_price>';
            xmlCode += '<Insert_gross_price>0</Insert_gross_price>';
            xmlCode += '</Order_Insert>';
            xmlCode += '</Order_Row>';
            xmlCode += '</Order_Record>';
            xmlCode += '</FutureProofAdvert>';

            xmlCode = xmlCode.replaceAll('>null<', '><');
            xmlCode = xmlCode.replaceAll('>"', '>');
            xmlCode = xmlCode.replaceAll('"<', '<');
            xmlCode = xmlCode.replaceAll('&','&amp;');
            SR.Miles33_ImportAdvert__c = xmlCode;
            }
            try{
                if(lstYPCSAR.size() > 0) {
                    update lstYPCSAR;
                    return 'Successful update';
                }                    
                return 'Error';                    
            }
            catch(System.DMLException e) {
                System.assert(false, e.getMessage());
                return e.getMessage();
            }                               
    
        return null;
    }   
}