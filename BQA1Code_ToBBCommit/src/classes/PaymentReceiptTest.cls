@isTest
public with sharing class PaymentReceiptTest {
	static testMethod void pymtTest(){
		Test.startTest();
		Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
		pymt__PaymentX__c pymt = TestMethodsUtility.generatePaymentXWithoutSalesInvoice(oppty, 200);
		pymt.pymt__Payment_Type__c ='Credit Card';
		insert pymt;
		apexpages.currentpage().getparameters().put('ipymt' , pymt.Id);
		PaymentReceipt PR = new PaymentReceipt(new ApexPages.StandardController(pymt));
		pymt.pymt__Payment_Type__c ='ECheck';
		update pymt;
		PaymentReceipt PR1 = new PaymentReceipt(new ApexPages.StandardController(pymt));
		Test.stopTest();
	}
}