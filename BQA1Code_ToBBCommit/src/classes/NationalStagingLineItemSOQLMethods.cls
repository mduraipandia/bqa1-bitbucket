public class NationalStagingLineItemSOQLMethods {    
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLIListForTransactionByNSOSId(Id NSOSId) {
        return [SELECT Is_Processed__c,Action__c, Advertising_Data__c,Needs_Review__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c,Standing_Order__c , 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c, 
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and New_Transaction__c = true order by Line_Number__c ASC,CreatedDate desc];
    }   
    
    public static List<National_Staging_Line_Item__c> getNSLIForTNXHTransactionByNSOSId(Id NSOSId,String TransType) {
       List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
       if(TransType=='T' || TransType=='N' || TransType=='H'){
        ListNSLI=[SELECT Is_Processed__c,Action__c, Advertising_Data__c,Needs_Review__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c,Standing_Order__c , 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c,Transaction_Id__c,
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and New_Transaction__c = true order by Transaction_Id__c ASC,CreatedDate desc];
        }
        else{
         ListNSLI=[SELECT Is_Processed__c,Action__c, Advertising_Data__c,Needs_Review__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c,Standing_Order__c , 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c,Transaction_Id__c,
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and New_Transaction__c = true order by Line_Number__c ASC,CreatedDate desc];
         
        
        }
        return ListNSLI;
    }
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLIListForStadingOrderByNSOSId(Id NSOSId) {
        return [SELECT Action__c, Advertising_Data__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c, Is_Processed__c, 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c, 
                Standing_Order__c, National_Staging_Header__r.name,Needs_Review__c,
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and Standing_Order__c = true order by Line_Number__c];
    }    
    
    
    public static List<National_Staging_Line_Item__c> getNSLIForStadingOrderTNXHByNSOSId(Id NSOSId,String TransType) {
       List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
       if(TransType=='T' || TransType=='N' || TransType=='H'){
           ListNSLI=[SELECT Action__c, Advertising_Data__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c, Is_Processed__c, 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c, 
                Standing_Order__c, National_Staging_Header__r.name,Needs_Review__c,
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and Standing_Order__c = true order by Transaction_Id__c];
       }
       else{
       ListNSLI=[SELECT Action__c, Advertising_Data__c, BAS__c, DAT__c, Discount__c,National_Discount__c, Full_Rate_f__c, New_Transaction__c, CreatedDate, 
                Line_Number__c, Line_Error_Description__c, National_Staging_Header__c, Name, Parent_ID__c, Product2__c, Is_Processed__c, 
                Ready_for_Processing__c, Id, Row_Type__c, SPINS__c, Sales_Rate__c, Transaction_Unique_ID__c, Type__c, UDAC__c, 
                Standing_Order__c, National_Staging_Header__r.name,Needs_Review__c,
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                National_Staging_Line_Item__c, Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)
                FROM National_Staging_Line_Item__c WHERE National_Staging_Header__c = : NSOSId and Standing_Order__c = true order by Line_Number__c];
       }
       return ListNSLI;
    }
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLIListByNSLIId(set<Id> NSLIId){
        return [SELECT Advertising_Data__c, BAS__c, DAT__c, Directory_Heading__c, Discount__c, National_Discount__c,Full_Rate_f__c, Id, Rate__c, Type__c,
                Line_Error_Description__c,Line_Number__c,Listing__c, National_Staging_Header__c, Parent_ID__c,Product2__c, Product2__r.Print_Product_Type__c,
                Ready_for_Processing__c, Row_Type__c,Sales_Rate__c, Scoped_Listing__c, SPINS__c, Transaction_Unique_ID__c, Local_Full_Rate__c, Name,
                National_Staging_Header__r.Directory__r.IsCompanion__c, National_Staging_Header__r.Directory__r.National_Rate_Up_for_Companion__c,
                Transaction_Version_ID__c, UDAC__c, National_Pricing__r.Exclude_National_Upcharge__c FROM National_Staging_Line_Item__c 
                WHERE ID IN: NSLIId];
    }
    
    
    public static map<Id, National_Staging_Line_Item__c> getNationalStagingLineItemByIds(set<Id> setIDs){
        return new map<Id, National_Staging_Line_Item__c>([Select UDAC__c, Type__c, Transaction_Unique_ID__c, Scoped_Listing__c, Sales_Rate__c, SPINS__c, Row_Type__c, Ready_for_Processing__c, 
                Product2__c, Parent_ID__c, National_Staging_Header__c, Name, Listing__c,National_Discount__c, Line_Number__c, Line_Error_Description__c, 
                Id, Full_Rate_f__c, Discount__c, DAT__c, BAS__c, Auto_Number__c, Advertising_Data__c, Action__c, 
                (Select Id From Opportunity_Product__r), 
                (Select Id From National_Reference_Object__r) 
                From National_Staging_Line_Item__c WHERE Id IN :setIDs]);
    }    
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLineItemListByNSOSIds(set<Id> NSOSIds){
        return [Select Action__c, Advertising_Data__c, Auto_Number__c, BAS__c, CORE_Migration_ID__c, DAT__c, Delete__c, Directory_Heading__c, 
                Discount__c, Transaction_Line_Id__c, Transaction_Version_ID__c ,Type__c, UDAC__c,National_Discount__c, 
                Full_Rate_f__c, Line_Number__c, Line_Error_Description__c, Listing__c, National_Staging_Header__c, Product2__c, 
                Ready_for_Processing__c, Row_Type__c, Sales_Rate__c, Scoped_Listing__c, SEQ__c,SPINS__c, Transaction_Id__c,Is_UDAC_Changed__c 
                From National_Staging_Line_Item__c WHERE National_Staging_Header__c IN :NSOSIDs];
    }
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLineItemWithOPPLineItem(set<Id> NSOSIds){
        return [Select UDAC__c, Type__c, Transaction_Version_ID__c,TradeMark_Parent_Id__c, Transaction_Unique_ID__c, Transaction_Line_Id__c, Transaction_Id__c, Standing_Order__c, 
        Scoped_Listing__c, Sales_Rate__c,  SPINS__c, SEQ__c, SAC_Date__c, Ready_for_Processing__c, RAC_Date__c, Product2__c, Seniority_Date__c,National_Staging_Header__r.RecordType.Name,
        Processed_Order_Line_Item__c, Parent_ID__c, New_Transaction__c, Needs_Review__c,National_Staging_Header__r.IsMigratedXtrans__c,National_Staging_Header__r.TRANS_Code__c, National_Staging_Header__c, National_Pricing__c, 
        National_Graphics_ID__c, Listing__c,National_Discount__c, Line_Number__c, Is_Processed__c, Is_Changed__c, Id, Full_Rate_f__c, Discount__c, Directory__c, 
        Directory_Section__c, Directory_Heading__c, Directory_Code__c, DAT__c, CreatedDate, Caption_Phone_Number__c, Caption_Indent_Order__c, 
        Caption_Indent_Level__c, Caption_Header__c, Caption_Display_Text__c, BAS__c, Advertising_Data__c, Action__c,Is_UDAC_Changed__c, (Select Id, OpportunityId, 
        PricebookEntryId, Quantity, Discount, Subtotal, TotalPrice, UnitPrice, ListPrice, ServiceDate, Billing_Duration__c, National_Staging_Line_Item__c,
        UDAC__c, Section_Code__c, Type__c, Heading_Code__c, Listing__c, Advertising_Data__c, Action__c, Display_Ad__c, Heading__c, Geo_Type__c, Geo_Code__c, 
        Distribution_Area__c, Directory_Heading__c, CMR_Number__c, CMR_Name__c, Date__c, Directory__c, Client_Name__c, Directory_Edition__c, Duration__c, 
        Effective_Date__c, From__c, Full_Rate__c, Geo_Name__c, Line_Number__c, NAT_Client_ID__c, NAT__c, Publication_Date__c, Start_Date__c, To__c, Trans__c, 
        Transaction_ID__c, Client_Number__c, ProductCode__c, Product_Type__c, Listing_Id__c, BAS__c, DAT__c, SPINS__c, Trans_Version__c, Seniority_Date__c,
        OP_National_Graphics_ID__c, Publication_Company__c,Trade_Mark_Parent_Id__c, Publication_Code__c, Category__c, Listing_Name__c, Directory_Section__c, UDAC_Group__c, Line_Unique_Id__c From 
        Opportunity_Product__r) From National_Staging_Line_Item__c where UDAC__c != null and National_Staging_Header__c IN:NSOSIds and Is_Changed__c = true];       
    }
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLineItemReference(set<Id> NSOSIds){
        return [Select Id, New_Transaction__c,National_Staging_Header__r.RecordType.Name,Line_Error_Description__c,National_Staging_Header__r.IsMigratedXtrans__c,National_Staging_Header__r.TRANS_Code__c,National_Staging_Header__c,Is_UDAC_Changed__c From National_Staging_Line_Item__c 
        where UDAC__c = null and National_Staging_Header__c IN:NSOSIds and New_Transaction__c = true];       
    }
    
    public static List<National_Staging_Line_Item__c> getNationalStagingLIListByNSLI(List<National_Staging_Line_Item__c> listNSLI) {
        return [SELECT Advertising_Data__c, BAS__c, DAT__c, Directory_Heading__c, Discount__c, National_Discount__c,Full_Rate_f__c, Id, Rate__c, Type__c,
                Line_Error_Description__c,Line_Number__c,Listing__c, National_Staging_Header__c, Parent_ID__c,Product2__c, Product2__r.Print_Product_Type__c,
                Ready_for_Processing__c, Row_Type__c,Sales_Rate__c, Scoped_Listing__c, SPINS__c, Transaction_Unique_ID__c, Local_Full_Rate__c, Name,
                National_Staging_Header__r.Directory__r.IsCompanion__c, National_Staging_Header__r.Directory__r.National_Rate_Up_for_Companion__c,
                National_Staging_Header__r.Directory__r.Months_for_National__c,
                Transaction_Version_ID__c, UDAC__c, National_Pricing__r.Exclude_National_Upcharge__c FROM National_Staging_Line_Item__c 
                WHERE ID IN: listNSLI];
    }
}