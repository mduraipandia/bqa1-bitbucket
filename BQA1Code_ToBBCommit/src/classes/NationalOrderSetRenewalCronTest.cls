@IsTest
public class NationalOrderSetRenewalCronTest{

  public static testMethod void NationalOrderSetRenewalCrontestMethod(){
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        objDirEd.Directory__c = objDir.Id;
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        
        Directory_Edition__c iterator=[Select Id,Future_Order_Start_Date__c from Directory_Edition__c  where Id=:objDirEd.Id];
        
        Test.startTest(); 
        
        NationalOrderSetRenewalCron sh1 = new NationalOrderSetRenewalCron(iterator.Future_Order_Start_Date__c );      
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        
        Test.StopTest();
  }
  /*
  public static testMethod void NOSRCrontestMethodPositive(){
        Canvass__c c=TestMethodsUtility.generateCanvass();
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=date.today();
        objDirEd.Directory__c = objDir.Id;
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        objNSOS.Is_Converted__c=true;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        
        Test.startTest(); 
        List<Directory_Edition__c> ListDE=[Select id,Name,Directory__c,(Select id from National_Staging_Order_Sets__r) from Directory_Edition__c where Id=:objDirEd.Id AND Book_Status__c ='BOTS'];
        
        NationalOrderSetRenewalCron sh1 = new NationalOrderSetRenewalCron(date.today()); 
        Database.BatchableContext bc;
        sh1.execute(bc, ListDE);    
        
        Test.StopTest();
  }
  */
  public static testMethod void NationalOrderSetRenewalHandlertestMethod(){
        Recordtype rtId = [SELECT DeveloperName,Id,Name FROM RecordType where Name = 'Daily Service Orders'];
        
        Schema.DescribeSObjectResult RDList = Directory_Listing__c.SObjectType.getDescribe(); 
        List<Schema.RecordTypeInfo> RTDList = RDList.getRecordTypeInfos(); 
        
        map<String,String> mapdirListRecordTypes = new map<String,String>(); 
        for(Schema.RecordTypeInfo R : RTDList){ 
                mapdirListRecordTypes.put(R.getName(),R.getRecordTypeId()); 
        }
        Account telcoAcc = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(telcoAcc.Id);
        Canvass__c objCanvss = TestMethodsUtility.generateCanvass(telco);
        objCanvss.Name = 'Unit Test Canvass';
        objCanvss.co_brand_name__c = 'Test';
        objCanvss.IsActive__c = true;
        insert objCanvss;
        
        Canvass__c c = TestMethodsUtility.generateCanvass(telco);
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        /*Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;*/
        Directory__c objDir =TestMethodsUtility.createDirectory();  
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd.New_Print_Bill_Date__c=date.today();
        objDirEd.Directory__c = objDir.Id;
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.book_status__c='BOTS';
        objDirEd.Pub_Date__c=date.today();
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Trademark_Product__c='Trademark Finding Line';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        //PricebookEntry pbe = [SELECT ID  FROM PricebookEntry WHERE IsActive = true Limit 1];
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        OpportunityLineItem oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=newOpportunity.Id, Quantity=1, TotalPrice=99);
        oli.Directory_Edition__c=objDirEd.Id;
        insert oli;
        
        OpportunityLineItem oli1 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=newOpportunity.Id, Quantity=1, TotalPrice=99);
        oli1.Directory_Edition__c=objDirEd.Id;
        insert oli1;
        
        OpportunityLineItem oli2 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=newOpportunity.Id, Quantity=1, TotalPrice=99);
        oli2.Directory_Edition__c=objDirEd.Id;
        insert oli2;
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        objNSOS.Directory_Edition__c =objDirEd.Id;
        objNSOS.Is_Converted__c=true;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=objProd.Id;
        insert objNSLI;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=objProd1.Id;
        objNSLI1.Parent_ID__c=objNSLI.Id;
        insert objNSLI1;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        objNSLI2.Parent_ID__c=objNSLI.Id;
        insert objNSLI2;
        ListNSLI.add(objNSLI2);
       
        newOpportunity.National_Staging_Order_Set__c=objNSOS.id;
        update newOpportunity;
        
        newOrderSet.National_Staging_Order_Set__c=objNSOS.id;
        update newOrderSet;
        
        List<OpportunityLineItem> ListOppli=new List<OpportunityLineItem>();
        integer iCount=0;
        for(OpportunityLineItem iterator:[Select Id,National_Staging_Line_Item__c from OpportunityLineItem where OpportunityId=:newOpportunity.Id]){
           iterator.National_Staging_Line_Item__c=ListNSLI[iCount].Id;
           iCount++;
           ListOppli.add(iterator);
        }
        
        update ListOppli;
        
        service_order_stage__c objSO = new service_order_stage__c();
        objSO.Name = 'Test SO XXX';
        objSO.Action__c = 'New';
        objSO.Phone__c = '(288) 451-1021';
        objSo.Disconnected__c = false;
        objSO.Bus_Res_Gov_Indicator__c = 'Business';
        objSO.BOID__c = 1234;
        objSO.Directory__c = objDir.Id;
        objSO.Indent_Order__c=0;
        objSO.Indent_Level__c=1;
        objSO.Caption_Header__c = true;
        objSO.Caption_Member__c = false;
        objSO.Directory_Section__c = objDS.Id;
        objSO.recordtypeId = rtId.Id;
        objSO.Telco_Provider__c=objTelco .id;
        objSO.BEX__c=2;
        objSO.Cross_Reference_Text__c='Test';
        objSO.Designation__c='SEO';
        objSO.Honorary_Title__c='TEST';
        objSO.Lineage_Title__c='TEST123';
        objSO.Listing_City__c='NY';
        objSO.Listing_State__c='TX';
        objSO.Listing_Street_Number__c='311';
        objSO.Listing_Street__c='St. Lane';
        objSO.Listing_Country__c='USA';
        objSO.Listing_PO_Box__c='232';
        objSO.Listing_Postal_Code__c='101';
        objSO.Listing_Street__c='NX';
        objSO.BOID__c=4;
        objSO.Bus_Res_Gov_Indicator__c='Government';
        objSO.Effective_Date__c=Date.parse('01/01/2013');
        objSO.First_Name__c='Test';
        objSO.SOS_LastName_BusinessName__c='TEST';
        insert objSO;   
        
        Listing__c objL = new Listing__c();
        objL.Name = objSO.name; 
        objL.Phone__c = objSO.Phone__c; 
        objL.Telco_Provider__c = objSO.Telco_Provider__c;
        objL.BEX__c = objSO.BEX__c;
        objL.Cross_Reference_Text__c = objSO.Cross_Reference_Text__c;
        objL.Designation__c = objSO.Designation__c;
        objL.Honorary_Title__c = objSO.Honorary_Title__c;
        objL.Lineage_Title__c = objSO.Lineage_Title__c;
        objL.Listing_City__c = objSO.Listing_City__c;
        objL.Listing_Country__c = objSO.Listing_Country__c;
        objL.Listing_PO_Box__c = objSO.Listing_PO_Box__c;
        objL.Listing_Postal_Code__c = objSO.Listing_Postal_Code__c;
        objL.Listing_State__c= objSO.Listing_State__c;
        objL.Listing_Street_Number__c= objSO.Listing_Street_Number__c;
        objL.Listing_Street__c= objSO.Listing_Street__c;
        objL.BOID__c= objSO.BOID__c;
        objL.Company__c= objSO.name;
        objL.Bus_Res_Gov_Indicator__c= objSO.Bus_Res_Gov_Indicator__c;
        objL.Effective_Date__c = objSO.Effective_Date__c;
        objL.First_Name__c= objSO.First_Name__c;
        objL.Manual_Sort_As_Override__c = objSO.Manual_Sort_As_Override__c;
        objL.Phone_Type__c = objSO.Phone_Type__c;
        objL.Primary_Canvass__c= objSO.Primary_Canvass__c;
        objL.Region__c= objSO.Region__c;
        objL.Secondary_Surname__c = objSO.Secondary_Surname__c;
        objL.Service_Order_Stage_Item__c = objSO.Id;
        objL.Service_Order__c = objSO.Service_Order__c; 
        objL.Year__c= objSO.Year__c;
        objL.Phone_Override__c = objSO.Phone_Override__c;
        objL.Omit_Address_OAD__c= objSO.Omit_Address_OAD__c;
        objL.Right_Telephone_Phrase__c=objSO.Right_Telephone_Phrase__c;
        objL.Left_Telephone_Phrase__c=objSO.Left_Telephone_Phrase__c;
        objL.Telco_Sort_Order__c=objSO.Telco_Sort_Order__c;
        insert objL; 
        
        Directory_Listing__c objRL = new Directory_Listing__c();
        objRL.Caption_Header__c = true;
        objRL.Name = objSO.Name;
        objRL.First_Name__c = objSO.First_Name__c;
        objRL.Listing_Street_Number__c = objSO.Listing_Street_Number__c;
        objRL.Listing_Street__c = objSO.Listing_Street__c;
        objRL.Listing_State__c = objSO.Listing_State__c;
        objRL.Listing_PO_Box__c = objSO.Listing_PO_Box__c;
        objRL.Listing_Postal_Code__c = objSO.Listing_Postal_Code__c;
        objRL.Listing_City__c = objSO.Listing_City__c;
        objRL.Listing_Country__c = objSO.Listing_Country__c;
        objRL.Created_by_Batch__c= TRUE;
        objRL.Directory__c = objSO.Directory__c;
        objRL.Directory_Heading__c = objSO.Directory_Heading__c;
        objRL.Directory_Section__c = objSO.Directory_Section__c;
        objRL.Effective_Date__c = objSO.Effective_Date__c;
        objRL.Manual_Sort_As_Override__c = objSO.Manual_Sort_As_Override__c;
        objRL.Phone_Number__c = objSO.Phone__c;
        objRL.Phone_Type__c = objSO.Phone_Type__c;
        objRL.Indent_Level__c = objSO.Indent_Level__c;
        objRL.Indent_Order__c = objSO.Indent_Order__c;
        objRL.Caption_Header__c = objSO.Caption_Header__c;
        objRL.Disconnect_Reason__c = objSO.Disconnect_Reason__c;
        objRL.Disconnected__c = objSO.Disconnected__c;
        objRL.Caption_Display_Text__C = objSO.Caption_Display_Text__c;
        objRL.Caption_Member__c = objSO.Caption_Member__c;
        objRL.Service_Order_Stage__c = objSO.Id;
        objRL.Service_Order__c = objSO.Service_Order__c;
        objRL.Telco_Provider__c = objSO.Telco_Provider__c;
        objRL.CLEC_Provider__c = objSO.CLEC_Provider__c;
        objRL.Cross_Reference_Text__c = objSO.Cross_Reference_Text__c;
        objRL.Phone_Override__c = objSO.Phone_Override__c;
        objRL.Bus_Res_Gov_Indicator__c = objSO.Bus_Res_Gov_Indicator__c;
        objRL.Normalized_Designation__c = objSO.Normalized_Designation__c;
        objRL.Normalized_First_Name__c = objSO.Normalized_First_Name__c;
        objRL.Normalized_Honorary_Title__c = objSO.Normalized_Honorary_Title__c;
        objRL.Normalized_Last_Name_Business_Name__c = objSO.Normalized_Last_Name_Business_Name__c;
        objRL.Normalized_Lineage_Title__c = objSO.Normalized_Lineage_Title__c;
        objRL.Normalized_Listing_City__c = objSO.Normalized_Listing_City__c;
        objRL.Normalized_Listing_PO_Box__c = objSO.Normalized_Listing_PO_Box__c;
        objRL.Normalized_Listing_Postal_Code__c = objSO.Normalized_Listing_Postal_Code__c;
        objRL.Normalized_Listing_Street_Name__c = objSO.Normalized_Listing_Street_Name__c;
        objRL.Normalized_Listing_Street_Number__c = objSO.Normalized_Listing_Street_Number__c;
        objRL.Normalized_Phone__c = objSO.Normalized_Phone__c;
        objRL.Normalized_Secondary_Surname__c = objSO.Normalized_Secondary_Surname__c;
        objRL.Omit_Address_OAD__c= objSO.Omit_Address_OAD__c;
        objRL.Right_Telephone_Phrase__c=objSO.Right_Telephone_Phrase__c;
        objRL.Left_Telephone_Phrase__c=objSO.Left_Telephone_Phrase__c;
        objRL.Section_Page_Type_Lookup__c=objSO.Directory_Section__r.Section_Page_Type__c;
        objRL.Telco_Sort_Order__c=objSO.Telco_Sort_Order__c;
        objRL.RecordtypeId = mapdirListRecordTypes.get('Caption Member'); 
        objRL.Listing__c=objL.id;
        insert objRL;
        
        Test.startTest(); 
        
        Digital_Product_Requirement__c objDFF=TestMethodsUtility.generateDataFulfillmentForm();
        objDFF.Caption_Indent_Level__c=2;
        objDFF.Caption_Indent_Order__c=2;
        objDFF.Caption_Header__c=true;
        objDFF.Caption_Member__c =false;
        objDFF.OpportunityId__c =newOpportunity.id;
        insert objDFF;
        
        Digital_Product_Requirement__c objDFF1=TestMethodsUtility.generateDataFulfillmentForm();
        objDFF1.Caption_Indent_Level__c=4;
        objDFF1.Caption_Indent_Order__c=5;
        objDFF1.Caption_Header__c=false;
        objDFF1.Caption_Member__c =true;
        objDFF1.OpportunityId__c =newOpportunity.id;
        insert objDFF1;
        
        Digital_Product_Requirement__c objDFF2=TestMethodsUtility.generateDataFulfillmentForm();
        objDFF2.Caption_Indent_Level__c=5;
        objDFF2.Caption_Indent_Order__c=6;
        objDFF2.Caption_Header__c=false;
        objDFF2.Caption_Member__c =true;
        objDFF2.OpportunityId__c =newOpportunity.id;
        insert objDFF2;
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=objCanvss.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,
        Scoped_Caption_Header__c=objRL.id,Digital_Product_Requirement__c =objDFF.id,Directory_Section__c=objDS.Id,National_Staging_Line_Item__c=ListNSLI[0].Id);
        insert objOrderLineItem;
        
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=objCanvss.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,Trademark_Finding_Line_OLI__c=objOrderLineItem.Id,
        Anchor_Listing_Caption_Header__c=objOrderLineItem.id,Digital_Product_Requirement__c =objDFF1.id,Directory_Section__c=objDS.Id,National_Staging_Line_Item__c=ListNSLI[1].Id);
        insert objOrderLineItem1;
        
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=objCanvss.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,Trademark_Finding_Line_OLI__c=objOrderLineItem.Id,
        Anchor_Listing_Caption_Header__c=objOrderLineItem.id,Digital_Product_Requirement__c =objDFF2.id,Directory_Section__c=objDS.Id,National_Staging_Line_Item__c=ListNSLI[2].Id);
        insert objOrderLineItem2;
        
        List<Digital_Product_Requirement__c> ListDFF=new List<Digital_Product_Requirement__c >();
        objDFF.OrderLineItemID__c =objOrderLineItem.Id;
        objDFF1.OrderLineItemID__c =objOrderLineItem1.Id;
        objDFF2.OrderLineItemID__c =objOrderLineItem2.Id;
        ListDFF.add(objDFF);
        ListDFF.add(objDFF1);
        ListDFF.add(objDFF2);
        
        update ListDFF;
        
        //NationalOpportunityCreationHandler_v3.NationalOrderProcessByManually(new set<Id>{objNSOS.id});
        Test.StopTest();
        
        List<Directory_Edition__c> ListDE=[Select id,Name,Directory__c,(Select id from National_Staging_Order_Sets__r) from Directory_Edition__c where Id=:objDirEd.Id AND Book_Status__c ='BOTS'];
        
        NationalOrderSetRenewalCron sh1 = new NationalOrderSetRenewalCron(date.today()); 
        Database.BatchableContext bc;
        sh1.execute(bc, ListDE); 
        
  }
}