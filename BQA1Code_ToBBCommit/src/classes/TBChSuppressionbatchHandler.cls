global class TBChSuppressionbatchHandler implements TBCscSupressionBatchScheduler.TBCscSupressionBatchSchedulerInterface {
	 global void execute(SchedulableContext sc) {  
	 	list<Directory__c> lstDirectory = DirectorySOQLMethods.fetchDirectoryandBOTSDE();
	 	set<Id> setDEId = new set<Id>();
	 	//set<Id> setDirId = new set<Id>();
	 	if(lstDirectory.size()> 0) {
	 		for(Directory__c iterator : lstDirectory) {
	 			if(iterator.Directory_Editions__r.size()> 0) {
	 				for(Directory_Edition__c iteratorDE : iterator.Directory_Editions__r) {
	 					if(system.today() < iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c && iteratorDE.Book_Status__c == 'BOTS') {
		 			    	setDEId.add(iteratorDE.Id);
	 					}
	 					else if(system.today() > iterator.Dir_BOTS_Suppress_UnSuppress_OLI__c && iteratorDE.Book_Status__c == 'NI') {
		 			    	setDEId.add(iteratorDE.Id);
	 					}
	 				}
	 			}
	 		}
	 	}
	 	//for suppression
	 	if(setDEId.size()> 0) {
	 		TBCbSuppressingScopedListingsBatch suppressBatch = new TBCbSuppressingScopedListingsBatch(setDEId);
	 		database.executebatch(suppressBatch);
	 	}
	 }
}