/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class FFA_UpdateTelcoAccountRollupBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable
{
    private List<String> errorMessages;
    private List<String> successMessages;

    public void execute (SchedulableContext ctx)
    {
        List<String> classNames = new List<String>{'FFA_TelcoCashMatchingByGroupQueueable'}; 
        List<String> batchStatus = new List<String>{'Queued', 'In Progress', 'Preparing'};
        Integer jobs = [SELECT count() FROM AsyncApexJob WHERE ApexClass.Name IN :classNames AND Status IN :batchStatus LIMIT 1];

        if(jobs > 0)
        {
            Datetime sysTime = System.now().addminutes( 10 );
            String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            FFA_UpdateTelcoAccountRollupBatch batch = new FFA_UpdateTelcoAccountRollupBatch();
            System.schedule('FFA_UpdateTelcoAccountRollupBatch' + sysTime.getTime(), chron_exp, batch);
        }
        else
        {
            Database.executeBatch( new FFA_UpdateTelcoAccountRollupBatch() , 200 );
        }
    }

    public FFA_UpdateTelcoAccountRollupBatch() 
    {
        initialise();
    }
    
    private void initialise()
    {
        errorMessages = new List<String>();
        successMessages = new List<String>();    
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date matchingDate = system.today().adddays(-3);
        Id userId = UserInfo.getUserId();
        
        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__Matching_Reference__c, '+
            '   ffps_bmatching__Completed__c '+
            'FROM '+
            '   ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '   ffps_bmatching__Completed__c = true '+
            'AND '+
            '   ffps_bmatching__Update_Account__c = true '+
            'AND '+
            '   CreatedDate >= :matchingDate '+
            'AND '+
            '   CreatedById = :userId ';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> unTypedScope) 
    {   
        Set<id> headerIds = new set<id>();
        List<ffps_bmatching__Custom_Cash_Matching_History__c> scope = (List<ffps_bmatching__Custom_Cash_Matching_History__c>)unTypedScope;
        for(ffps_bmatching__Custom_Cash_Matching_History__c so :scope)
        {
            headerIds.add(so.id);
        }

        Set<Id> transLineIds = new Set<id>();

        List<ffps_bmatching__Custom_Cash_Matching_History__c> matchingHistories = 
            [SELECT
                id,
                name,
                ffps_bmatching__Matching_Reference__c,
                ffps_bmatching__Period__c,
                ffps_bmatching__Completed__c,
                ffps_bmatching__Account__c,             
                ffps_bmatching__Matching_Date__c,
                ffps_bmatching__Update_Account__c,
                (SELECT
                    id,
                    ffps_bmatching__Transaction_Line_Id__c,
                    ffps_bmatching__Transaction_Line_Item__c,
                    ffps_bmatching__Amount_Matched__c
                FROM
                    ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
            FROM 
                ffps_bmatching__Custom_Cash_Matching_History__c
            WHERE
                id IN :headerIds];

        System.Savepoint sp = Database.setSavepoint();
        try
        {
            for(ffps_bmatching__Custom_Cash_Matching_History__c so :matchingHistories)
            {
                so.ffps_bmatching__Update_Account__c = false;
                for(ffps_bmatching__Custom_Cash_Matching_History_Line__c line :so.ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
                {
                    transLineIds.add(line.ffps_bmatching__Transaction_Line_Item__c);
                    transLineIds.add(line.ffps_bmatching__Transaction_Line_Id__c);
                }
            }
            if(transLineIds.size() > 0)
            {
                List<ffps_bmatching__Account_Rollup_Staging_Table__c> accountLines = getAccountRollupLines(transLineIds);

                for(ffps_bmatching__Account_Rollup_Staging_Table__c accLine :accountLines)
                {
                    if(accline.ffps_bmatching__Transaction_Line_Item__r.c2g__LineType__c == 'Account')
                    {
                        if(accline.ffps_bmatching__Payment_In_Advance_Checkbox__c) accline.ffps_bmatching__Payment_In_Advance__c = accline.ffps_bmatching__Transaction_Line_Item__r.c2g__AccountOutstandingValue__c;
                        if(accline.ffps_bmatching__Credit_on_Account_Checkbox__c) accline.ffps_bmatching__Credit_on_Account_Amount__c = accline.ffps_bmatching__Transaction_Line_Item__r.c2g__AccountOutstandingValue__c;
                        if(accline.ffps_bmatching__Current_Billing_Checkbox__c == true && System.today() <= accline.ffps_bmatching__Due_Date__c)
                            accline.ffps_bmatching__Current_Billing_Amount__c = accline.ffps_bmatching__Transaction_Line_Item__r.c2g__AccountOutstandingValue__c;
                        if(accline.ffps_bmatching__Total_Past_Due_Checkbox__c == true && System.today() > accline.ffps_bmatching__Due_Date__c)
                            accline.ffps_bmatching__Total_Past_Due_Amount__c = accline.ffps_bmatching__Transaction_Line_Item__r.c2g__AccountOutstandingValue__c;
                    }
                    if(accline.ffps_bmatching__Transaction_Line_Item__r.c2g__LineType__c == 'Analysis')
                    {
                        if(accline.ffps_bmatching__Total_Past_Due_Less_Tax_and_Fees_Checkbx__c == true && System.today() > accline.ffps_bmatching__Due_Date__c)
                            accline.ffps_bmatching__Total_Past_Due_Less_Tax_and_Fees_Amount__c = removeTaxAmountAnalysis(accline, accline.ffps_bmatching__Transaction_Line_Item__r.ffps_bmatching__Balance_Due__c, true);
                        if(accline.ffps_bmatching__Collection_Amount_Checkbox__c == true) accline.ffps_bmatching__Collection_Amount__c = accline.ffps_bmatching__Transaction_Line_Item__r.ffps_bmatching__Balance_Due__c;
                    }
                }
                update accountLines;
            }
            update matchingHistories;
        }
        catch (Exception e)
        {
            Database.rollback(sp);
            errorMessages.add( 'Update Account Rollup Staging Table : failed to update : ' + e.getMessage() + '(' + e.getLineNumber() + ')' + '\n\n');
        }
    }
    public void finish( Database.BatchableContext BC )
    {
        FFA_CleanupTelcoCashMatchingApiBatch batch = new FFA_CleanupTelcoCashMatchingApiBatch();
        database.executeBatch(batch, 7);
        
        AsyncApexJob batchJob = [
            Select
                Id, 
                Status, 
                NumberOfErrors, 
                ExtendedStatus, 
                JobItemsProcessed, 
                TotalJobItems, 
                CreatedBy.Email 
            From 
                AsyncApexJob 
            Where 
                Id = :bc.getJobId()
        ];
        
        if(!Test.isRunningTest()) sendEmail( batchJob.CreatedBy.Email, successMessages, errorMessages );
    }

    private void sendEmail( String address, List<String>successMessages, List<String>errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of matching:\n\n';
 
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Cash Matching API Batch' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }

    private static List<ffps_bmatching__Account_Rollup_Staging_Table__c> getAccountRollupLines(Set<id> transLineIds)
    {
        String query =   
                'Select '+
                '   id, '+
                '   ffps_bmatching__Account__c, '+
                '   ffps_bmatching__Account_Outstanding_Balance__c, '+
                '   ffps_bmatching__Collection_Amount_Checkbox__c, '+
                '   ffps_bmatching__Collection_Amount__c, '+
                '   ffps_bmatching__Payment_In_Advance_Checkbox__c, '+
                '   ffps_bmatching__Payment_In_Advance__c, '+
                '   ffps_bmatching__Credit_on_Account_Amount__c, '+
                '   ffps_bmatching__Credit_on_Account_Checkbox__c, '+
                '   ffps_bmatching__Current_Billing_Amount__c, '+
                '   ffps_bmatching__Current_Billing_Checkbox__c, '+
                '   ffps_bmatching__Due_Date__c, '+
                '   ffps_bmatching__Total_Past_Due_Amount__c, '+
                '   ffps_bmatching__Total_Past_Due_Checkbox__c, '+
                '   ffps_bmatching__Total_Past_Due_Less_Tax_and_Fees_Amount__c, '+
                '   ffps_bmatching__Total_Past_Due_Less_Tax_and_Fees_Checkbx__c, '+
				'	ffps_bmatching__Transaction_Line_Item__r.c2g__Transaction__r.c2g__AccountOutstandingTotal__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__AccountOutstandingValue__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__LineType__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__DueDate__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.ffps_bmatching__Balance_Due__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.ffps_bmatching__Paid__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__Transaction__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentTaxTotal__c, '+
                '   ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentValue__c, '+
                '   ffps_bmatching__Transaction_Line_Item__c '+
                'From '+
                '   ffps_bmatching__Account_Rollup_Staging_Table__c ' + 
                'Where '+
                '   ffps_bmatching__Transaction_Line_Item__c in :transLineIds ';

        return database.query(query);
    }
    /**
        * First remove the Paid Balance off the Product value first then Tax
    **/
    private Decimal removeTaxAmountAnalysis(ffps_bmatching__Account_Rollup_Staging_Table__c accLine, Decimal balance, Boolean afterUpdate)
    {
        //Just incase Custom Balance__c field failed to update 
        if(accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__Transaction__r.c2g__AccountOutstandingTotal__c == 0 && afterUpdate) return 0;

        Decimal lineOutstandingAmount;
        //Just incase Custom Balance__c field failed to update, work out outstanding on Analysis Line
        if( accLine.ffps_bmatching__Transaction_Line_Item__r.ffps_bmatching__Paid__c == 0 && balance == 0)
        {
            lineOutstandingAmount = outstandingBalance(accLine);
        }
        else
        {
            lineOutstandingAmount = balance;
        }

        Decimal documentTaxableValue = accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentTaxTotal__c == null ? 0 : (accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentTaxTotal__c * -1);
        Decimal documentValue = accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentValue__c  == null ? 0 : (accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentValue__c * -1);

        //Failsafe 
        if(documentTaxableValue == 0 || documentValue == 0 || lineOutstandingAmount == 0) return lineOutstandingAmount;

        //If Product Value is greater than Tax value, then remove Tax amount or else return 0 (Product Line has been matched without Tax Inclusion)
        if(math.abs(lineOutstandingAmount) > math.abs(documentTaxableValue)) return lineOutstandingAmount - documentTaxableValue;

        return 0;
    }

    private Decimal outstandingBalance(ffps_bmatching__Account_Rollup_Staging_Table__c accLine)
    {
        Decimal documentTaxValue = accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentTaxTotal__c == null ? 0 : (accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentTaxTotal__c * -1);
        Decimal documentValue = accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentValue__c  == null ? 0 : (accLine.ffps_bmatching__Transaction_Line_Item__r.c2g__DocumentValue__c * -1);

        return (documentValue + documentTaxValue);
    }
}