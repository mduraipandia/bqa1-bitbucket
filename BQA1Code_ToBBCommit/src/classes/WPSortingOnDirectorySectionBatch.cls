global class WPSortingOnDirectorySectionBatch implements Database.Batchable<sobject>{
    Set<Id> setTelcoIds = new Set<Id>();
    String checkOrUncheck;
    global WPSortingOnDirectorySectionBatch(Set<Id> telcoIds, String chOrUnch) {
        setTelcoIds = telcoIds;
        checkOrUncheck = chOrUnch;
    }
    
    global Database.Querylocator start(Database.BatchableContext bc) {
        String wpSecType = CommonMessages.wpSecPageType;
        String SOQL = 'SELECT ID, Telco_Drives_WP_Sorting__c FROM Directory_Section__c WHERE Directory__r.Telco_Provider__c IN: setTelcoIds AND Section_Page_Type__c =: wpSecType';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Section__c> listDirSec) {
        if(checkOrUncheck == 'checked') {
            for(Directory_Section__c DS : listDirSec) {
                DS.Telco_Drives_WP_Sorting__c = true;
            }            
        } else {
            for(Directory_Section__c DS : listDirSec) {
                DS.Telco_Drives_WP_Sorting__c = false;
            } 
        }
        update listDirSec;
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}