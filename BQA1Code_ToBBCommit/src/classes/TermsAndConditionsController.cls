public with sharing class TermsAndConditionsController {
  
//  public Contact portalCont {get; set;} 
//Changed by RK.G on 02/01/2013 Becz of Contact Object permission on Customer portal
  public User portalCont {get; set;} 
  
  public static final string T_AND_C_REDIRECT = '/apex/PortalHomePage';
  public static final string LOGOUT_URL = '/secur/logout.jsp';
  public static final String STATIC_RESOURCE_ERROR = 'An Error Occurred while loading the static resource ';
  public static final String INVALID_CONTACT_ID_ERROR = 'Error querying for Contact ';
  public static final string RESOURCE_NAME = 'Terms_And_Conditions';
  
  public StaticResource resource; 
  
  public String bodyText {get; set;}
  
  public Boolean hasMessages{
                get
                {
                  return ApexPages.hasMessages();
                }
                 private set;
              }

  public TermsAndConditionsController(string resourceName)
  {
    
    try{
      resource = [SELECT id, Body FROM StaticResource WHERE name = :resourceName];
      bodyText = resource.body.toString();
    }
    catch(Exception ex)
    {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, STATIC_RESOURCE_ERROR + ex.getMessage()));
      return;
    }
    
    Id contactId;
    
    try {
        contactId = ApexPages.currentPage().getParameters().get('ContactId');
        //portalCont = [Select id, Name,Terms_And_Conditions_Agreed__c from Contact where id = :contactId ];
        //Changed by RK.G on 02/01/2013 Becz of Contact Object permission on Customer portal
        portalCont = [Select id,Name,Terms_And_Conditions_Agreed__c from User where id = :contactId ];
             system.debug('portalCont ===='+portalCont );  
          } catch(Exception e) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, INVALID_CONTACT_ID_ERROR + e.getMessage()));
          }
  }  

  public TermsAndConditionsController()
  {
    this(RESOURCE_NAME);
          
  } 
  
  public Pagereference ok()
  {
    if(portalCont != null)
    {
      try {
        portalCont.Terms_And_Conditions_Agreed__c = true;        
                update portalCont;
               
          } catch(DmlException e) {
              ApexPages.addMessages(e);
          }          
    }
    Pagereference ref = new Pagereference(T_AND_C_REDIRECT);
      return ref;
  }
  
  
  public Pagereference cancel()
  {
    Pagereference ref = new Pagereference(LOGOUT_URL);
    return ref;
  }

}