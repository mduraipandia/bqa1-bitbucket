@IsTest
private Class ADPriceBookUtilityControllerTest {
    static testMethod void testADPriceBookUtilityCtlrTest() {                 
        Product2 newProduct = TestMethodsUtility.generateproduct();
        insert newProduct;

        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);

        // Create a PriceBookEntry Record

        List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = System.Label.PricebookId, IsActive = false);
        lstinsertPBE.add(pbe);
        insert lstinsertPBE;


        // Create a Custom Setting record of PriceBookEntry
        PriceBookEntrySetting__c pbesetting = new PriceBookEntrySetting__c();
        pbesetting.name = newProduct.Id;
        insert pbesetting;

        // Create a Custom Setting record of Product2Setting

        Product2Setting__c productsetting = new Product2Setting__c();
        productsetting.name = newProduct.Id;
        insert productsetting;

        Test.startTest();

        ADPriceBookUtilityController.activateEntry();
        ADPriceBookUtilityController.deactivateEntry();
        ADPriceBookUtilityController.activateProduct();
        ADPriceBookUtilityController.deactivateProduct();

        Test.stopTest();
    }
}