/*************************************************************
Batch Class to send Fulfillment cancellations for each product
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 09/21/2015
*************************************************************/
global class DeleteFulfillmentBatch implements Database.Batchable < sObject > , Database.AllowsCallouts {

    global List < Order_Line_Items__c > start(Database.BatchableContext BC) {

        List < Order_Line_Items__c > fnlLst = new List < Order_Line_Items__c > ();

        //Fetch all OLI records
        List < Order_Line_Items__c > allOLns = [SELECT id, Parent_ID__c, Cutomer_Cancel_Date__c, Cancellation__c, Order__r.Account__r.TalusAccountId__c, Digital_Product_Requirement__r.Id, Digital_Product_Requirement__r.OwnerId, Digital_Product_Requirement__r.Final_Status__c, Digital_Product_Requirement__r.Talus_Subscription_Id__c,
            Digital_Product_Requirement__r.Submit_Yodle_Cancel__c, Digital_Product_Requirement__r.Contact__c, Digital_Product_Requirement__r.Fulfillment_Submit_Status__c, Digital_Product_Requirement__r.Customer_Cancel_Date__c, Digital_Product_Requirement__r.Bundle__c, Spotzer_Bundle__c,
            Digital_Product_Requirement__r.Enterprise_Customer_ID__c, Digital_Product_Requirement__r.UDAC__c, Digital_Product_Requirement__r.business_name__c, Digital_Product_Requirement__r.business_email__c, Digital_Product_Requirement__r.business_url__c, Digital_Product_Requirement__r.business_phone_number_office__c,
            Digital_Product_Requirement__r.business_address1__c, Digital_Product_Requirement__r.business_city__c, Digital_Product_Requirement__r.business_state__c, Digital_Product_Requirement__r.business_postal_code__c, Digital_Product_Requirement__r.CreatedById, Billing_Contact__c, Digital_Product_Requirement__r.Effective_Date__c,
            Digital_Product_Requirement__r.Talus_DFF_Id__c, Status__c, Digital_Product_Requirement__c, Digital_Product_Requirement__r.Name, Digital_Product_Requirement__r.RecordTypeId, Order_Group__r.oli_count__c, Digital_Product_Requirement__r.RecordType.DeveloperName, Digital_Product_Requirement__r.Fulfillment_Type__c from Order_Line_Items__c
            where Action_Code__c = 'Cancel'
            and Cutomer_Cancel_Date__c <= today and Status__c not in ('Cancelation Requested', 'Cancelled') and media_type__c = 'Digital'
            and Product_Type__c Not In('General Add-on')
            limit 50000
        ];

        //Passing Queried List to parentAddonOLI method, in order to seperate records based on bundle logic
        fnlLst = DeleteTalusSubscription.parentAddonOLIs(allOLns);

        //System.debug('************ListOLIs************' + fnlLst.size());
        /*
        List < Order_Line_Items__c > flmntOLs = new List < Order_Line_Items__c > ();
        Set < String > milesRcdTyp = CommonUtility.milesDffRT();
        Set < String > talusRcdTyp = CommonUtility.talusDffRT();
        talusRcdTyp.addAll(milesRcdTyp);
        
        for(Order_Line_Items__c iterator: fnlLst){
            if (talusRcdTyp.contains(iterator.Digital_Product_Requirement__r.RecordType.DeveloperName) && String.isNotBlank(iterator.Digital_Product_Requirement__r.Talus_Subscription_Id__c) && iterator.Digital_Product_Requirement__r.Fulfillment_Type__c == 'Talus' && String.isNotBlank(iterator.Digital_Product_Requirement__r.Talus_DFF_Id__c) && iterator.Spotzer_Bundle__c != true && iterator.Cutomer_Cancel_Date__c <= system.today()) {
                flmntOLs.add(iterator);
            }                
        }
        System.debug('************CalloutDffsList************' + flmntOLs.size());
        */
        return fnlLst;

    }

    global void execute(Database.BatchableContext BC, List < Order_Line_Items__c > scope) {
        if (scope.size() > 0) {

            //Calling external class to send cancellations
            DeleteTalusSubscription.deleteSubscriptionNonFuture(scope);            

        }
    }

    global void finish(Database.BatchableContext BC) {

        List < Messaging.SingleEmailMessage > lstAdmEmails = new List < Messaging.SingleEmailMessage > ();
        List < String > admEmails = Label.Fulfillment_Batch_Notification_Emails.split(',');

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        //Notify Admin on Job completion
        for (String trgtObjId: admEmails) {
            lstAdmEmails.add(CommonUtility.emailUsingTargetObjectId(trgtObjId, 'Batch Job For Cancel Fulfillment Records has ' + a.Status, 'Batch Job has processed Total of ' + a.TotalJobItems + ' batches with ' + a.NumberOfErrors + ' failures'));
        }
        Messaging.sendEmail(lstAdmEmails);
    }
}