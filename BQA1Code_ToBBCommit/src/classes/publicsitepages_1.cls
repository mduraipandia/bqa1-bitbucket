public with sharing class publicsitepages_1 {

    //Getter and Setters
    public Boolean hdngFlag{get;set;}
    public Boolean dirFlag{get;set;}
    public String dirHdngId{get;set;}
    public String dirCode{get;set;}
    public String advInd{get;set;}
    public String dirHdngName{get;set;}
    public Transient List<Section_Heading_Mapping__c> lstDirSec{get;set;}
    public Transient List<Directory__c> lstDirty{get;set;}
    public Transient List<Cross_Reference_Headings__c> hdngChilds{get;set;}
    public Directory_Heading__c currentRcd{get;set;}
    public Directory__c dirRcd{get;set;}
    public String dirId{get;set;}
    public String getDirId{get;set;}
    public String refToHdng{get;set;}
    public String crsRefHdng{get;set;}
    public Integer dirCnt{get;set;}

    //Constructor
    public publicsitepages_1(ApexPages.StandardController controller) {
        dirHdngId = ApexPages.currentPage().getParameters().get('Id');
        dirCode = System.currentPagereference().getParameters().get('dirCode');
        currentRcd = [SELECT Id, Name, Code__c FROM Directory_Heading__c WHERE Id =:ApexPages.currentPage().getParameters().get('id')];
        default();
    }

    public void default() {
        refToHdng = '';
        crsRefHdng = '';
        //CommonUtility.msgInfo('************DirCode************' + dirCode);
        lstDirSec = [SELECT Id, Directory_Section__r.Directory__c, Directory_Section__r.Directory__r.Name,
                                                      Directory_Section__r.Directory__r.Directory_Code__c, Directory_Section__r.Directory__r.Publication_Company__r.Name
                                                      from Section_Heading_Mapping__c where Directory_Heading__c =: dirHdngId limit 50000];

        for (Cross_Reference_Headings__c cr: [Select Name, Source_Heading__r.Name, Cross_Reference_Heading__r.Name, Cross_Reference_Heading__c, Source_Heading_Code__c from Cross_Reference_Headings__c where Source_Heading__c =: dirHdngId OR Cross_Reference_Heading__c =: dirHdngId limit 50000]) {
            System.debug('************CrsRfs************' + cr);
            if (Cr.Source_Heading_Code__c != null && currentRcd.Name != Cr.Source_Heading__r.Name) {
                refToHdng = refToHdng + Cr.Source_Heading__r.Name + ',';
            }
            if (Cr.Cross_Reference_Heading__c != null && currentRcd.Name != Cr.Cross_Reference_Heading__r.Name) {
                crsRefHdng = crsRefHdng + Cr.Cross_Reference_Heading__r.Name + ',';
            }
        }

        refToHdng = refToHdng.removeEnd(',');
        crsRefHdng = crsRefHdng.removeEnd(',');

        if(lstDirSec.size()>0){
            lstDirSec.sort();
            dirCnt = lstDirSec.size();
        }else{
            CommonUtility.msgInfo('No records to display');
        }

        if(String.isNotBlank(dirCode)) {
            advInd = secHdngAdvCnt();
        }
        //System.debug('************' + lstDirSec.size());
    }

    public  String secHdngAdvCnt() {
      String advCnt;
      Section_Heading_Mapping__c schHMp = [SELECT Id, Directory_Code__c, Directory_Heading__c, SHM_Advertising_Content_Not_Available__c from Section_Heading_Mapping__c where Directory_Heading__c =: dirHdngId and Directory_Code__c =: dirCode limit 1];
      if(schHMp.SHM_Advertising_Content_Not_Available__c){
        advCnt = 'N';
      } else{
        advCnt = 'Y';
      }
      return advCnt;
    }

}