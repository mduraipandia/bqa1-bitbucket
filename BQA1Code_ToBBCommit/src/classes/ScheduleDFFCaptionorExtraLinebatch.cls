global class ScheduleDFFCaptionorExtraLinebatch implements Schedulable {
   public Interface ScheduleDFFCaptionorExtraLinebatchInterface{
     void execute(SchedulableContext sc);
   }
   global void execute(SchedulableContext sc) {
    Type targetType = Type.forName('ScheduleDFFCaptionorExtraLinebatchHndlr');
        if(targetType != null) {
            ScheduleDFFCaptionorExtraLinebatchInterface obj = (ScheduleDFFCaptionorExtraLinebatchInterface)targetType.newInstance();
            obj.execute(sc);
        }
   
   }
}