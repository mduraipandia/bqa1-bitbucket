global class TBCscMonthlyCronScheduler Implements Schedulable {
    public interface TBCscMonthlyCronSchedulerInterface {
        void execute(SchedulableContext sc);
    }
    global void execute(SchedulableContext sc) {
        Type targetType = Type.forName('TBChMonthlyCronSchedulerHandler');
        if(targetType != null) {
            TBCscMonthlyCronSchedulerInterface obj = (TBCscMonthlyCronSchedulerInterface)targetType.newInstance();
            obj.execute(sc);
        }
    }
}