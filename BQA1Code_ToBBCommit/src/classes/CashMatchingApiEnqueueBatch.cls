/*
    * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
    * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
    * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
    * result in criminal or other legal proceedings.
    *
    * Copyright FinancialForce.com, inc. All rights reserved.
*/

public class CashMatchingApiEnqueueBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable 
{
    private Set<String> errorMessages;
    private Set<String> successMessages;
    private decimal counter;
    private Id lastRecordId;
    private id transactionId = null;
    public CashMatchingApiEnqueueBatch() { initialise(null, null, 0); }
    public CashMatchingApiEnqueueBatch(Id lastRecordId, Decimal counter) { initialise(lastRecordId, null, counter); }
    public CashMatchingApiEnqueueBatch(Id transactionId, Id lastRecordId) { initialise(lastRecordId, transactionId, 0); }

    private void initialise(Id m_lastRecordId, Id m_transactionId, Decimal m_counter)
    {
        errorMessages = new Set<String>();
        successMessages = new Set<String>();
        this.lastRecordId = m_lastRecordId;
        this.transactionId = m_transactionId;
        counter = m_counter;
    }

    public void execute (SchedulableContext ctx)
    {
        Database.executeBatch( new CashMatchingApiEnqueueBatch() , (Integer)ffps_bmatching__BatchInterfaceSettings__c.getInstance().ffps_bmatching__WriteOffBatchSize__c );
    }

    public Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date matchingDate = system.today().adddays(-1);
        Id userId = UserInfo.getUserId();
        
        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__Matching_Reference__c, '+
            '   ffps_bmatching__Completed__c '+
            'FROM '+
            '   ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '   ffps_bmatching__Completed__c = false '+
            'AND '+
            '   CreatedDate >= :matchingDate '+
            'AND '+
            '   CreatedById = :userId ';
            if(lastRecordId != null ) query += ' and Id > :lastRecordId ';
            query += ' Order By Name ';
            query += ' LIMIT 400 ';

        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> unTypedScope) 
    {   
        Set<id> headerIds = new set<id>();
        List<ffps_bmatching__Custom_Cash_Matching_History__c> scope = (List<ffps_bmatching__Custom_Cash_Matching_History__c>)unTypedScope;
        for(ffps_bmatching__Custom_Cash_Matching_History__c so :scope)
        {
            headerIds.add(so.id);
        }

        List<ffps_bmatching__Custom_Cash_Matching_History__c> matchingHistories = 
            [SELECT
                id,
                name,
                ffps_bmatching__Matching_Reference__c,
                ffps_bmatching__Period__c,
                ffps_bmatching__Completed__c,
                ffps_bmatching__Account__c,             
                ffps_bmatching__Matching_Date__c,
                (SELECT
                    id,
                    ffps_bmatching__Transaction_Line_Id__c,
                    ffps_bmatching__Transaction_Line_Item__c,
                    ffps_bmatching__Amount_Matched__c
                FROM
                    ffps_bmatching__Custom_Cash_Matching_History_Lines__r)
            FROM 
                ffps_bmatching__Custom_Cash_Matching_History__c
            WHERE
                id IN :headerIds];
                
        System.enqueueJob(new CashMatchingApiQueueable(matchingHistories));
        lastRecordId = scope[scope.size()-1].Id;
    }
    
    public void finish(Database.BatchableContext BC) 
    {
        AsyncApexJob batchJob = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];

        List<ffps_bmatching__Custom_Cash_Matching_History__c> transactionLines = recordsStillToBeProcessed(lastRecordId);
        //Counter - dont want to run more than 50000 records 
        if(!transactionLines.isEmpty() && counter < 125)
        {
            counter++;
            CashMatchingApiEnqueueBatch batch = new CashMatchingApiEnqueueBatch(lastRecordId, counter);
            database.executebatch(batch, (Integer)ffps_bmatching__BatchInterfaceSettings__c.getInstance().ffps_bmatching__CashMatchingAPILimit__c);
        }
    }

    private void sendEmail( String address, Set<String> successMessages, Set<String> errorMessages  )
    {   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new String[] { address } );

        String body = 'Results of Historic Write-off - Matching API (WriteOffEnqueueBatch):\n\n';
        
        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';
        }
        else
        {
            body += 'No errors occurred.\n';
            body += '\n';
        }
        for( String success : successMessages )
        {
            body += success +'\n';
        }
        
        mail.setPlainTextBody( body );
        mail.setSubject( 'Results of Historic Write-off - Matching API (WriteOffEnqueueBatch)' );
        
        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }

    private static List<ffps_bmatching__Custom_Cash_Matching_History__c> recordsStillToBeProcessed(Id transId)
    {
        Date matchingDate = system.today().adddays(-1);
        Id userId = UserInfo.getUserId();
        
        String query = 
            'SELECT '+
            '   Id, '+
            '   Name, '+
            '   ffps_bmatching__Matching_Reference__c, '+
            '   ffps_bmatching__Completed__c '+
            'FROM '+
            '   ffps_bmatching__Custom_Cash_Matching_History__c '+
            'WHERE '+
            '   ffps_bmatching__Completed__c = false '+
            'AND '+
            '   CreatedDate >= :matchingDate '+
            'AND '+
            '   CreatedById = :userId '+
            'AND '+
            '    Id > :transId ';
            query += ' Order By Name ';
            query += ' LIMIT 3000 ';
        return database.query(query); 
    }
}