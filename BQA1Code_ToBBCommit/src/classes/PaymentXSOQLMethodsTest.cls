@isTest(SeeAllData=True)
public class PaymentXSOQLMethodsTest{
@IsTest(SeeAllData = true) static void PaymentXMethodsTest() {
        Set<Id> setSIId = new Set<Id>();
        Set<Id> setOppId = new Set<Id>();
        Set<Id> setPXId = new Set<Id>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        setOppId.add(newOpportunity.Id);
        c2g__codaCompany__c newCompany = TestMethodsUtility.createCompany('VAT');
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.generateSalesInvoice(newAccount, newOpportunity);
        newSalesInvoice.c2g__OwnerCompany__c = newCompany.Id;
        insert newSalesInvoice;
        setSIId.add(newSalesInvoice.Id);
        pymt__PaymentX__c newPaymentX = TestMethodsUtility.createPaymentX(newSalesInvoice, newOpportunity);
        setPXId.add(newPaymentX.Id);
        System.assertNotEquals(null, PaymentXSOQLMethods.getPaymentxByOpportunityID(setOppId));
        System.assertNotEquals(null, PaymentXSOQLMethods.getPaymentxByID(setPXId));
    }
    }