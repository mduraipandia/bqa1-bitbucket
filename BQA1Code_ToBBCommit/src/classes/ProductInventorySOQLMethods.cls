public with sharing class ProductInventorySOQLMethods {
    
    
    public static List<Product_Inventory__c> getActiveProdInventoriesByOLIIds(set<Id> OLIIds){
        return [SELECT Active__c, Quantity__c, Product2__r.Print_Specialty_Product_Type__c, Order_Line_Item__c FROM Product_Inventory__c 
                WHERE Order_Line_Item__c IN : OLIIds AND Active__c = true];
    }
}