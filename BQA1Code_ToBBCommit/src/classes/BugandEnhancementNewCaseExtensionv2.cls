public with sharing class BugandEnhancementNewCaseExtensionv2
{
   // extension constructor
   public BugandEnhancementNewCaseExtensionv2(ApexPages.StandardController std){
   }

   Case cs;

   public Case getCase() {
        if (cs == null) cs = new Case();
        return cs;
   }
  
   public PageReference save() {
   
     try{
        insert cs;
        PageReference nextPage = Page.BugandEnhancementExternalDetailv2;
        nextPage.getParameters().put('id', cs.Id);
        return nextPage;

    } catch (DMLException e) {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error creating case'));
        return null;
    }

    return null;
  
      
   }
}