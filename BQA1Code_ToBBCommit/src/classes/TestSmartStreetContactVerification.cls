@isTest
 private class TestSmartStreetContactVerification
 {
          
       public static testMethod void myUnitTest_1()
       {
        
              /*  Contact Cont_obj = new Contact();
                Cont_obj.FirstName= 'Bravoch';
                Cont_obj.LastName='voch';
                Cont_obj.mailingPostalCode = '450450';
                Cont_obj.mailingCity = 'NV';
                Cont_obj.Phone = '9985123456';
                Cont_obj.mailingCountry = 'USA';
                Cont_obj.mailingState = 'OH';
                Cont_obj.Street_Status__c='valid';
                Cont_obj.Do_Not_Validate_Address__c=true;
                insert Cont_obj; */
               
                Account objAccount = new Account(Name = 'Unit Testing', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999');
                insert objAccount;
         
                Contact objContact = TestMethodsUtility.generateContact(objAccount.Id);
                insert objContact;        
               
                 Test.startTest();
               ApexPages.CurrentPage().getParameters().put('id',objContact.id);
               ApexPages.StandardController sc = new ApexPages.standardController(objContact);
               SmartStreetContactVerification  obj_sca=new SmartStreetContactVerification(sc); 
             // obj_ssa.acc_incremnt=acct_obj;
             // obj_ssa.acc=acct_obj;
              obj_sca.incrementCounter();
              SmartStreetContactVerification.updateContact( objContact.id);
              
              Test.Stoptest();
        
        }  
        public static testMethod void myUnitTest_2()
       {
         
               /* Contact Cont_obj = new Contact();
                Cont_obj.FirstName= 'Bravoch';
                Cont_obj.LastName='voch';
                Cont_obj.mailingPostalCode = '450450';
                Cont_obj.mailingCity = 'NV';
                Cont_obj.Phone = '9985123456';
                Cont_obj.mailingCountry = 'USA';
                Cont_obj.mailingState = 'OH';
                Cont_obj.Street_Status__c='invalid';
                Cont_obj.Do_Not_Validate_Address__c=false;
                insert Cont_obj; */
                
                Account objAccount = new Account(Name = 'Unit Testing', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999');
                insert objAccount;
         
                Contact objContact = TestMethodsUtility.generateContact(objAccount.Id);
                insert objContact;    
                
                Test.startTest();
               ApexPages.CurrentPage().getParameters().put('id',objContact.id);
               ApexPages.StandardController sc = new ApexPages.standardController(objContact);
               SmartStreetContactVerification  obj_sca=new SmartStreetContactVerification(sc); 
             // obj_ssa.acc_incremnt=acct_obj;
             // obj_ssa.acc=acct_obj;
              obj_sca.incrementCounter();
              SmartStreetContactVerification.updateContact(objContact.id);
              
              Test.Stoptest();
        
        }  
        
        }