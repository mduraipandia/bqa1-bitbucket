public with sharing class SalesCreditNoteLineItemSOQLMethods {
    
    
    public static list<c2g__codaCreditNoteLineItem__c> getSysGenSCNLineItemByOLI(set<Id> setOLIId) {
        return [SELECT Id,c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,
                c2g__CreditNote__r.c2g__Period__c,Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,c2g__CreditNote__r.Customer_Name__c,
                Order_Line_Item__r.Product2__r.RGU__c,Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c, 
                Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,
                c2g__CreditNote__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,
                c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,Order_Line_Item__r.Directory__c, c2g__CreditNote__r.c2g__DueDate__c,
                Order_Line_Item__r.Directory_Edition__c,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,
                Billing_Frequency__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c, c2g__CreditNote__r.c2g__InvoiceDate__c,c2g__CreditNote__r.Telco_Reversal__c  
                FROM c2g__codaCreditNoteLineItem__c WHERE Order_Line_Item__c IN :setOLIId AND c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c= false AND c2g__CreditNote__r.Telco_Reversal__c=false];
    }
    public static Map<Id,c2g__codaCreditNoteLineItem__c> getSCNLIMapByOLiIds(set<Id> setOLIIds){
        Map<Id,c2g__codaCreditNoteLineItem__c>  reqdCreditNoteMap=new Map<Id,c2g__codaCreditNoteLineItem__c>([select Id,c2g__CreditNote__c,c2g__CreditNote__r.Credit_Frequency__c,c2g__CreditNote__r.Case__c,
            c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteCurrency__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,c2g__CreditNote__r.c2g__CreditNoteDescription__c,c2g__CreditNote__r.c2g__CreditNoteReason__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__CreditNoteTotal__c,c2g__CreditNote__r.c2g__CustomerReference__c,c2g__CreditNote__r.c2g__DueDate__c,
            c2g__CreditNote__r.c2g__Invoice__c,c2g__CreditNote__r.c2g__NetTotal__c,c2g__CreditNote__r.c2g__Opportunity__c,c2g__CreditNote__r.c2g__OutstandingValue__c,c2g__CreditNote__r.c2g__OwnerCompany__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__Period__c,c2g__CreditNote__r.c2g__Transaction__c,c2g__CreditNote__r.c2g__UnitOfWork__c,c2g__CreditNote__r.c2g__Year__c,
            c2g__CreditNote__r.CMR_Number__c,c2g__CreditNote__r.Customer_Name__c,c2g__CreditNote__r.Name,c2g__CreditNote__r.National_Client_Number__c,c2g__CreditNote__r.OpCo__c,c2g__CreditNote__r.Order_Line_Item_Discount__c,c2g__CreditNote__r.Transaction_Type__c,c2g__CreditNote__r.SC_Billing_Partner__c ,c2g__CreditNote__r.SC_Media_Type__c,c2g__CreditNote__r.SC_Payment_Method__c ,
            c2g__CreditNote__r.SC_Billing_Entity__c,c2g__CreditNote__r.SC_P4P__c, Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,Order_Line_Item__r.Product2__r.RGU__c,
            Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Directory_Edition__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c,Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,
            c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c,Billing_Frequency__c 
            from c2g__codaCreditNoteLineItem__c WHERE c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c= false AND Order_Line_Item__c IN :setOLIIds  AND  c2g__CreditNote__r.c2g__Account__c != null AND c2g__CreditNote__r.Telco_Reversal__c=false AND c2g__CreditNote__r.c2g__CreditNoteDate__c>TODAY]);   
        return reqdCreditNoteMap;
    }

    public static Map<Id,c2g__codaCreditNoteLineItem__c> getSysGenSCNLineItemByOLIForRetro(set<Id> setOLIId) {
        Map<Id,c2g__codaCreditNoteLineItem__c>  reqdCreditNoteMap=new Map<Id,c2g__codaCreditNoteLineItem__c>([select Id,c2g__CreditNote__c,c2g__CreditNote__r.Credit_Frequency__c,c2g__CreditNote__r.Case__c,
            c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteCurrency__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,c2g__CreditNote__r.c2g__CreditNoteDescription__c,c2g__CreditNote__r.c2g__CreditNoteReason__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__CreditNoteTotal__c,c2g__CreditNote__r.c2g__CustomerReference__c,c2g__CreditNote__r.c2g__DueDate__c,
            c2g__CreditNote__r.c2g__Invoice__c,c2g__CreditNote__r.c2g__NetTotal__c,c2g__CreditNote__r.c2g__Opportunity__c,c2g__CreditNote__r.c2g__OutstandingValue__c,c2g__CreditNote__r.c2g__OwnerCompany__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__Period__c,c2g__CreditNote__r.c2g__Transaction__c,c2g__CreditNote__r.c2g__UnitOfWork__c,c2g__CreditNote__r.c2g__Year__c,
            c2g__CreditNote__r.CMR_Number__c,c2g__CreditNote__r.Customer_Name__c,c2g__CreditNote__r.Name,c2g__CreditNote__r.National_Client_Number__c,c2g__CreditNote__r.OpCo__c,c2g__CreditNote__r.Order_Line_Item_Discount__c,c2g__CreditNote__r.Transaction_Type__c,c2g__CreditNote__r.SC_Billing_Partner__c ,c2g__CreditNote__r.SC_Media_Type__c,c2g__CreditNote__r.SC_Payment_Method__c ,
            c2g__CreditNote__r.SC_Billing_Entity__c,c2g__CreditNote__r.SC_P4P__c, Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,Order_Line_Item__r.Product2__r.RGU__c,
            Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Directory_Edition__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c,Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c,
            c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c,Billing_Frequency__c 
            FROM c2g__codaCreditNoteLineItem__c WHERE Order_Line_Item__c IN :setOLIId AND c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c= false AND c2g__CreditNote__r.Telco_Reversal__c=false]);
        return reqdCreditNoteMap;
    }

    public static list<c2g__codaCreditNoteLineItem__c> getSysGenSCNLineItemByOLIForRetro(set<Id> setOLIId, string strPartnerAccountId, boolean bFutureTransfer) {
            Set<String> setFrequency = new Set<String>{'Recurring'};
            String strQuery = 'select Id,c2g__CreditNote__c,c2g__CreditNote__r.Credit_Frequency__c,c2g__CreditNote__r.Case__c, '+
            'c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteCurrency__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,c2g__CreditNote__r.c2g__CreditNoteDescription__c,c2g__CreditNote__r.c2g__CreditNoteReason__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__CreditNoteTotal__c,c2g__CreditNote__r.c2g__CustomerReference__c,c2g__CreditNote__r.c2g__DueDate__c, '+
            'c2g__CreditNote__r.c2g__Invoice__c,c2g__CreditNote__r.c2g__NetTotal__c,c2g__CreditNote__r.c2g__Opportunity__c,c2g__CreditNote__r.c2g__OutstandingValue__c,c2g__CreditNote__r.c2g__OwnerCompany__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__Period__c,c2g__CreditNote__r.c2g__Transaction__c,c2g__CreditNote__r.c2g__UnitOfWork__c,c2g__CreditNote__r.c2g__Year__c, '+
            'c2g__CreditNote__r.CMR_Number__c,c2g__CreditNote__r.Customer_Name__c,c2g__CreditNote__r.Name,c2g__CreditNote__r.National_Client_Number__c,c2g__CreditNote__r.OpCo__c,c2g__CreditNote__r.Order_Line_Item_Discount__c,c2g__CreditNote__r.Transaction_Type__c,c2g__CreditNote__r.SC_Billing_Partner__c ,c2g__CreditNote__r.SC_Media_Type__c,c2g__CreditNote__r.SC_Payment_Method__c , '+
            'c2g__CreditNote__r.SC_Billing_Entity__c,c2g__CreditNote__r.SC_P4P__c, Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,Order_Line_Item__r.Product2__r.RGU__c, '+
            'Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Directory_Edition__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c,Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c, '+
            'c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c,Billing_Frequency__c  '+
            'FROM c2g__codaCreditNoteLineItem__c WHERE Order_Line_Item__c IN:setOLIId AND c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c = false AND c2g__CreditNote__r.Telco_Reversal__c = false'; // vikas- removed condition for checking the credit frequency we are not checking for partner change

            if(String.isNotEmpty(strPartnerAccountId)) {
                strQuery += ' AND c2g__CreditNote__r.c2g__Account__c =:strPartnerAccountId';
            }
            if(bFutureTransfer) {
                strQuery += ' AND c2g__CreditNote__r.c2g__CreditNoteDate__c > TODAY';
            }
            strQuery += ' order by Order_Line_Item__c ASC';
            System.debug('====>>> '+strQuery);
        return Database.query(strQuery);
    }

    public static list<c2g__codaCreditNoteLineItem__c> getSysGenSCNLineItemByOLIForRetro(set<Id> setOLIId, string strPartnerAccountId, boolean bFutureTransfer, String strFrequencyMode) {
            Set<String> setFrequency = new Set<String>{'Recurring'};
            String strQuery = 'select Id,c2g__CreditNote__c,c2g__CreditNote__r.Credit_Frequency__c,c2g__CreditNote__r.Case__c, '+
            'c2g__CreditNote__r.c2g__Account__c,c2g__CreditNote__r.c2g__CreditNoteCurrency__c,c2g__CreditNote__r.c2g__CreditNoteDate__c,c2g__CreditNote__r.c2g__CreditNoteDescription__c,c2g__CreditNote__r.c2g__CreditNoteReason__c,c2g__CreditNote__r.c2g__CreditNoteStatus__c,c2g__CreditNote__r.c2g__CreditNoteTotal__c,c2g__CreditNote__r.c2g__CustomerReference__c,c2g__CreditNote__r.c2g__DueDate__c, '+
            'c2g__CreditNote__r.c2g__Invoice__c,c2g__CreditNote__r.c2g__NetTotal__c,c2g__CreditNote__r.c2g__Opportunity__c,c2g__CreditNote__r.c2g__OutstandingValue__c,c2g__CreditNote__r.c2g__OwnerCompany__c,c2g__CreditNote__r.c2g__PaymentStatus__c,c2g__CreditNote__r.c2g__Period__c,c2g__CreditNote__r.c2g__Transaction__c,c2g__CreditNote__r.c2g__UnitOfWork__c,c2g__CreditNote__r.c2g__Year__c, '+
            'c2g__CreditNote__r.CMR_Number__c,c2g__CreditNote__r.Customer_Name__c,c2g__CreditNote__r.Name,c2g__CreditNote__r.National_Client_Number__c,c2g__CreditNote__r.OpCo__c,c2g__CreditNote__r.Order_Line_Item_Discount__c,c2g__CreditNote__r.Transaction_Type__c,c2g__CreditNote__r.SC_Billing_Partner__c ,c2g__CreditNote__r.SC_Media_Type__c,c2g__CreditNote__r.SC_Payment_Method__c , '+
            'c2g__CreditNote__r.SC_Billing_Entity__c,c2g__CreditNote__r.SC_P4P__c, Order_Line_Item__c,c2g__UnitPrice__c,c2g__Product__c,c2g__Quantity__c,c2g__LineDescription__c,Order_Line_Item__r.Product2__r.RGU__c, '+
            'Order_Line_Item__r.Directory_Code__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Directory_Edition__c,Order_Line_Item__r.Canvass__r.Canvass_Code__c,Order_Line_Item__r.CMR_Name__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.P4P_Billing__c,Order_Line_Item__r.Is_P4P__c,c2g__Dimension1__c,c2g__Dimension2__c,c2g__Dimension3__c, '+
            'c2g__Dimension4__c,c2g__LineNumber__c,c2g__NetValue__c,c2g__OwnerCompany__c,Directory_Edition__c,Directory__c,Edition_Code__c,Name,OLI_Line_Number__c,OLI_Product_Name__c,Payment__c,Sales_Invoice_Line_Item__c,Transaction_Type__c,c2g__TaxCode1__c,c2g__TaxCode2__c,c2g__TaxCode3__c,c2g__TaxValue1__c,c2g__TaxValue2__c,c2g__TaxValue3__c,Billing_Frequency__c  '+
            'FROM c2g__codaCreditNoteLineItem__c WHERE Order_Line_Item__c IN:setOLIId AND c2g__CreditNote__r.Is_Manually_Created__c = false AND Offset__c = false AND c2g__CreditNote__r.Telco_Reversal__c = false AND c2g__CreditNote__r.Credit_Frequency__c IN:setFrequency AND Billing_Frequency__c =:strFrequencyMode';

            if(String.isNotEmpty(strPartnerAccountId)) {
                strQuery += ' AND c2g__CreditNote__r.c2g__Account__c =:strPartnerAccountId';
            }
            if(bFutureTransfer) {
                strQuery += ' AND c2g__CreditNote__r.c2g__CreditNoteDate__c > TODAY';
            }
            strQuery += ' order by Order_Line_Item__c ASC';
        return Database.query(strQuery);
    } 
}