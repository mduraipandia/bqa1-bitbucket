global class TBChBillingCronSchedulerHandler implements TBCscBillingCronScheduler.TBCscBillingCronSchedulerInterface {
    global void execute(SchedulableContext sc) {
        PrintFirstMonthBilling objFMPBatch = new PrintFirstMonthBilling(system.today(), CommonMessages.MCFMPMediaType, true);
        Database.executeBatch(objFMPBatch, 20);
    }
}