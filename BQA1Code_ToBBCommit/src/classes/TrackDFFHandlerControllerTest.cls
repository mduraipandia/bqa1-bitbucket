@isTest(seeAllData=true)
public class TrackDFFHandlerControllerTest {
	
	static testmethod void test_method_one() {

		List<Track_DFF_Update__c> lstTDFFs = new List<Track_DFF_Update__c>();

		Test.startTest();

	    Product2 prdt = CommonUtility.createPrdtTest();
	    prdt.Product_Type__c = 'iYP Bronze';

	    Product2 prdt1 = CommonUtility.createPrdtTest();
	    prdt1.Product_Type__c = 'VP5';
	    prdt1.Product_or_Addon__c = 'Add-on';

	    Canvass__c c = CommonUtility.createCanvasTest();
	    insert c;

	    Account a = CommonUtility.createAccountTest(c);
	    a.TalusAccountId__c = 'afdfd132323';
	    insert a;

	    Contact cnt = CommonUtility.createContactTest(a);
	    insert cnt;

	    Opportunity oppty = CommonUtility.createOpptyTest(a);
	    insert oppty;

	    Order__c ord = CommonUtility.createOrderTest(a);
	    insert ord;

	    Order_Group__c ordGrp = CommonUtility.createOGTest(a, ord, oppty);
	    insert ordGrp;

	    Order_Line_Items__c OLI = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
	    OLI.Product2__c = prdt.Id;
	    OLI.Effective_Date__c = system.Today();
	    OLI.Status__c = 'Fulfilled';
	    OLI.isCanceled__c = true;
	    insert OLI;

	    Order_Line_Items__c OLI1 = CommonUtility.createOLITest(c, a, cnt, oppty, ord, ordGrp);
	    OLI1.Product2__c = prdt1.Id;
	    //OLI1.Effective_Date__c = system.Today();
	    insert OLI1;	    

	    Modification_Order_Line_Item__c mOLI = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, ordGrp);
	    //mOLI.Effective_Date__c=system.today();
	    insert mOLI;

	    Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
	    dff.recordTypeId = Label.YPC_Record_Type_Id;
        dff.ModificationOrderLineItem__c = mOLI.Id;	    
	    dff.DFF_Product__c = prdt.Id;
	    insert dff;

	    Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
	    dff1.DFF_Product__c = prdt1.Id;
	    dff1.Data_Fulfillment_Form__c = dff.Id;
	    dff1.OrderLineItemID__c = OLI1.Id;
	    insert dff1;

	    Track_DFF_Update__c tDff = CommonUtility.createTrackDFFTest(dff.Id);
	    tDff.Status__c = 'Applied';
	    tDff.Add_Ons__c = true;
	    tDff.Updated_Info__c = 'effective_date: BRZ';
	    insert tDff;

	    tDff.Status__c = 'Fulfilled';
	    update tDff;

	    Test.stopTest();

	}
	
	
}