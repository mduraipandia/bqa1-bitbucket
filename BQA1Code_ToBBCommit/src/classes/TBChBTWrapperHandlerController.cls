public class TBChBTWrapperHandlerController{

    /*** Wrapper for display Directories on Page 1 ***/
    public class DirectoryEditionWrapper {
        public string directoryEditionIds {get;set;}
        public Directory__c directory {get;set;}
        public String DirectoryEdition {get;set;}
        public DirectoryEditionWrapper(Directory__c directory, String DirectoryEdition, String directoryEditionIds) {
            this.directory = directory;
            this.DirectoryEdition = DirectoryEdition;
            this.directoryEditionIds = directoryEditionIds;
        }
    }
    /*** End Wrapper for display Directories on Page 1 ***/
    
    
    /*** Wrapper for display OLI on Page 2 ***/
    public class OrderLineItemByDECanvassWrapper{
        public Id DECanvassId{get;set;}
        public String DECanvassName{get;set;}
        public List<OrderLineItemWrapper> lstOLI{get;set;}
        
        public OrderLineItemByDECanvassWrapper(Id DECanvassId,String DECanvassName,List<OrderLineItemWrapper> lstOLI) {
            this.DECanvassId = DECanvassId;
            this.DECanvassName= DECanvassName;
            this.lstOLI=lstOLI;
        }
    }
    
    /*** Wrapper for display OLI on Page 2 ***/
    public class OrderLineItemByOrderSetWrapper {
        public Id OrderSetId{get;set;}
        public String OrderSetName{get;set;}
        public List<OrderLineItemWrapper> lstOLI{get;set;}
        
        public OrderLineItemByOrderSetWrapper(Id OrderSetId,String OrderSetName,List<OrderLineItemWrapper> lstOLI) {
            this.OrderSetId = OrderSetId;
            this.OrderSetName = OrderSetName;
            this.lstOLI=lstOLI;
        }
    }
    
    public class OrderLineItemWrapper{
        public boolean isChecked{get;set;}
        public Order_Line_Items__c oli{get;set;}
        public OrderLineItemWrapper(boolean isChecked,Order_Line_Items__c oli){
            this.oli=oli;
            this.isChecked=isChecked;
        }
    }
    /*** End Wrapper for display OLI on Page 2 ***/
    
    /*** Wrapper for display Invoice on Page 3 ***/
    public class SalesInvoiceWrapper{
            public String editionName {get;set;}
            public ID dId {get;set;}
            
            public List < SubSalesInvoiceWrapper > subSalesWrapper {get;set;}
            public SalesInvoiceWrapper(List < SubSalesInvoiceWrapper > subSalesWrapper, string editionName,Id dId) {
                this.subSalesWrapper = subSalesWrapper;
                this.editionName = editionName;
                this.dId=dId;
            }
    }
    public class SubSalesInvoiceWrapper{
        public string SIName {get;set;}
        public string frequency {get;set;}
        public string SIAccountName {get;set;}
        public date invoiceDte {get;set;}
        public date dueDate {get;set;}        
        public double invoiceAmount {get;set;}
         public double invoiceLineItemAmount {get;set;}
        public string status {get;set;}
        public Id invoiceId {get;set;}
        public Id accountId {get;set;}
        public Id opportunityId {get;set;}
        public Id periodId {get;set;}
        public Id currencyId {get;set;}
        public order_Line_Items__c oliForSalesInvoice {get;set;}
        public boolean isTransfer {get;set;}
        public list<c2g__codaInvoiceLineItem__c> lstInv {get;set;}
        public Integer nofoMonthTransfer {get;set;}
        public Integer successfulPayment {get;set;}
        public Integer paymentRemaining {get;set;}
        public SubSalesInvoiceWrapper(Double invoiceLineItemAmount,string frequency,string SIAccountName,Id accountId,Id opportunityId,Id periodId,Id currencyId,string SIName,date invoiceDte,date dueDate,Double invoiceAmount,string status,Id invoiceId,order_Line_Items__c oliForSalesInvoice,integer nofoMonthTransfer,list<c2g__codaInvoiceLineItem__c> lstInv,integer successfulPayment,integer paymentRemaining,boolean isTransfer ){
            this.SIName=SIName;
            this.frequency=frequency;
            this.invoiceLineItemAmount=invoiceLineItemAmount;
            this.SIAccountName=SIAccountName;
            this.invoiceDte=invoiceDte;
            this.dueDate=dueDate;
            this.invoiceAmount=invoiceAmount;
            this.status=status;
            this.oliForSalesInvoice=oliForSalesInvoice;
            this.invoiceId=invoiceId;
            this.lstInv = lstInv;
            this.nofoMonthTransfer = nofoMonthTransfer; 
            this.accountId = accountId;
            this.opportunityId = opportunityId;
            this.periodId = periodId;
            this.currencyId = currencyId;
            this.successfulPayment=successfulPayment;
            this.paymentRemaining=paymentRemaining;
            this.isTransfer=isTransfer;
        }
        public SubSalesInvoiceWrapper()
        {
        }
     }

    /*** End Wrapper for display Invoice on Page 3 ***/ 
    public class SalesInvoiceWrapperForSort implements Comparable 
    {
        public c2g__codaInvoice__c InvoiceInstance;
        public SalesInvoiceWrapperForSort(c2g__codaInvoice__c invoice) {
            InvoiceInstance = invoice;
        }
        public Integer compareTo(Object compareTo) 
        {
            SalesInvoiceWrapperForSort compareToInvoiceInstance = (SalesInvoiceWrapperForSort)compareTo;
            Integer returnValue = 0;
            if (InvoiceInstance.c2g__InvoiceDate__c > compareToInvoiceInstance.InvoiceInstance.c2g__InvoiceDate__c) 
            {
                returnValue = 1;
            } 
            else if (InvoiceInstance.c2g__InvoiceDate__c < compareToInvoiceInstance.InvoiceInstance.c2g__InvoiceDate__c) 
            {
                returnValue = -1;
            }
            
            return returnValue;       
        }
    } 
    
    /** Wrapper For Display the count of Sales Credit Note **/
    public class SalesCreditNoteCountWrapper implements Comparable
    {
        public string OLIName{get;set;}
        public integer countOfSCN{get;set;}
        
        public SalesCreditNoteCountWrapper(string OLIName,integer countOfSCN)
        {
            this.OLIName=OLIName;
            this.countOfSCN=countOfSCN;
        }
        public Integer compareTo(Object objToCompare) {
             return OLIName.CompareTo(((SalesCreditNoteCountWrapper)ObjToCompare).OLIName);
        }
    }
    
    public class SalesInvoiceLineItemWrapper{
        public boolean isTransfer{get;set;}
        public boolean bRetroFuture{get;set;}
        public c2g__codaInvoiceLineItem__c salesinvoicelineitem{get;set;}
        public SalesInvoiceLineItemWrapper(boolean isTransfer,c2g__codaInvoiceLineItem__c salesinvoicelineitem) {
            this.isTransfer=isTransfer;
            this.salesinvoicelineitem=salesinvoicelineitem;
        }
        public SalesInvoiceLineItemWrapper(boolean isTransfer,c2g__codaInvoiceLineItem__c salesinvoicelineitem, boolean bRetroFuture) {
            this.isTransfer=isTransfer;
            this.salesinvoicelineitem=salesinvoicelineitem;
            this.bRetroFuture = bRetroFuture;
        }
        public SalesInvoiceLineItemWrapper(){}
    }
}