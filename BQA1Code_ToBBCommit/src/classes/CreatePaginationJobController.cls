public class CreatePaginationJobController
{   
    private Id sfId;
    public boolean preventpaginate{get;set;}
    public List<Directory_Section__c> DSs {get;set;}
    Map<String,Id> mapForDirectoryEditionId=new Map<String,Id>();
    Map<String,string> mapForyear=new Map<String,string>();
    set<String> bookstatusSet;
    List<Directory_Edition__c> dEdition;
    public List<SelectOption> yearList{get;set;}
    public Pagination_Job__c PJ {get;set;}
    public String year {get;set;}
    public List<DirectorySectionWrapper> DirectorySectionWrapperList{get;set;}
    
    // constructor
    public CreatePaginationJobController(ApexPages.StandardController stdController)
    {
        bookstatusSet=new set<String>{'NI','BOTS','BOTS-1'};
        dEdition=new List<Directory_Edition__c>();
        yearList=new List<SelectOption> ();
        sfId = stdController.getRecord().Id;
        DirectorySectionWrapperList=new List<DirectorySectionWrapper>();
        preventpaginate=false;
        
        if (stdController.getRecord().getsObjectType() == Directory_Section__c.getsObjectType())
        {
            queryDirectorySection();
            
            DSs[0].Include_In_NCR_Section__c = true;
        }
        else if (stdController.getRecord().getsObjectType() == Directory__c.getsObjectType())
        {
            queryDirectorySectionByDirectory();
        
            for (Directory_Section__c DS : DSs)
            {
                DS.Include_In_NCR_Section__c = false;
            }
        }
        
        //year = String.valueOf(Date.today().year());
    }
    
    public PageReference doSave()
    {
        System.debug('*****doSave');
         System.debug('@@@year'+year);
         //System.debug('@@@mapForyear'+mapForyear);
         system.debug('@@@mapForDirectoryEditionId'+mapForDirectoryEditionId);
         //String editionValue = mapForyear.get(year);
         ID editionId=mapForDirectoryEditionId.get(year);
         System.debug('@@@editionId'+editionId);
         set<Id> setSectionId = new set<Id>();
        //year=year.split('  ')[0];
        //year=mapForyear.get(year);
        List<Pagination_Job__c> PJs = new List<Pagination_Job__c>();
        for (Directory_Section__c DS : DSs)
        {
            // overload boolean field to determine if to include in pagination job
            if (DS.Include_In_NCR_Section__c)
            {
                Pagination_Job__c PJ = new Pagination_Job__c();
                PJ.Directory_Section__c = DS.Id;
                setSectionId.add(DS.Id);
                PJ.Directory_Edition__c = editionId;
                System.debug('@@@ '+year);
                PJ.Year__c = year;
                PJ.Sequencing_Status__c = 'Not Started';
                PJs.add(PJ);
            }
        }
        
        if(PJs.size() > 0) {
    		List<Community_Section_Abbreviation__c> CASlst = CommunitySectionAbbreviationSOQLMethods.fetchCommSecAbb(setSectionId);
			set<string> setDS = new set<string>();
			set<string> setCity = new set<string>();
			map<string,Community_Section_Abbreviation__c> mapstrCSA = new map<string,Community_Section_Abbreviation__c>();
			if(CASlst.size()>0) {
				for(Community_Section_Abbreviation__c iteratorCSA : CASlst) {
					if(iteratorCSA.Directory_Section__c != null && iteratorCSA.Community_Name__c != null) {
						mapstrCSA.put(iteratorCSA.Directory_Section__c+iteratorCSA.Community_Name__c,iteratorCSA);
					}
					if(iteratorCSA.Directory_Section__c != null){
						setDS.add(iteratorCSA.Directory_Section__c);   
					}
					if(iteratorCSA.Community_Name__c != null){
						setCity.add(iteratorCSA.Community_Name__c);   
					}
				}
			}
			if(setDS.size()> 0) {
				insert PJs;
				ApplyCityServiceBatchClass apcsBatch= new ApplyCityServiceBatchClass(setDS,setCity,mapstrCSA, null, PJs);
				Database.executeBatch(apcsBatch);
		    }
		    else {
		    	for(Pagination_Job__c obj : PJs) {
		    		obj.PJ_Ready_to_Run_Informatica_Job__c = true;
		    	}
		    	insert PJs;
		    }
        }
        
        System.debug('*****PJs: ' + PJs);
        
        Integer dirSecSize = DSs.size();        
        
        /*for (Directory_Section__c DS : DSs) {
            if(i == dirSecSize){
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DS.Id, DS.Section_Page_Type__c, year, PJs);
                Database.executeBatch(DPSB);
            } else {
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DS.Id, DS.Section_Page_Type__c, year, null);
                Database.executeBatch(DPSB);
            }
            i += 1;
        }*/
        /*System.debug('*****dirSecSize PJs: ' + dirSecSize );
        Map<Integer,DirPaginationSequencingBatch> varNameToValueMap = new Map<Integer,DirPaginationSequencingBatch>();
        for(Integer i = 0; i < dirSecSize; i++){
            
            if(i==0 && i == dirSecSize-1){
              System.debug('@@@test '+year);
               System.debug('@@@ '+PJs);
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DSs.get(i).Id, DSs.get(i).Section_Page_Type__c, year, PJs, null);
                varNameToValueMap.put(i,DPSB);
            }
            else if(i==0){
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DSs.get(i).Id, DSs.get(i).Section_Page_Type__c, year, PJs, null);
                varNameToValueMap.put(i,DPSB);
            }
            else if(i == dirSecSize-1){            
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DSs.get(i).Id, DSs.get(i).Section_Page_Type__c, year, null, varNameToValueMap.get(i-1));
                varNameToValueMap.put(i,DPSB);
            }
            else{            
                DirPaginationSequencingBatch DPSB = new DirPaginationSequencingBatch(DSs.get(i).Id, DSs.get(i).Section_Page_Type__c, year, null, varNameToValueMap.get(i-1));
                varNameToValueMap.put(i,DPSB);
            }
            
        }
        
        
        Database.executeBatch(varNameToValueMap.get(dirSecSize-1));*/
            
        
        return new PageReference('/' + sfId);
    }
    
    public PageReference doCancel()
    {
        System.debug('*****doCancel');
        
        return new PageReference('/' + sfId);
    }
    
    private void queryDirectorySection()
    {
        //preventpaginate=false;
        DSs = [Select Id, Name, Non_Paginated_Section__c,Prevent_to_Paginate__c,Directory__c, Directory_Code__c, Section_Code__c, Section_Page_Type__c, 
                    PDF_Name__c, Include_In_NCR_Section__c, Inactive_Directory_Section__c,(select id from Directory_Paginations1__r  limit 1)
               From Directory_Section__c
               Where Id = :sfId];
                dEdition=[select id,Book_Status__c,Year__c,Edition_Code__c from Directory_Edition__c where Book_Status__c IN :bookstatusSet and Directory__c =:DSs[0].Directory__c  order by Year__c desc];
                fetchYears(dEdition);               
               if(DSs[0].Prevent_to_Paginate__c)
               {
                   if(DSs[0].Directory_Paginations1__r.size()==0)
                       preventpaginate=true;
               }
    }   

    private void queryDirectorySectionByDirectory()
    {
        DSs = [Select Id, Name, Non_Paginated_Section__c, Directory__c, Directory_Code__c, Section_Code__c, Section_Page_Type__c, 
                    PDF_Name__c,Prevent_to_Paginate__c , Include_In_NCR_Section__c, Inactive_Directory_Section__c,(select id from Directory_Paginations1__r  limit 1)
               From Directory_Section__c
               Where Directory__c = :sfId];
                dEdition=[select id,Book_Status__c,Year__c,Edition_Code__c from Directory_Edition__c where Book_Status__c IN :bookstatusSet and Directory__c =:sfId  order by Year__c desc];
                fetchYears(dEdition);
               for(Directory_Section__c d:DSs)
               {
                   preventpaginate=false;
                   if(d.Prevent_to_Paginate__c)
                   {
                       if(d.Directory_Paginations1__r.size()==0)
                           preventpaginate=true;
                   }
                   DirectorySectionWrapperList.add(new DirectorySectionWrapper(d,preventpaginate));
               }
    }
    private void fetchYears(List<Directory_Edition__c> directoryEditionList)
    {
        for(Directory_Edition__c tempEdition :directoryEditionList)
        {
            mapForDirectoryEditionId.put(tempEdition.Year__c,tempEdition.Id);
             mapForyear.put(String.valueOf(tempEdition.Edition_Code__c)+'  '+String.valueOf(tempEdition.Book_Status__c),tempEdition.Year__c);
            string value=String.valueOf(tempEdition.Year__c);
            string showvalue=String.valueOf(tempEdition.Edition_Code__c)+'  '+String.valueOf(tempEdition.Book_Status__c);
            yearList.add(new SelectOption(value,showvalue));
        }
    }
    
    public class DirectorySectionWrapper
    {
        public Directory_Section__c dsection{get;set;}
        public boolean preventPagination{get;set;}
        public DirectorySectionWrapper(Directory_Section__c dsection,boolean preventPagination)
        {
            this.dsection=dsection;
            this.preventPagination=preventPagination;
        }
    }
}