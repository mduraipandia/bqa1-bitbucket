global class TestSIPosting implements Database.Batchable<sObject> {
	public Integer startNumber;
	public Integer endNumber;
	public Date invoiceDate;
	global TestSIPosting(Date invoiceDate, Integer startNumber, Integer endNumber) {
		this.endNumber = endNumber;
		this.startNumber = startNumber;
		this.invoiceDate = invoiceDate;
	}
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //Date fromDate = Date.newInstance(2015, 9, 22);
        boolean bFalg = true;
        String strUserId = '005Z0000002oreN';
        String strQuery = 'SELECT Id, Reconcile__c FROM c2g__codaInvoice__c WHERE '
                            + 'c2g__InvoiceStatus__c = \'In Progress\''
                            + ' AND Write_Off__c =:bFalg and createdbyid =:strUserId and TestBulkPost__c >=:startNumber and TestBulkPost__c <=:endNumber';
        System.debug('Testingg strQuery '+strQuery);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> listSI) {
        set<Id> setInvoiceID = new set<Id>();
        for(c2g__codaInvoice__c iterator: listSI){
            setInvoiceID.add(iterator.Id);
        }
        BillingWizardCommonController.postSalesInvoice(BillingWizardCommonController.generateCODAAPICommonReference(setInvoiceID));
    }

    global void finish(Database.BatchableContext bc) {
        
    }
}