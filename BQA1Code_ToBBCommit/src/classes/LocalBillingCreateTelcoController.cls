public with sharing class LocalBillingCreateTelcoController {
	
	public Dummy_Object__c objDummy {get;set;}
	public list<SelectOption> lstBillingEntity {get;set;}
	public String strSelectedBillingEntity {get;set;}
	public String strPrintDigital {get;set;}
	public String strSelectedDirectoryEdition {get;set;}
	public list<SelectOption> lstDirectoryEdition {get;set;}	
	public boolean bShowCreateTelcoPrint {get;set;}
	public boolean bShowCreateTelcoDigi {get;set;}
	public Directory_Edition__c objDEValues{get;set;}
	public Digital_Telco_Scheduler__c objDTS {get;set;}
	public String strBatchStatusTelco {get;set;}
	public boolean bEnabledPollerForTelco {get;set;}
	public Decimal dTelcoInvoiceTotal {get;set;}
	public Decimal SILITotal {get;set;}
	
	Map<String,Digital_Telco_Scheduler__c> mpTelcoScheBE;
	
	public LocalBillingCreateTelcoController() {
        objDummy = new Dummy_Object__c();
        strPrintDigital='None';
        clearDatas();
        mpTelcoScheBE = new Map<String,Digital_Telco_Scheduler__c>();
        objDEValues = new Directory_Edition__c();
        /*Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();        
        if(reportIds.get('Digital Invoice Posting')!=null) digitalInvoicePostReport = reportIds.get('Digital Invoice Posting').Report_Id__c;
        if(reportIds.get('Digital SCN Posting')!=null) digitalSCNPostReport = reportIds.get('Digital SCN Posting').Report_Id__c;
        */   
    }
    
    public void clearDatas() {    	
    	bShowCreateTelcoPrint = false;
    	bShowCreateTelcoDigi = false;
    	strBatchStatusTelco = '';
    	bEnabledPollerForTelco = false;
    	dTelcoInvoiceTotal = 0.0;
    	
    	/*strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        strBatchStatusTelco = '';
        strSCNBatchStatus = '';
        strInvBatchStatus = '';
        bPostAllInvoice = false;
        bEnabledPollerForTelco = false;
        bInvoicePresent = false;
        bEnabledPoller = false;
        bShowPostFF = false;
        bShowCreateTelco = false;
        SetSalesCreditnote = new Set<Id>();
        setSIId = new set<Id>();        
    	MapSalesinvoice=new Map<id,c2g__codaInvoice__c>();
    	MapofSalesCreditnote =new Map<id,c2g__codaCreditNote__c>();*/
    }
    
    public void fetchDE() {
    	
    	if(strPrintDigital == 'Digital') {
    		lstBillingEntity = new list<SelectOption>();
	        if(objDummy.Date__c != null) {
	            list<Digital_Telco_Scheduler__c> lstTelcoScheduler = [Select id,Name,Telco__r.Name,Billing_Entity__c,Invoice_To_Date__c,Invoice_From_Date__c,Telco__c,Telco_Receives_EFile__c,
	            														XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where Bill_Prep_Date__c =:objDummy.Date__c];
	            if(lstTelcoScheduler.size()>0) {
	                for(Digital_Telco_Scheduler__c iterator : lstTelcoScheduler) {
						lstBillingEntity.add(new SelectOption(iterator.id, iterator.Billing_Entity__c));
						mpTelcoScheBE.put(iterator.id, iterator);
	                }
	            }
	        }
    	}
        else {
        	lstDirectoryEdition = new list<SelectOption>();
	        if(objDummy.Date__c != null) {
	            list<Directory_Edition__c> lstDE = [Select id,Name,New_Print_Bill_Date__c from Directory_Edition__c where Bill_Prep__c =:objDummy.Date__c Order By Name ASC];
	            if(lstDE.size()>0) {
	                for(Directory_Edition__c iterator : lstDE) {
	                    lstDirectoryEdition.add(new SelectOption(iterator.id, iterator.Name));
	                }
	            }
	        }
        }
        
    }
    
    public void clickGoPrint() {
    	clearDatas();
    	
		fetchDirectoryEditionForReportPrint();
		list<c2g__codaInvoice__c> lstSalesInvoice = [Select Id, SI_Telco__c, SI_Telco__r.Name, Reconcile__c, c2g__Account__c, c2g__Account__r.Name, Customer_Name__c, c2g__NetTotal__c, SI_Payment_Method__c, SI_Billing_Partner__c, (Select Id, Order_Line_Item__r.UnitPrice__c From c2g__InvoiceLineItems__r) 
                                                    From c2g__codaInvoice__c
                                                    where SI_Successful_Payments__c = 1 AND SI_P4P__c = false AND SI_Media_Type__c = 'Print' AND c2g__InvoiceStatus__c = 'Complete' 
                                                    //AND SI_Payments_Remaining__c IN (11,0) 
                                                    AND SI_Directory_Edition_Code__c =:objDEValues.Edition_Code__c AND  SI_Directory_Code__c =:objDEValues.Directory_Code__c];
    	if(lstSalesInvoice.size() > 0) {
    		bShowCreateTelcoPrint = true;
    		map<Id, Decimal> mapSITotalByTelcoID = new map<Id, Decimal>();
    		map<Id, set<Id>> mapAccountIdByTelcoId = new map<Id, set<Id>>();
    		for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
    			if(iterator.SI_Payment_Method__c == 'Telco Billing') {
    				if(!mapAccountIdByTelcoId.containsKey(iterator.c2g__Account__c)) {
    					mapAccountIdByTelcoId.put(iterator.c2g__Account__c, new set<Id>());
    					mapSITotalByTelcoID.put(iterator.c2g__Account__c, 0);
    				}
    				mapAccountIdByTelcoId.get(iterator.c2g__Account__c).add(iterator.Customer_Name__c);
    				mapSITotalByTelcoID.put(iterator.c2g__Account__c, mapSITotalByTelcoID.get(iterator.c2g__Account__c) + iterator.c2g__NetTotal__c.setscale(2));
    			}
    		}
    		for(Id idTelco : mapAccountIdByTelcoId.keySet()) {
    			dTelcoInvoiceTotal += mapSITotalByTelcoID.get(idTelco);
    		}
    	}
    }
    
    public void clickGoDigital() {
    	clearDatas();
    	
		if(String.isNotBlank(strSelectedBillingEntity)) {
        	objDTS = mpTelcoScheBE.get(strSelectedBillingEntity);
        	SILITotal = 0.0;
        	list<c2g__codaInvoice__c> lstSalesInvoice = [SELECT SI_Billing_Partner__c, Customer_Name__c,Reconcile__c,Name,c2g__NetTotal__c 
        											FROM c2g__codaInvoice__c 
        											where SI_P4P__c = false AND c2g__InvoiceStatus__c = 'Complete' AND Billing_Entity__c=:objDTS.Billing_Entity__c 
        											AND SI_Media_Type__c='Digital' AND  c2g__InvoiceDate__c >=:objDTS.Invoice_From_Date__c AND c2g__InvoiceDate__c <=:objDTS.Invoice_To_Date__c  ];
			if(lstSalesInvoice != null && lstSalesInvoice.size() > 0) {
				bShowCreateTelcoDigi = true;
				for(c2g__codaInvoice__c iterator : lstSalesInvoice) {				
	                if(iterator.SI_Billing_Partner__c != CommonMessages.BerryForDimension) {
	                    SILITotal=SILITotal+iterator.c2g__NetTotal__c;
	                }
	            }
			}
		}
    }
    
    public void fetchDirectoryEditionForReportPrint() {
        list<Directory_Edition__c> lstDE= [Select Directory__c,Telco__c,Directory_Code__c,Edition_Code__c,Telco_Recives_Electronice_File__c,XML_Output_Total_Amount__c,Sent_Telco_File__c,
                                            id,Name from Directory_Edition__c where id=:strSelectedDirectoryEdition];
        for(Directory_Edition__c iterator : lstDE) {
            objDEValues = iterator;
            if(objDEValues.XML_Output_Total_Amount__c != null && objDEValues.XML_Output_Total_Amount__c > 0) {
                strBatchStatusTelco = 'XML Telco Invoice Status : Completed';
                bEnabledPollerForTelco = false;
            }
        }
    }
    
    public void CheckTelcoOutputDigital() {
        if(objDTS !=null) {
            strBatchStatusTelco = 'XML Telco Invoice Status : InProgress';
            fetchDTSForReportDigital();
        }
    }
    
    private void fetchDTSForReportDigital() {
        objDTS=[select id,Name,Invoice_From_Date__c ,Billing_Entity__c,Invoice_To_Date__c,Telco__c,Telco_Receives_EFile__c,XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where id=:strSelectedBillingEntity];
        if(objDTS.XML_Output_Total_Amount__c != null && objDTS.XML_Output_Total_Amount__c > 0) {
            strBatchStatusTelco = 'XML Telco Invoice Status : Completed';
            bEnabledPollerForTelco = false;
        }
    }
    
    public void SendTelcoFileDigital() {
        if(objDTS != null) {
            Digital_Telco_Scheduler__c dts=new Digital_Telco_Scheduler__c(id=objDTS.id,Sent_Telco_File__c=true);
            bEnabledPollerForTelco = true;
            update dts;         
        }
    }
    
    public void createTelcoFilePrint() {
        if(String.isNotBlank(strSelectedDirectoryEdition)) {
            objDEValues.Sent_Telco_File__c = true;
            update objDEValues;
            strBatchStatusTelco = 'XML Telco Invoice Status : Queued';
            bEnabledPollerForTelco = true;
        }
    }
    
    public void checkTelcoXMLOutputPrint() {
        if(String.isNotBlank(strSelectedDirectoryEdition)) {
            strBatchStatusTelco = 'XML Telco Invoice Status : InProgress';
            fetchDirectoryEditionForReportPrint();
        }
    }
}