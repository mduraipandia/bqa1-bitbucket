@isTest
public class MediaTraxHeadingManualCallOutTest{
    public static testmethod void MediaTraxHeadingManualCallOutTest01(){
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        
        PageReference pageRef1 = new PageReference('/apex/MediaTraxHeadingCalloutPage_V1');
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(objDH);                     
        
        MediaTraxHeadingManualCallOutController objMediaController = new MediaTraxHeadingManualCallOutController(con);
        objMediaController.AddHeading();
        objMediaController.AddDirectory();
        objMediaController.AddClient();
        objMediaController.GetAddHeading();
        objMediaController.GetAddDirectory();
    }
    
    public static testmethod void MediaTraxHeadingManualCallOutTest02(){
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        
        PageReference pageRef1 = new PageReference('/apex/MediaTraxHeadingCalloutPage_V1');
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(objDH);                     
        
        MediaTraxHeadingManualCallOutController objMediaController = new MediaTraxHeadingManualCallOutController(con);
        objMediaController.GetAddClient();
        objMediaController.UpdateHeading();
        objMediaController.UpdateDirectory();
        objMediaController.UpdateClient();
        objMediaController.AuthendationSutureCallout();
    }
}