@isTest
public class DirectoryListingInsertTriggerBatchTest {
	
	static testmethod void test_directoryListingInsertTriggerBatch() {
        Account newTelcoAccount = TestMethodsUtility.generateTelcoAccount();
        insert newTelcoAccount;
        Telco__c newTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = newTelco.Id;
        insert objDir;
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Directory_Heading__c dirHeading = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(100);
        insert dirHeading;
        Directory_Heading__c dirHeading1 = Testmethodsutility.generateDirectoryHeading(); 
        dirHeading1.Directory_Heading_Name__c=Testmethodsutility.generateRandomString(101);
        insert dirHeading1;
        Listing__c  listing = Testmethodsutility.createListing('Main Listing');
        Directory_Listing__c DL1 = Testmethodsutility.generateDirectoryListing();
        DL1.Directory__c=objDir.Id;
        DL1.Listing__c=listing.Id;
        DL1.Directory_Section__c=objDS.Id;
        DL1.Directory_Heading__c = dirHeading.Id;
        DL1.Telco_Provider__c = newTelco.Id;
        DL1.DL_DM_isTriggerExecuted__c=false;
        List<Directory_Listing__c> dList = new List<Directory_Listing__c>();
        dList.add(DL1);
        insert dList;
        Database.executeBatch(new DirectoryListingInsertTriggerBatch('where id!=null'));
        
    }

}