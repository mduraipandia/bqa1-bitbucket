public class LocalBillingReportsController {
		
	public Id goLiveReportId {get;set;}
    public Id ARPostedReport{get;set;}
    public Id P4PReport{get;set;}
    public Id ClosedQuoteReport{get;set;}
    public Id LOBMatchingReportId{get;set;}
    public Id SCNCreditCardReport{get;set;}
    public Id DeclinedInvoicePaymentsReport{get;set;}
    public Id StatementSummaryReport{get;set;}
    public Id FFBillingAuditReport{get;set;}
    public Id CCNoExpirationReport{get;set;}
    public Id UpcomingCCExpirationsReport{get;set;}
    public Id SummaryReport{get;set;}
    
    public LocalBillingReportsController() {
            Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
            goLiveReportId = reportIds.get('Go Live').Report_Id__c;
            LOBMatchingReportId= reportIds.get('LOB Matching Report').Report_Id__c;
            ARPostedReport= reportIds.get('ARPostedReport').Report_Id__c;
            P4PReport=reportIds.get('P4PReport').Report_Id__c;
            ClosedQuoteReport=reportIds.get('ClosedQuotesGoneLive').Report_Id__c;
            SCNCreditCardReport=reportIds.get('SalesCreditNotesforCC/ACH').Report_Id__c;
            DeclinedInvoicePaymentsReport=reportIds.get('DeclinedInvoicePaymentsReport').Report_Id__c;
            StatementSummaryReport=reportIds.get('StatementSummaryReport').Report_Id__c;
            FFBillingAuditReport=reportIds.get('FFBillingAuditReport').Report_Id__c;
            CCNoExpirationReport=reportIds.get('CCNoExpirationReport').Report_Id__c;
            UpcomingCCExpirationsReport=reportIds.get('UpcomingCCExpirationsReport').Report_Id__c;
            SummaryReport=reportIds.get('SummaryReport').Report_Id__c;            
    }
}