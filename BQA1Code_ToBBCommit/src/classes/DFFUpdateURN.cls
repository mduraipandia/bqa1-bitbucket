global class DFFUpdateURN implements Database.Batchable<sObject>{
    
    //global string filtercondition;
    
    global DFFUpdateURN(){
        //filtercondition=condition;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'select id,urn_number__c from digital_product_Requirement__c where urn_number__c = null and recordtypeid in (\'012G0000000WYJf\',\'012G0000000WYJb\')';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<digital_product_Requirement__c> lstDFF) {
        
        Savepoint sp = Database.setSavepoint();
        Dummy_Object__c objDummyObj;
        List<Dummy_Object__c> lstDummyObj = new List<Dummy_Object__c>();
        List<Dummy_Object__c> lstSelDummyObj;
        List<digital_product_Requirement__c> lstUpdtDFF = new List<digital_product_Requirement__c>();
        try{
            for(digital_product_Requirement__c objDFF : lstDFF) {
                objDummyObj = new Dummy_Object__c();
                lstDummyObj.add(objDummyObj);
            }
            if(lstDummyObj.size() > 0) {
                insert lstDummyObj;
                lstSelDummyObj = [select name from Dummy_Object__c where id in :lstDummyObj];
            }
            Integer intCount = 0;
            for(digital_product_Requirement__c objDFF : lstDFF) {
                objDFF.URN_Number__c = Decimal.valueOf(lstSelDummyObj[intCount].name);
                intCount++;
                lstUpdtDFF.add(objDFF);
            }
            update lstUpdtDFF;
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch trigger for Listing insert');
        } 
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }

}