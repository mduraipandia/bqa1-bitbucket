<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>AboutBlacklist</defaultLandingTab>
    <description>Chatter Blacklist provides the ability to specify words or phrases on a blacklist which, when included in a Chatter post will be scrubbed for compliance with company standards such as no swearing, racist or sexist comments, product code words etc.</description>
    <label>Chatter Blacklist</label>
    <logo>ButtonsLogosImages/Blacklist_Logo2.gif</logo>
    <tab>AboutBlacklist</tab>
    <tab>Blacklisted_Word__c</tab>
    <tab>Blacklist_Audit__c</tab>
    <tab>Payment_History</tab>
    <tab>Directory_Guide__c</tab>
    <tab>Radio_Button_Background_Matching</tab>
    <tab>ffps_ct__FlatTransaction__c</tab>
    <tab>Galley_Parameter_N</tab>
    <tab>ffps_ct__MatchingWriteOff__c</tab>
    <tab>Tabular_Custom_Data_Table_Tax__c</tab>
    <tab>ffps_ct__StagingTax__c</tab>
    <tab>Billing_Transfer_Status__c</tab>
</CustomApplication>
