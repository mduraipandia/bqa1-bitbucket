<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Billing Transfer</label>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Opportunity</tab>
    <tab>Order__c</tab>
    <tab>Order_Group__c</tab>
    <tab>Order_Line_Items__c</tab>
    <tab>c2g__codaInvoice__c</tab>
    <tab>c2g__codaCreditNote__c</tab>
    <tab>Line_Item_History__c</tab>
    <tab>Billing_Transfer_Status__c</tab>
    <tab>Monthly_Cron__c</tab>
</CustomApplication>
