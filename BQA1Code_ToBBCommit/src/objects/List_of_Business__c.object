<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_ID__c</fullName>
        <externalId>false</externalId>
        <formula>LOB_Account_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account ID</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Account_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>List of Businesses</relationshipLabel>
        <relationshipName>List_of_Businesses</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>LOB_Account_Number__c</fullName>
        <externalId>false</externalId>
        <label>Account Number</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Billing_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Billing Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>LOB_Billing_Entity__c</fullName>
        <externalId>false</externalId>
        <label>Billing Entity</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Billing_Frequency__c</fullName>
        <externalId>false</externalId>
        <label>Billing Frequency</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Billing_Partner__c</fullName>
        <externalId>false</externalId>
        <label>Billing Partner</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Billing_Telephone__c</fullName>
        <externalId>false</externalId>
        <label>Billing Telephone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>LOB_Canvass__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Canvass</label>
        <referenceTo>Canvass__c</referenceTo>
        <relationshipLabel>List of Businesses</relationshipLabel>
        <relationshipName>List_of_Businesses</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>LOB_Directory_Code__c</fullName>
        <externalId>false</externalId>
        <label>Directory Code</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Directory_Name__c</fullName>
        <externalId>false</externalId>
        <label>Directory Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Edition_Code__c</fullName>
        <externalId>false</externalId>
        <label>Edition Code</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Edition_Name__c</fullName>
        <externalId>false</externalId>
        <label>Edition Name</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Invoice_Date__c</fullName>
        <externalId>false</externalId>
        <label>Invoice Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>LOB_Listing_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Listing Name</label>
        <referenceTo>Listing__c</referenceTo>
        <relationshipLabel>List of Businesses</relationshipLabel>
        <relationshipName>List_of_Businesses</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>LOB_Listing_Phone_Number__c</fullName>
        <externalId>false</externalId>
        <label>Listing Phone Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>LOB_Media_Type__c</fullName>
        <externalId>false</externalId>
        <label>Media Type</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_OLI_Count__c</fullName>
        <externalId>false</externalId>
        <label>OLI Count</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_OLI_Ids__c</fullName>
        <externalId>false</externalId>
        <label>OLI Ids</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>LOB_Parent_3L_External_ID__c</fullName>
        <externalId>false</externalId>
        <label>Parent 3L External ID</label>
        <length>50</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Parent_Account_Number__c</fullName>
        <externalId>false</externalId>
        <label>Parent Account Number</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Parent_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Parent Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>List of Businesses (Parent Account)</relationshipLabel>
        <relationshipName>List_of_Businesses1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>LOB_Service_End_Date__c</fullName>
        <externalId>false</externalId>
        <label>Service End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>LOB_Service_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Service Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>LOB_Telco_Name__c</fullName>
        <externalId>false</externalId>
        <label>Telco Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_Transaction_Type__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LOB_X3L_External_ID__c</fullName>
        <externalId>false</externalId>
        <label>3L External ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>List of Business</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>LOB_Account_Name__c</columns>
        <columns>LOB_Billing_Amount__c</columns>
        <columns>LOB_Billing_Entity__c</columns>
        <columns>LOB_Billing_Frequency__c</columns>
        <columns>LOB_Billing_Partner__c</columns>
        <columns>LOB_Billing_Telephone__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>LOB - {000000}</displayFormat>
        <label>List of Business Number</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>List of Businesses</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
