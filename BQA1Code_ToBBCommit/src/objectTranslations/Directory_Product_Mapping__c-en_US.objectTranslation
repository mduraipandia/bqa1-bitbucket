<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <caseValues>
        <plural>false</plural>
        <value>Directory Product Mapping</value>
    </caseValues>
    <caseValues>
        <plural>true</plural>
        <value>Directory Product Mappings</value>
    </caseValues>
    <fields>
        <help><!-- This field is updated via a Workflow and concatenates the full 18 Character Salesforce record ids for Product and Directory to ensure uniqueness --></help>
        <label><!-- DPM Dir Prod External ID --></label>
        <name>DPM_Dir_Prod_External_ID__c</name>
    </fields>
    <fields>
        <help><!-- Concatenation of the Directory 18 Character SFDC ID and the Product 18 Character SFDC ID --></help>
        <label><!-- DPM External ID --></label>
        <name>DPM_External_ID__c</name>
    </fields>
    <fields>
        <label><!-- Directory CASESAFEID --></label>
        <name>Directory_CASESAFEID__c</name>
    </fields>
    <fields>
        <label><!-- Directory Code --></label>
        <name>Directory_Code__c</name>
    </fields>
    <fields>
        <help><!-- The Directory this Product is allowed to be sold into --></help>
        <label><!-- Directory --></label>
        <name>Directory__c</name>
        <relationshipLabel><!-- Directory Product Mappings --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Exclude National Upcharge --></label>
        <name>Exclude_National_Upcharge__c</name>
    </fields>
    <fields>
        <label><!-- Foreign EAS WP Ban/Bill Remaining --></label>
        <name>Foreign_EAS_WP_Ban_Bill_Remaining__c</name>
    </fields>
    <fields>
        <help><!-- Amount of WP Banners or Billboards sold within an EAS Section that requires separate Inventory Tracking --></help>
        <label><!-- Foreign EAS WP Ban/Bill Sold --></label>
        <name>Foreign_EAS_WP_Ban_Bill_Sold__c</name>
    </fields>
    <fields>
        <label><!-- Full Rate --></label>
        <name>FullRate__c</name>
    </fields>
    <fields>
        <label><!-- Grandfathered --></label>
        <name>Grandfathered__c</name>
    </fields>
    <fields>
        <label><!-- Inventory Tracking Group --></label>
        <name>Inventory_Tracking_Group__c</name>
        <picklistValues>
            <masterLabel>Delivery RIDE</masterLabel>
            <translation><!-- Delivery RIDE --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Front Cover</masterLabel>
            <translation><!-- Front Cover --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Front Cover - Mini</masterLabel>
            <translation><!-- Front Cover - Mini --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inside Back Cover</masterLabel>
            <translation><!-- Inside Back Cover --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inside Back Cover - Half</masterLabel>
            <translation><!-- Inside Back Cover - Half --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inside Front Cover</masterLabel>
            <translation><!-- Inside Front Cover --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Inside Front Cover - Half</masterLabel>
            <translation><!-- Inside Front Cover - Half --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outside Back Cover</masterLabel>
            <translation><!-- Outside Back Cover --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Outside Back Cover - Half</masterLabel>
            <translation><!-- Outside Back Cover - Half --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Spine</masterLabel>
            <translation><!-- Spine --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Spine - Half</masterLabel>
            <translation><!-- Spine - Half --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tip On - 4 X 4 / 4 X 2</masterLabel>
            <translation><!-- Tip On - 4 X 4 / 4 X 2 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Tip On - 6 X 6</masterLabel>
            <translation><!-- Tip On - 6 X 6 --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>WP Banners</masterLabel>
            <translation><!-- WP Banners --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>WP Billboards</masterLabel>
            <translation><!-- WP Billboards --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>YP Leader Ad</masterLabel>
            <translation><!-- YP Leader Ad --></translation>
        </picklistValues>
    </fields>
    <fields>
        <label><!-- Local Only --></label>
        <name>Local_Only__c</name>
    </fields>
    <fields>
        <label><!-- Max Quantity Allowed --></label>
        <name>Max_Quantity_Allowed__c</name>
    </fields>
    <fields>
        <label><!-- National Full Rate --></label>
        <name>National_Full_Rate__c</name>
    </fields>
    <fields>
        <label><!-- National Only --></label>
        <name>National_Only__c</name>
    </fields>
    <fields>
        <label><!-- Product2 --></label>
        <name>Product2__c</name>
        <relationshipLabel><!-- Directory Product Mappings --></relationshipLabel>
    </fields>
    <fields>
        <label><!-- Product CASESAFEID --></label>
        <name>Product_CASESAFEID__c</name>
    </fields>
    <fields>
        <label><!-- Product Code/UDAC --></label>
        <name>Product_Code_UDAC__c</name>
    </fields>
    <fields>
        <label><!-- Quantity Remaining --></label>
        <name>Quantity_Remaining__c</name>
    </fields>
    <fields>
        <help><!-- The quantity of the specific product sold per the specific Directory --></help>
        <label><!-- Quantity Sold --></label>
        <name>Quantity__c</name>
    </fields>
    <fields>
        <help><!-- This Checkbox should be TRUE if this Product in this Directory requires Inventory Tracking --></help>
        <label><!-- Requires Inventory Tracking --></label>
        <name>Requires_Inventory_Tracking__c</name>
    </fields>
    <fields>
        <label><!-- Round Test --></label>
        <name>Round_Test__c</name>
    </fields>
    <fields>
        <label><!-- is Active --></label>
        <name>is_Active__c</name>
    </fields>
    <fields>
        <label><!-- strProductInventoryCombo --></label>
        <name>strProductInventoryCombo__c</name>
    </fields>
    <layouts>
        <layout>Admin Directory Product Mapping Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
    </layouts>
    <layouts>
        <layout>Directory Product Mapping Layout</layout>
        <sections>
            <label><!-- Custom Links --></label>
            <section>Custom Links</section>
        </sections>
    </layouts>
    <startsWith>Consonant</startsWith>
</CustomObjectTranslation>
